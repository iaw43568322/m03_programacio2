package Exercici_reforç_de_1r;

public class LlocDeServei {

	// ATRIBUTS
	private int id;
	private String nom;
	private String descripcio;
	
	/**
	 * @param id
	 * @param nom
	 * @param descripcio
	 */
	public LlocDeServei(int id, String nom, String descripcio) {
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the descripcio
	 */
	public String getDescripcio() {
		return descripcio;
	}

	/**
	 * @param descripcio the descripcio to set
	 */
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
}
