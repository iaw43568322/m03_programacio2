package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public abstract class Tripulant {
	// Atributs
	protected static final String bandol = "Imperi Klingon";
	protected String id;
	protected String nom;
	protected boolean actiu;
	protected LocalDateTime dataAlta;
	protected int departament;
	private int llocDeServei;
	
	// CONSTRUCTORS
	
	public Tripulant(String id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	public Tripulant(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei) {
		this.id = id;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.departament = departament;
		this.llocDeServei = llocDeServei;
	}
	
	// GETTERS & SETTERS
	/**
	 * @return the bandol
	 */
	protected String getBandol() {
		return bandol;
	}
	/**
	 * @return the id
	 */
	protected String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	protected void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	protected String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	protected void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the actiu
	 */
	protected boolean isActiu() {
		return actiu;
	}
	/**
	 * @param actiu the actiu to set
	 */
	protected void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	/**
	 * @return the dataAlta
	 */
	protected LocalDateTime getDataAlta() {
		return dataAlta;
	}
	/**
	 * @param dataAlta the dataAlta to set
	 */
	protected void setDataAlta(LocalDateTime dataAlta) {
		this.dataAlta = dataAlta;
	}
	/**
	 * @return the departament
	 */
	protected int getDepartament() {
		return departament;
	}
	/**
	 * @param departament the departament to set
	 */
	protected void setDepartament(int departament) {
		this.departament = departament;
	}
	/**
	 * @return the llocDeServei
	 */
	protected int getLlocDeServei() {
		return llocDeServei;
	}
	/**
	 * @param llocDeServei the llocDeServei to set
	 */
	protected void setLlocDeServei(int llocDeServei) {
		this.llocDeServei = llocDeServei;
	}
	
	// MÈTODES
	/**
	 * Mètode abstracte
	 */
	protected abstract String ImprimirDadesTripulant();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
