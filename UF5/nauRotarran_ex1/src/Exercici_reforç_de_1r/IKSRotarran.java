package Exercici_reforç_de_1r;

import java.time.LocalDateTime;
import java.time.format.*;

public class IKSRotarran {

	public static void main(String[] args) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		// Creem l'objecte capità i mariner
		Oficial capita = new Oficial("001-A", "Martok", true, LocalDateTime.parse("21-04-2000 12:22", formatter), 1, 5, true, "Capitanejar la nau");
		Mariner mariner_02_03 = new Mariner("758-J", "Kurak", true,LocalDateTime.parse("14-12-1996 10:20", formatter), 3, 1, true, "Mariner encarregat del timó i la navegació durant el 2n torn");
		
		/* EXERCICIS */
		/* 1. Treure   per   pantalla   el   departament   del   capita   accedint  directament
		 * a   l'atribut   pertinent
		 */
		System.out.println("\n-----------EXERCICI 1---------\n");
		System.out.println("Departament del capità: " + capita.departament);
		/* 2. Treure per pantalla la descripció de la feina que fa el capita 
		 * accedint directament a l'atributpertinent. 
		 */
		System.out.println("Descripcio de la feina del capità: " + capita.getDescripcioFeina());
		/* 3. Cridar a la funció  imprimirDadesTripulant()  del capità. */
		System.out.println("Dades del capità:\n" + capita.ImprimirDadesTripulant() + "\n");
		/* 4. Canviar el departament del capità al nº10 directament  */
		capita.departament = 10;
		System.out.println("Departament del capità: " + capita.departament);
		
		/* 5. */
		System.out.println("\n----------EXERICICI 5----------\n");
		System.out.println("Lloc de servei del capità: " + IKSRotarranConstants.LLOCS_DE_SERVEI[capita.getLlocDeServei()]);

	}

}
