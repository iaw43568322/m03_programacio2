package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Oficial extends Tripulant {
	
	// ATRIBUTS
	private boolean serveiEnElPont;
	private String descripcioFeina;

	public Oficial(String id, String nom, boolean serveiEnElPont, String descripcioFeina) {
		super(id, nom);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}

	public Oficial(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei, boolean serveiEnElPont, String descripcioFeina) {
		super(id, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}

	/**
	 * @return the serveiEnElPont
	 */
	public boolean isServeiEnElPont() {
		return serveiEnElPont;
	}

	/**
	 * @param serveiEnElPont the serveiEnElPont to set
	 */
	public void setServeiEnElPont(boolean serveiEnElPont) {
		this.serveiEnElPont = serveiEnElPont;
	}

	/**
	 * @return the descripcioFeina
	 */
	public String getDescripcioFeina() {
		return descripcioFeina;
	}

	/**
	 * @param descripcioFeina the descripcioFeina to set
	 */
	public void setDescripcioFeina(String descripcioFeina) {
		this.descripcioFeina = descripcioFeina;
	}

	@Override
	protected String ImprimirDadesTripulant() {
		return "TRIPULANT " + this.id + " :\n\nNom: " + this.nom +
				"\nBàndol: " + this.bandol + "\nDepartament: " + this.departament +
				"Lloc de Servei: " + this.getLlocDeServei() +
				"\nDescripció feina: " + this.descripcioFeina;
	}

}
