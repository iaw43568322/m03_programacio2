package com.daw.eva.ecom;


import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.CallableStatement;


// import com.mysql.cj.jdbc.CallableStatement;

import java.sql.ResultSet;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class ClientJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "2965bruan";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "clientes";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DIR = "direccion";

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
    	// Conectem amb la base de dades a partir d'un try catch
    	try {
    		conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    		System.out.println(DB_MSQ_CONN_OK);
    		return true;
    	} catch (SQLException e) {
    		throw new IllegalStateException(DB_MSQ_CONN_NO, e);
    	}
        
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     */
    public static boolean isConnected() {
        // Comprobamos estado de la conexión
        try {
        	// Comprobem si la connexió està oberta o tancada
        	if (conn.isClosed()) {
        		System.out.println(DB_MSQ_CONN_NO);
        		return false;
        	} else {
        		System.out.println(DB_MSQ_CONN_OK);
        		return true;
        	}
        } catch (SQLException e) {
        	System.out.println(DB_MSQ_CONN_NO + "\n" + e);
        	return false;
        	
        }
    }

    /**
     * Cierra la conexión con la base de datos
     */
    public static void close() {
    	// Intentem tancar la connexió
    	try {
    		conn.close();
    		System.out.println("Connexió tancada amb èxit");
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
        
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA CLIENTES
    //////////////////////////////////////////////////
    ;
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla clientes de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     * @throws DAOException 
     */
    public static ResultSet getTablaClientes(int resultSetType, int resultSetConcurrency) throws DAOException {
        // Conectem la base de dades i agafem el contingut de la tabla clients
    	try {
    		Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    		CallableStatement sentSQL = conn.prepareCall("Call getClients()", resultSetType, resultSetConcurrency);
    		ResultSet rs = sentSQL.executeQuery();
    		return rs;
    	} catch (SQLException e) {
    		throw new DAOException(e);
    	}

    }

    /**
     * Obtiene toda la tabla clientes de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     * @throws DAOException 
     */
    public static ResultSet getTablaClientes() throws DAOException {
    	return getTablaClientes(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
     
    }

    /**
     * Imprime por pantalla el contenido de la tabla clientes
     * @throws DAOException 
     */
    public static void printTablaClientes() throws DAOException {
        // Agafem el resultat de la tabla clientes i el recorrem 1 per 1
    	try {
    		ResultSet result = ClientJDBCDAO.getTablaClientes();
    		
    		while (result.next() ) {
    			// Guardem els camps del resultat
    			int id = result.getInt(DB_CLI_NOM);
    			String nom = result.getString(DB_CLI_NOM);
    			String direccio = result.getString(DB_CLI_DIR);
    			// Ho imprimim per pantalla
    			System.out.println("Client " + id + " : \nNom: " + nom + "\nDirecció: " + direccio);
    		}
    	} catch (SQLException e) {
    		throw new DAOException(e);
    	}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO CLIENTE
    //////////////////////////////////////////////////
    ;
    
    /**
     * Solicita a la BD el cliente con id indicado
     * @param id id del cliente
     * @return ResultSet con el resultado de la consulta, null en caso de error
     * @throws DAOException 
     */
    public static ResultSet getCliente(int id) throws DAOException {
        try {
        	Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	
        	CallableStatement stm = conn.prepareCall("Call getClientById(?)", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        	stm.setLong(1,  id);
        	// Guardem el resultat
        	ResultSet result = stm.executeQuery();
        	result.next();
        	return result;
        } catch (Exception e) {
        	throw new DAOException(e);
        }
    }

    /**
     * Comprueba si en la BD existe el cliente con id indicado
     *
     * @param id id del cliente
     * @return verdadero si existe, false en caso contrario
     * @throws DAOException 
     */
    public static boolean existsCliente(int id) throws DAOException {
        try {
        	ResultSet result = getCliente(id);
        	// Comprovem si el resultat està buit o no
        	if (result == null) {
        		return false;
        	}
        	
        	result.close();
        	return true;
        } catch (SQLException e) {
        	throw new DAOException(e);
        }
    }

    /**
     * Imprime los datos del cliente con id indicado  --> Carga objecto Client
     *
     * @param id id del cliente
     * @throws DAOException 
     */
    public static void printCliente(int id) throws DAOException {
        try {
        	ResultSet result = getCliente(id);
        	
        	if (result == null) {
        		throw new DAOException("El client no existeix");
        	}
        	
        	int idClient = result.getInt(DB_CLI_ID);
        	String nomClient = result.getString(DB_CLI_NOM);
        	String direccioClient = result.getString(DB_CLI_DIR);
        	
        	System.out.println("Client " + idClient + " : \nNom: " + nomClient + "\nDirecció: " + direccioClient);
        	
        } catch (SQLException e) {
        	throw new DAOException(e);
        }
    }

    /**
     * Solicita a la BD insertar un nuevo registro cliente
     *
     * @param nombre nombre del cliente
     * @param direccion dirección del cliente
     * @return verdadero si pudo insertarlo, false en caso contrario
     * @throws DAOException 
     */
    public static boolean insertCliente(String nombre, String direccion) throws DAOException {
        try {
        	ResultSet result = getTablaClientes(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        	
        	result.moveToInsertRow();
        	result.updateString(2,  nombre);
        	result.updateString(3,  direccion);
        	result.insertRow();
        	// Mostrem missatge d'èxit
        	System.out.println("Client afegit amb èxit");
        	return true;
        } catch (SQLException e) {
        	throw new DAOException(e);
        }
    }

    /**
     * Solicita a la BD modificar los datos de un cliente
     *
     * @param id id del cliente a modificar
     * @param nombre nuevo nombre del cliente
     * @param direccion nueva dirección del cliente
     * @return verdadero si pudo modificarlo, false en caso contrario
     * @throws DAOException 
     */
    public static boolean updateCliente(int id, String nuevoNombre, String nuevaDireccion) throws DAOException {
        try {
        	ResultSet result = getCliente(id);
        	
        	if (result == null) {
        		throw new DAOException("El client no existeix");
        	}
        	
        	result.updateString(2,  nuevoNombre);
        	result.updateString(3,  nuevaDireccion);
        	result.updateRow();
        	result.close();
        	// Mostrem missatge d'èxit
        	System.out.println("Dades del client modificades amb èxit");
        	return true;
        } catch (SQLException e) {
        	throw new DAOException(e);
        }
    }

    /**
     * Solicita a la BD eliminar un cliente
     *
     * @param id id del cliente a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     * @throws DAOException 
     */
    public static boolean deleteCliente(int id) throws DAOException {
        try {
        	ResultSet result = getCliente(id);
        	
        	if (!existsCliente(id)) {
        		throw new DAOException("El client no existeix");
        	}
        	
        	result.deleteRow();
        	result.close();
        	// Mostrem missatge d'èxit
        	System.out.println("Client esborrat amb èxit");
        	return true;
        } catch (SQLException e) {
        	throw new DAOException(e);
        }
    }

}
