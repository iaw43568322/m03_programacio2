package streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedReader;


public class Exercici5 {

	public static void main(String[] args) throws IOException {
		// Declaració de variables
		ArrayList<File> llistaArxius = new ArrayList<File>();
		File file1 = new File("prova1.txt");
		File file2 = new File("prova2.txt");
		FileReader fr = null;
		BufferedReader br;
		String linia;
		
		llistaArxius.add(file1);
		llistaArxius.add(file2);
		
		for(int i = 0; i < llistaArxius.size(); i++) {
			try {
				// Iniciem el reader i el buffer
				fr = new FileReader(llistaArxius.get(i));
				br = new BufferedReader(fr);
				int contLinies = 0;
				int contCaracters = 0;

				// Mentres pugui llegir una línia contem com una línia més
				while((linia = br.readLine()) != null) {
					contLinies++;
					// Contem el número de caràcters
					contCaracters += linia.length();
					
				}
				System.out.println("Nom del fitxer: " + llistaArxius.get(i));
				System.out.println("Número de línies: " + contLinies);
				System.out.println("Número de caràcters: " + contCaracters);
				System.out.println();
			} catch (FileNotFoundException e) {
				System.out.println();
				System.out.println("El fitxer " + llistaArxius.get(i) + " no existeix");
				System.out.println();
			}
			
			
		}

	}

}
