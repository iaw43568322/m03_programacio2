package streams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Exercici7 {

	public static void main(String[] args) throws IOException {
		// Declaració de variables
		File file1 = new File("prova2.txt");
		HashMap<Character, Integer> lletres = new HashMap<Character, Integer>();
		FileReader fr = null;
		BufferedReader br;
		String linia;
		int charAnterior;
		
		try {
			fr = new FileReader(file1);
			br = new BufferedReader(fr);
			
			// Recorrem totes les línies del fitxer
			while((linia = br.readLine()) != null) {
				// Convertim tota la cadena a minúsculas
				String liniaMin = linia.toLowerCase();
				// Recorrem tots els caràcters de cada línia i anem guardant les repeticions
				for (int i = 0; i < liniaMin.length(); i++) {
					// Si el caràcter ja està en el HashMap li sumem una repetició
					// En cas contrari l'afegim per pimer cop
					if (lletres.containsKey(liniaMin.charAt(i))) {
						charAnterior = lletres.get(liniaMin.charAt(i));
						charAnterior++;
						lletres.put(liniaMin.charAt(i), charAnterior);
					} else {
						lletres.put(liniaMin.charAt(i), 1);
					}
				}
			}
			lletres.forEach((k, v) -> System.out.println("Lletra: " + k + "\nRepeticions: " + v + "\n"));
			/*
			for (HashMap<Character, Integer> lletra : lletres) {
				System.out.println("Caràcter: " + );
			}
			*/
			
		} catch (FileNotFoundException e) {
			System.out.println();
			System.out.println("El fitxer " + file1 + " no existeix");
			System.out.println();
		}
	}
}
