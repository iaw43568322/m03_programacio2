package streams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercici3 {
	
	// Mètodes
	public static void main(String[] args) {
		File file1 = new File("prova1.txt");
		File file2 = new File("prova2.txt");
		
		compareFiles(file1, file2);
	}
	
	public static void compareFiles(File file1, File file2) {
		// Declaració de variables
		FileReader fr1 = null;
		BufferedReader br1 = null;
		String linea1;
		FileReader fr2 = null;
		// BufferedReader br2 = null;
		String linea2;
		String resultat = "";
		
		try {
			fr1 = new FileReader(file1);
			fr2 = new FileReader(file2);
			br1 = new BufferedReader(fr1);
			//br1 = new BufferedReader(fr2);
			linea1 = br1.readLine();
			br1 = new BufferedReader(fr2);
			linea2 = br1.readLine();
			// Mentres els dos fitxers tinguin més lineas per llegir seguim
			/*
			while ((linea1 = br1.readLine()) != null) {
				// Si les líneas són diferents avisem a l'usuari
				resultat += linea1 + "\n";
			}
			*/
			resultat += linea1 + "\n" + linea2 + "\n\n";
					
		} catch (FileNotFoundException e) {
			resultat = "Fitxer no existeix";
		} catch (IOException e) {
			resultat = e.getMessage();
		}
		System.out.println(resultat);
	}
	
}
