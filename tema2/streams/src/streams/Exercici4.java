package streams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Exercici4 {
	
	public static void main(String[] args) throws IOException {
		// Declaració de variables
		File dir = new File("exercici4");
		FileReader fr = null;
		BufferedReader br;
		FileWriter fw = null;
		PrintWriter pw;
		String dirSeguretat;
		String linia;
		int cont = 0;
		boolean append = true;
		
		// Comprobem que el directori passat existeixi
		if (dir.isDirectory()) {
			// Guardem tots els fitxers del directori
			String[] llistaFitxers = dir.list();
			// Mirem si hi ha un directori que es diu backUp0 o backUp1...
			for (int i = 0; i < llistaFitxers.length; i++) {
				// Comprobem que no hi hagi cap fitxer que es digui backUp0, 1, 2...
				if (llistaFitxers[i].equals("backUp" + cont)) {
					i = 0;
					cont++;				
				}
			}
			dirSeguretat = dir + "/backUp" + cont;
			File copia = new File(dirSeguretat);
			// Creem el directori copia de seguretat
			if (copia.mkdir()) {
				System.out.println("S'ha creat el directori backUp" + cont);
				try {
					for (int i = 0; i < llistaFitxers.length; i++) {
						// Si el fitxer és un directori no el copiem
						File f = new File(llistaFitxers[i]);
						if ( !(f.isDirectory()) ) {
							fw = new FileWriter(dirSeguretat + "/" + llistaFitxers[i], append);
							pw = new PrintWriter(fw, true);
							fr = new FileReader("exercici4/" + llistaFitxers[i]);
							br = new BufferedReader(fr);
							// Mentres hi hagi liníes anem copiant-les
							while ((linia = br.readLine()) != null) {
								pw.println(linia);
							}
						}
						
					}
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			} else {
				System.out.println("Error al crear el directori");
			}
			// System.out.println("S'ha creat el directori backUp" + cont);
		} else {
			System.out.println("El directori " + dir + " no existeix o no és un directori");
		}
	}
}
