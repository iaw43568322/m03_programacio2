
public class Car implements Vehicle {
	int price;
	String name;
	String brand;
	
	/**
	 * @param price
	 * @param name
	 * @param brand
	 */
	public Car(int price, String name, String brand) {
		this.price = price;
		this.name = name;
		this.brand = brand;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public void Drive() {
		// TODO Auto-generated method stub
		System.out.println("Conduint el cotxe");
	}

	@Override
	public void Stop() {
		// TODO Auto-generated method stub
		System.out.println("Parant el cotxe");
	}

	@Override
	public void StartEngine() {
		// TODO Auto-generated method stub
		System.out.println("Engegant el cotxe");
	}

	@Override
	public void TurnLeft() {
		// TODO Auto-generated method stub
		System.out.println("Girant el cotxe");
	}

	@Override
	public void GoToITV() {
		// TODO Auto-generated method stub
		System.out.println("Revisió al cotxe");
	}
	
}
