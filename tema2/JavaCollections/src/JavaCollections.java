import java.util.ArrayList;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {

		// Creem l'array list
		ArrayList<String> l1 = new ArrayList<String>();

		// Afegim dos animals a la llista
		l1.add("Gat");
		l1.add("jaguar");

		System.out.println(l1.get(0));
		System.out.println(l1.get(1));

		// Modifiquem l'animal de la posició 0
		l1.set(0, "Gos");

		System.out.println(l1.get(0));
		System.out.println(l1.get(1));

		// Afegim més animals
		l1.add("pantera");
		l1.add("gat");
		l1.add("lynx");

		// Mirem si l'animal lynx està a la llista
		if (l1.contains("lynx")) {
			System.out.println("Lynx està present a la llista");
		} else {
			System.out.println("Lynx no està present a la llista");
		}

		// Recorrem tota la llista amb bucles
		
		for (int i = 0; i < l1.size(); i++) {
			System.out.println("\nAnimal " + i + " : " + l1.get(i));
		}
		
		System.out.println();
		
		for (String animal : l1) {
			System.out.println(animal);
		}
		
		System.out.println();

		Iterator i = l1.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
		
		System.out.println();
		
		// Convertim la arrayList en un array
		String s[] = new String[l1.size()];
		s = l1.toArray(s); // També pot ser l1.toArray(s); 
		for (String animal : s) {
			System.out.println(animal);
		}
		
		System.out.println();
		
		// Instanciem 4 cotxes
		Car c1 = new Car(50000, "m3", "bmw");
		Car c2 = new Car(65000, "amg", "mercedes");
		Car c3 = new Car(10000, "scenic", "renault");
		Car c4 = new Car(35000, "gti5", "golf");
		
		c1.TurnLeft();
		c2.Drive();
		
		// Creem una llista de cotxes i afegim tots els cotxes
		ArrayList<Car> c = new ArrayList<Car>();
		c.add(c1);
		c.add(c2);
		c.add(c3);
		c.add(c4);
		
		// Recorrem aquesta llista accedint a tots els atributs de cada cotxe
		for (int j = 0; j < c.size(); j++) {
			System.out.println("\nCotxe " + j + " : " + c.get(j).brand + " , " + c.get(j).name + " , " + c.get(j).price);
		}
		
		System.out.println();
		
		for (Car cotxe : c) {
			System.out.println(cotxe.name + " , " + cotxe.brand + " , " + cotxe.price);
		}
		
	}
}
