package excepcions;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		System.out.println("Introdueix un número");
		Scanner sc = new Scanner(System.in);
		int user_id = sc.nextInt();
		
		try {
			if (user_id != 1234) {
				throw new InvalidUserIdException();
			}
			System.out.println("Entra l'ingrés a dipositar");
			int deposit_amount = sc.nextInt();
			int current_balance = 0;
			
			try {
				if (deposit_amount < 0) {
					throw new NegativeNotAllowedException();
				} else {
					current_balance += deposit_amount;
					System.out.println("Updated balance is: " + current_balance);
				}
			}
			catch (Exception e) {
				System.out.println(e);
			}
		}
		catch (Exception e) {
			System.out.println(e);
		}
		
		
	}

}
