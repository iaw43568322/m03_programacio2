package exercici_10;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class Main {

	public static void main(String[] args) {
		try {
			// Apartat 1: Crea el directori "directoriExercici10" i dins el directori
			// "origen" (forceMkdir)
			// Creem el directori
			String path = "directoriExercici10/";
			File directori = new File(path);
			File directoriOrigen = null;
			// L'esborrem en cas de que existeixi
			if (directori.exists()) {
				FileUtils.deleteDirectory(directori);
			}
			FileUtils.forceMkdir(directori);
			// Dins d'aquest creem el directori "origen"
			directoriOrigen = new File(path + "origen");
			FileUtils.forceMkdir(directoriOrigen);

			// Apartat 2: Crea amb un bucle 5 fitxers anomenats "fitxer1.txt" -
			// "fitxer5.txt" (createNewFile)
			// Bucle per crear els fitxers
			ArrayList<String> nomsFitxers = new ArrayList<String>();
			for (int i = 0; i < 5; i++) {
				String pathFitxer = path + "fitxer" + (i + 1) + ".txt";
				// Afegim el path al nom de fitxers
				nomsFitxers.add(pathFitxer);
				File fitxer = new File(pathFitxer);
				// Comprobem que es crea el nou fitxer
				boolean fitxerCreat = fitxer.createNewFile();
				System.out.println("APARTAT 2: S'ha creat el fitxer " + pathFitxer + "? " + fitxerCreat);

			}
			System.out.println();
			System.out.println();
			// Apartat 3: Afegeix una llista de linies de text en els fitxers (les linies de
			// text seràn el path + nom dels fitxers) (listFiles)(writeLines)
			// Llistem els arxius del directori
			String[] extensions = { "txt", "java" };
			List<File> llistaArxius = (List<File>) (FileUtils.listFiles(directori, extensions, true));
			// Recorrem els arxius
			for (File fitxer : llistaArxius) {
				FileUtils.writeLines(fitxer, "UTF-8", nomsFitxers, null, true);
			}
			System.out.println(
					"ARA ES HORA DE MIRAR SI S'HA AFEGIT EL CONTINGUT ALS FITXERS QUE ENCARA ES TROBEN EN " + path);
			bloquejarPantalla();

			// Apartat 4: Mou els 5 fitxers a dins del directori "origen"
			// (moveFileToDirectory)
			// Recorrem la llista de fitxer
			for (File fitxer : llistaArxius) {
				FileUtils.moveFileToDirectory(fitxer, directoriOrigen, false);
			}
			System.out.println("APARTAT 4: S'HAN MOGUT ELS 5 FITXERS A directoriExercici10/origen");
			System.out.println();
			System.out.println();
			// Apartat 5: Pregunta si el directori conté algún fitxer amb nom "fitxer3.txt"
			// (getFile)(directoryContains)
			boolean conteFitxer = FileUtils.directoryContains(directoriOrigen,
					FileUtils.getFile(directoriOrigen, "fitxer3.txt"));
			System.out.println("APARTAT 5: Esta fitxer3.txt?: " + conteFitxer);
			System.out.println();
			System.out.println();
			// Apartat 6: Esborra el fitxer "fitxer3.txt" i torna a executar l'apartat 5
			// (forceDelete)
			FileUtils.forceDelete(FileUtils.getFile(directoriOrigen, "fitxer3.txt"));
			conteFitxer = FileUtils.directoryContains(directoriOrigen,
					FileUtils.getFile(directoriOrigen, "fitxer3.txt"));
			System.out.println("APARTAT 6: Esta fitxer3.txt?: " + conteFitxer);
			System.out.println();
			System.out.println();
			// Apartat 7: Comproba si el fitxer "fitxer1.txt" és mes vell que "Fitxer5.txt"
			// (isFileOlder)
			File fitxer1 = FileUtils.getFile(directoriOrigen, "fitxer1.txt");
			File fitxer5 = FileUtils.getFile(directoriOrigen, "fitxer5.txt");
			System.out.println(
					"Data (long Date) última modificació de " + fitxer1.getName() + ": " + fitxer1.lastModified());
			System.out.println("Data (Calendar) última modificació de " + fitxer1.getName() + ": "
					+ convertirLongTimeEnCalendar(fitxer1.lastModified()));
			System.out.println(
					"Data (long Date) última modificació de " + fitxer5.getName() + ": " + fitxer5.lastModified());
			System.out.println("Data (Calendar) última modificació de " + fitxer5.getName() + ": "
					+ convertirLongTimeEnCalendar(fitxer5.lastModified()));
			System.out.println(fitxer1.getName() + " és més vell que " + fitxer5.getName() + "?: "
					+ FileUtils.isFileOlder(fitxer1, fitxer5));
			System.out.println();
			System.out.println();
			// Apartat 8: Copia, machacant el contingut anterior, la linia "linia
			// sobreescrita" en el fitxer "fitxer1.txt" (writeStringToFile)
			FileUtils.writeStringToFile(fitxer1, "linia sobreescrita");
			System.out.println("APARTAT 8: CANVIAT EL CONTINGUT DEL FITXER fitxer1.txt");
			System.out.println();
			System.out.println();
			// Apartat 9: Comproba si el fitxer "fitxer1.txt" és mes nou que "fitxer5.txt"
			System.out.println("APARTAT 9:");
			System.out.println(
					"Data (long Date) última modificació de " + fitxer1.getName() + ": " + fitxer1.lastModified());
			System.out.println("Data (Calendar) última modificació de " + fitxer1.getName() + ": "
					+ convertirLongTimeEnCalendar(fitxer1.lastModified()));
			System.out.println(
					"Data (long Date) última modificació de " + fitxer5.getName() + ": " + fitxer5.lastModified());
			System.out.println("Data (Calendar) última modificació de " + fitxer5.getName() + ": "
					+ convertirLongTimeEnCalendar(fitxer5.lastModified()));
			System.out.println(fitxer1.getName() + " és més nou que " + fitxer5.getName() + "?: "
					+ FileUtils.isFileNewer(fitxer1, fitxer5));
			bloquejarPantalla();

			// Apartat 10: Fes un touch sobre el fitxer "fitxer5.txt" (touch)
			FileUtils.touch(fitxer5);
			System.out.println("APARTAT 10:");
			System.out.println(
					"Data (long Date) última modificació de " + fitxer1.getName() + ": " + fitxer1.lastModified());
			System.out.println("Data (Calendar) última modificació de " + fitxer1.getName() + ": "
					+ convertirLongTimeEnCalendar(fitxer1.lastModified()));
			System.out.println(
					"Data (long Date) última modificació de " + fitxer5.getName() + ": " + fitxer5.lastModified());
			System.out.println("Data (Calendar) última modificació de " + fitxer5.getName() + ": "
					+ convertirLongTimeEnCalendar(fitxer5.lastModified()));
			System.out.println(fitxer1.getName() + " és més nou que " + fitxer5.getName() + "?: "
					+ FileUtils.isFileNewer(fitxer1, fitxer5));
			System.out.println();
			System.out.println();

			// Apartat 11: Mou el directori "origen" al directori "desti"
			// (moveDirectoryToDirectory) i si no existeix previament "desti" donçs que es
			// crei automàticament
			File directoriDesti = new File(path + "desti");
			if (!directoriDesti.exists()) {
				FileUtils.forceMkdir(directoriDesti);
			}
			FileUtils.moveDirectoryToDirectory(directoriOrigen, directoriDesti, false);
			System.out.println("APARTAT 11: S'HA MOGUT EL DIRECTORI directoriExercici10/" + directoriOrigen.getName()
					+ " A DINS DEL DIRECTORI directoriExercici10/" + directoriDesti.getName());
			System.out.println();
			System.out.println();

			// Apartat 12: Calcula el tamany del directori "desti"
			// (sizeOfDirectoryAsBigInteger)
			BigInteger tamanyDir = FileUtils.sizeOfAsBigInteger(directoriDesti);
			System.out.println("APARTAT 12: ELTAMANY DEL DIRECTORI desti ES: " + tamanyDir);
			System.out.println(
					"SUMEU EL TAMANY DELS 5 FITXERS QUE HI HA DINS DEL DIRECTORI dins I VEUREU QUE SUMEN " + tamanyDir);
			System.out.println();
			System.out.println();
			
			// Apartat 13: Fer servir el iterador de FileUtils per a recorre el directori
			// "directoriExercici10" i treure per pantalla el seu contingut indicant si son
			// fitxers o directoris (iterateFilesAndDirs).
			Iterator it = FileUtils.iterateFilesAndDirs(directori, new WildcardFileFilter("*.txt"),
					new WildcardFileFilter("*"));
			System.out.println("APARTAT 13:");
			while (it.hasNext()) {
				File fileTmp = (File) it.next();
				System.out.println("NOM DEL FILE TROBAT: " + ((File) fileTmp).getName());
				System.out.println("PATH DEL FILE TROBAT: " + ((File) fileTmp).getPath());
				System.out.println("ES UN FITXER?: " + ((File) fileTmp).isFile());
				System.out.println("ES UN DIRECTORI?: " + ((File) fileTmp).isDirectory());

			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void bloquejarPantalla() {
		Scanner in = new Scanner(System.in);
		System.out.println("Toca 'C' para continuar");
		while (in.hasNext()) {
			if ("C".equalsIgnoreCase(in.next())) {
				break;
			}
		}
		System.out.println();
		System.out.println();
	}

	public static String convertirLongTimeEnCalendar(long time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(time);
		return (cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR)
				+ " a les " + cal.get(Calendar.HOUR_OF_DAY) + "h " + cal.get(Calendar.MINUTE) + "' "
				+ cal.get(Calendar.SECOND) + "'' " + cal.get(Calendar.MILLISECOND));
	}

}
