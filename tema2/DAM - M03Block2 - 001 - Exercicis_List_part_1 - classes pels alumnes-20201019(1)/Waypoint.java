import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Waypoint {

	// Métode 1
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		// Creem un objecte de tipus comprovacioRendiment i el retornem
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		
		return comprovacioRendimentTmp;
	}
	
	// Métode 2
	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimentTmp) {
		// Declaració de variables
		long tempsMilisegons;
		long tempsInicialArray = System.nanoTime();
		
		for (int i = 0; i < numObjACrear; i++) {
			int[] array = {0, 0, 0};
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", array, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
			comprovacioRendimentTmp.addListArr(wd1);

		}
		
		long tempsFinalArray = System.nanoTime();
		tempsMilisegons = TimeUnit.MILLISECONDS.convert((tempsFinalArray - tempsInicialArray), TimeUnit.NANOSECONDS);
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en l'ArrayList: " + tempsMilisegons);
		
		long tempsInicialLinked = System.nanoTime();
		
		for (int i = 0; i < numObjACrear; i++) {
			int[] array = {0, 0, 0};
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			Waypoints_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", array, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
			comprovacioRendimentTmp.addListLinked(wd1);
		}
		
		long tempsFinalLinked = System.nanoTime();
		tempsMilisegons = TimeUnit.MILLISECONDS.convert((tempsFinalLinked - tempsInicialLinked), TimeUnit.NANOSECONDS);
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en el LinkedList: " + tempsMilisegons);
		
		return comprovacioRendimentTmp;
	}
	
	// Métode 3
	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		// Declaració de variables
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int[] array = {0, 0, 0};
		Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", array, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
		int meitatLlista = comprovacioRendimentTmp.llistaArrayList.size() / 2;
	}
}
