package pe3_examen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class Exercici2 {

	public static void main(String[] args) {
		// Declaració de variables
			ArrayList<File> llistaArxius = new ArrayList<File>();
			File file1 = new File("prova1.txt");
			File file2 = new File("prova2.txt");
			FileReader fr = null;
			BufferedReader br;
			String linia;
			
			// Afegim els fitxers a la llista
			llistaArxius.add(file1);
			llistaArxius.add(file2);
			
			// Recorrem la llista d'arxius
			for(File file : llistaArxius) {
				try {
					// Iniciem el reader i el buffer
					fr = new FileReader(file);
					br = new BufferedReader(fr);
					int contLinies = 0;
					int contCaracters = 0;
	
					// Mentres pugui llegir una línia contem com una línia més
					while((linia = br.readLine()) != null) {
						contLinies++;
						// Contem el número de caràcters
						contCaracters += linia.length();
						
					}
					System.out.println("Nom del fitxer: " + file);
					System.out.println("Número de línies: " + contLinies);
					System.out.println("Número de caràcters: " + contCaracters);
					System.out.println();
				} catch (Exception e) {
					System.out.println();
					System.out.println(e.getMessage());
					System.out.println();
				}
				
				
			}
			
			// Recorrem la llista d'arxius
			/*
			for(int i = 0; i < llistaArxius.size(); i++) {
				try {
					// Iniciem el reader i el buffer
					fr = new FileReader(llistaArxius.get(i));
					br = new BufferedReader(fr);
					int contLinies = 0;
					int contCaracters = 0;
	
					// Mentres pugui llegir una línia contem com una línia més
					while((linia = br.readLine()) != null) {
						contLinies++;
						// Contem el número de caràcters
						contCaracters += linia.length();
						
					}
					System.out.println("Nom del fitxer: " + llistaArxius.get(i));
					System.out.println("Número de línies: " + contLinies);
					System.out.println("Número de caràcters: " + contCaracters);
					System.out.println();
				} catch (Exception e) {
					System.out.println();
					System.out.println(e.getMessage());
					System.out.println();
				}
				
				
			}
			*/
	}

}
