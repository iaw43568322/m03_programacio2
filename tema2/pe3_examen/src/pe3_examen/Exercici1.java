package pe3_examen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

public class Exercici1 {

	public static void main(String[] args) throws IOException {
		// Declaració de variables
		File dirFitxersExamen = new File("fitxers_examen");
		File dirExercici1 = new File(dirFitxersExamen + "/exercici1");
		File dirPatata = new File(dirExercici1 + "/patata");
				
		// Comprobem si existeix el directori exercici1
		// En cas que no existeixi el creem
		if (!dirExercici1.isDirectory()) {
			if (dirExercici1.mkdir()) {
				System.out.println("Directori " + dirExercici1 + " creat amb èxit");
			} else {
				System.out.println("Error al crear el directori " + dirExercici1);
			}
		} else {
			System.out.println("El directori " + dirExercici1 + " ja existeix");
		}
		
		// Comprobem si existeix el directori patata dins del directori exercici1
		// En cas que no existeixi el creem
		if (!dirPatata.isDirectory()) {
			if (dirPatata.mkdir()) {
				System.out.println("Directori " + dirPatata + " creat amb èxit");
			} else {
				System.out.println("Error al crear el directori " + dirPatata);
			}
		} else {
			System.out.println("El directori " + dirPatata + " ja existeix");
		}
		// Creem el fitxer index.txt dins del directory patata
		File index = new File(dirPatata + "/index.txt");
		
		// Comprobem que existeix el fitxer index.txt
		try {
			// Escribim dues línies de text dins del fitxer index.txt
			ArrayList<String> linies = new ArrayList<String>();
			linies.add("Aquesta és la primera linia");
			linies.add("Aquesta és la segona linia");
			FileUtils.writeLines(index, linies, true);
			System.out.println("Línies escrites");
		} catch (FileNotFoundException e) {
			System.out.println();
			System.out.println("El fitxer " + index + " no existeix");
			System.out.println();
		} catch (Exception e) {
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println();
		}
		
		// Creem el file dirACopiar
		File dirACopiar = new File(dirFitxersExamen + "/dirACopiar");		
		// Copiem el contingut del directori dirACopiar al directori patata
		try {
			FileUtils.copyDirectory(dirACopiar, dirPatata);
			System.out.println("S'han copiat els fitxers del directori dirACopiar a patata");
		} catch (Exception e) {
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println();
		}
		
		// Copiem el fitxer index.txt en el directori fitxers_examen
		try {
			FileUtils.copyFileToDirectory(index, dirFitxersExamen);
			System.out.println("S'ha copiat el fitxer " + index + " al directori " + dirFitxersExamen);
		} catch (Exception e) {
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println();
		}
		
		
		// Esborrem tot el contingut del directori patata
		String[] fitxers = dirPatata.list();
		for (int i = 0; i < fitxers.length; i++) {
			File file = new File(dirPatata + "/" + fitxers[i]);
			try {
				FileUtils.forceDelete(file);
			} catch (Exception e) {
				System.out.println();
				System.out.println(e.getMessage());
				System.out.println();
			}
		}
		System.out.println("S'ha esborrat tot el contingut del directori " + dirPatata);
		
		// Copiem el fitxer index.txt al directori patata
		File index2 = new File(dirFitxersExamen + "/index.txt");
		try {
			FileUtils.copyFileToDirectory(index2, dirPatata);
			System.out.println("S'ha copiat el fitxer " + index2 + " al directori " + dirPatata);
		} catch (Exception e) {
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println();
		}
		
		// Comprobem que els dos fitxers index.txt siguin iguals
		try {
			if (FileUtils.contentEquals(index, index2)) {
				System.out.println("El fitxer " + index + " i " + index2 + " tenen el mateix contingut");
			} else {
				System.out.println("El fitxer " + index + " i " + index2 + " NO tenen el mateix contingut");
			}
		} catch (Exception e) {
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println();
		}
		


	}

}
