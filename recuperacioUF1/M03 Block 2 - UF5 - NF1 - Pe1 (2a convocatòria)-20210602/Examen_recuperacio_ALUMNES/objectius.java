

import java.util.Objects;


public class objectius{
    int objectiuID;
    String objectiuTipus;
    int objectiuPerill;
    int objectiuValor;
    double objectiuVelocitat;
    double objectiuAltura;

    
    
    public objectius(int objectiuID, String objectiuTipus, int objectiuPerill, int objectiuValor, double objectiuVelocitat, double objectiuAltura) {
        this.objectiuID = objectiuID;
        this.objectiuTipus = objectiuTipus;
        this.objectiuPerill = objectiuPerill;
        this.objectiuValor = objectiuValor;
        this.objectiuVelocitat = objectiuVelocitat;
        this.objectiuAltura = objectiuAltura;
    }

        
    public int getObjectiuID() {
        return objectiuID;
    }

    public String getObjectiuTipus() {
        return objectiuTipus;
    }

    public int getObjectiuPerill() {
        return objectiuPerill;
    }
    
    public int getObjectiuValor() {
        return objectiuValor;
    }

    public double getObjectiuVelocitat() {
        return objectiuVelocitat;
    }

    public double getObjectiuAltura() {
        return objectiuAltura;
    }

    public void setObjectiuID(int objectiuID) {
        this.objectiuID = objectiuID;
    }

    public void setObjectiuTipus(String objectiuTipus) {
        this.objectiuTipus = objectiuTipus;
    }

    public void setObjectiuPerill(int objectiuPerill) {
        this.objectiuPerill = objectiuPerill;
    }
    
    public void setObjectiuValor(int objectiuValor) {
        this.objectiuValor = objectiuValor;
    }

    public void setObjectiuVelocitat(double objectiuVelocitat) {
        this.objectiuVelocitat = objectiuVelocitat;
    }

    public void setObjectiuAltura(double objectiuAltura) {
        this.objectiuAltura = objectiuAltura;
    }
    

    

    @Override
    public String toString() {
        return "id " + objectiuID + ": Tipus = " + objectiuTipus + 
                " Perill = " + objectiuPerill + " Valor = " + objectiuValor + 
                " Velocitat = " + objectiuVelocitat + " Altura = " + objectiuAltura;
    }

   
    
    
}
