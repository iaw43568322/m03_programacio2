
class missatge {
    String missatgeCodi;
    int missatgeClauNumerica;
    String missatgeText;

    public missatge() {
    }

    public missatge(String missatgeCodi, int missatgeClauNumerica, String missatgeText) {
        this.missatgeCodi = missatgeCodi;
        this.missatgeClauNumerica = missatgeClauNumerica;
        this.missatgeText = missatgeText;
    }

    public String getMissatgeCodi() {
        return missatgeCodi;
    }

	public int getMissatgeClauNumerica() {
        return missatgeClauNumerica;
    }

    public String getMissatgeText() {
        return missatgeText;
    }

    public void setMissatgeCodi(String missatgeCodi) {
        this.missatgeCodi = missatgeCodi;
    }

    public void setMissatgeClauNumerica(int missatgeClauNumerica) {
        this.missatgeClauNumerica = missatgeClauNumerica;
    }

    public void setMissatgeText(String missatgeText) {
        this.missatgeText = missatgeText;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((missatgeCodi == null) ? 0 : missatgeCodi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		missatge other = (missatge) obj;
		if (missatgeCodi == null) {
			if (other.missatgeCodi != null)
				return false;
		} else if (!missatgeCodi.equals(other.missatgeCodi))
			return false;
		return true;
	}
    
    
    
    
}
