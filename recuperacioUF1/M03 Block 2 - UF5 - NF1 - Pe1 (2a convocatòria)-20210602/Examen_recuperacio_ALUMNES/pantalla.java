
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author gmartinez
 */
public class pantalla {
    public static void main(String[] args) {
 
        
        
        // -------- INICI EXERCICI 1 --------
        System.out.println("-------- INICI EXERCICI 1 --------");
        // Creem els diferents objectius
        objectius obj1 = new objectius(1, "caza1", 10, 10, 1000, 8000);
        objectius obj2 = new objectius(2, "caza2", 10, 10, 1000, 9000);
        objectius obj3 = new objectius(3, "radar tierra-aire", 5, 1, 0, 0);
        objectius obj4 = new objectius(4, "cazaBombardero1", 7, 8, 800, 220);
        objectius obj5 = new objectius(5, "cazaBombardero2", 7, 8, 800, 200);
        objectius obj6 = new objectius(6, "cazaBombardero3", 7, 8, 800, 180);
        objectius obj7 = new objectius(7, "pont1", 0, 2, 0, 5);
        objectius obj8 = new objectius(8, "pont2", 0, 2, 0, 0);
        // Creem la llista d'objectius i la omplim
        List<objectius> llistaObj = new ArrayList<objectius>();
        llistaObj.add(obj1);
        llistaObj.add(obj2);
        llistaObj.add(obj3);
        llistaObj.add(obj4);
        llistaObj.add(obj5);
        llistaObj.add(obj6);
        llistaObj.add(obj7);
        llistaObj.add(obj8);
        
        // Creem la llista arraydeque LIFO i la imprimim per pantalla
        System.out.println("1.3 Imprimint LIFO:");
        ArrayDeque<objectius> lifoObj = new ArrayDeque<objectius>();
        for (int i = 0; i < llistaObj.size(); i++) {
        	lifoObj.push(llistaObj.get(i));
        }
        while(lifoObj.size() > 0) {
        	System.out.println(lifoObj.getFirst());
        	lifoObj.pop();
        }
        
        // Creem la llista arraydeque FIFO i la imprimim per pantalla
        System.out.println("1.3 Imprimint FIFO:");
        ArrayDeque<objectius> fifoObj = new ArrayDeque<objectius>();
        for (int i = 0; i < llistaObj.size(); i++) {
        	fifoObj.add(llistaObj.get(i));
        }
        while(fifoObj.size() > 0) {
        	System.out.println(fifoObj.getFirst());
        	fifoObj.pop();
        }
        
        System.out.println("-------- FINAL EXERCICI 1 --------");
        // -------- FINAL EXERCICI 1 --------
        
        
        
        // -------- INICI EXERCICI 2 --------
        System.out.println();
        System.out.println();
        System.out.println("-------- INICI EXERCICI 2 --------");
        
        // Afegeixo 3 nous objectius
        objectius obj9 = new objectius(1000, "caza3", 10, 10, 1000, 8500);
        objectius obj10 = new objectius(1001, "caza4", 50, 50, 1000, 8500);
        objectius obj11 = new objectius(1002, "caza3", 50, 50, 1000, 8500);
        /*
        // Creem el treeSet i el mostrem per pantalla
        TreeSet<objectius> treeObj = new TreeSet<objectius>();
        treeObj.add(obj1);
        treeObj.add(obj2);
        treeObj.add(obj3);
        treeObj.add(obj4);
        treeObj.add(obj5);
        treeObj.add(obj6);
        treeObj.add(obj7);
        treeObj.add(obj8);
        treeObj.add(obj9);
        treeObj.add(obj10);
        treeObj.add(obj11);
        
        Iterator<objectius> i = treeObj.iterator();
        while(i.hasNext()) {
        	System.out.println(i.next());
        }
        */

        System.out.println("-------- FINAL EXERCICI 2 --------");        
        // -------- FINAL EXERCICI 2 --------
        
        
        
        // -------- INICI EXERCICI 3 --------
        System.out.println();
        System.out.println("-------- INICI EXERCICI 3 --------");
        
        String clau;
        // Creem 4 missatges
        missatge mis1 = new missatge("INI", 1, "Inici de la comunicació");
        missatge mis2 = new missatge("ESC", 2, "Esquiva enemics");
        missatge mis3 = new missatge("RETURN", 3, "Retorna a la base");
        missatge mis4 = new missatge("TER", 10, "Terminació de la comunicació");
        
        // Creem una nova llista de missatges
        List<missatge> llistaMis = new ArrayList<missatge>();
        llistaMis.add(mis1);
        llistaMis.add(mis2);
        llistaMis.add(mis3);
        llistaMis.add(mis4);
        
        // Creem el HashMap
        HashMap<String, missatge> hmMis = new HashMap<String, missatge>();
        hmMis.put(mis1.getMissatgeCodi(), mis1);
        hmMis.put(mis2.getMissatgeCodi(), mis2);
        hmMis.put(mis3.getMissatgeCodi(), mis3);
        hmMis.put(mis4.getMissatgeCodi(), mis4);
        System.out.println("Contingut del missatgesMapa a saco = " + hmMis);
        System.out.println();
        
        // Mostrem les claus del map
        System.out.println("3.2 missatgesMapa.keySet() a saco = " + hmMis.keySet());
        Iterator<String> it = hmMis.keySet().iterator();
        System.out.println("3.2 missatgeMapa.keySet() per bucle for:");
        while(it.hasNext()) {
        	clau = it.next();
        	System.out.println(clau);
        }
        System.out.println();
        
        // Comprovem si el hashMap conté la clas ESC i STOP
        System.out.println("3.3 Conté la clau ESC ?: " + hmMis.containsKey("ESC"));
        System.out.println("3.3 Conté la clau STOP ?: " + hmMis.containsKey("STOP"));
        System.out.println();
        // Imprimim tota la informació dels missatges
        System.out.println("3.4 Dades dels missatges:");
        Iterator<String> it2 = hmMis.keySet().iterator();
        while(it2.hasNext()) {
        	clau = it2.next();
            System.out.println("KEY = " + clau + "\nmissatgeClauNumerica = " + hmMis.get(clau).getMissatgeClauNumerica() + 
            		"\nmissatgeText = " + hmMis.get(clau).getMissatgeText());
            System.out.println();
        }
        
        // Creem dos missatges nous i comprovem si són iguals
        missatge mis5 = new missatge("ESC", 2, "Esquiva enemics");
        missatge mis6 = new missatge("ESC", 3, "Ataca enemics");
        
        System.out.println("missatge5.equals(missatge6) ?: " + mis5.equals(mis6));

        

        
        System.out.println("-------- FINAL EXERCICI 3 --------");
        // -------- FINAL EXERCICI 3 --------
        
        
        
        
    }
    
}
