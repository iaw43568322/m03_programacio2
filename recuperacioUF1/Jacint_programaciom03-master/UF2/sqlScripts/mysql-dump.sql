-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: hcalendar
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hcalendar`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hcalendar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `hcalendar`;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colors` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `red` int NOT NULL,
  `green` int NOT NULL,
  `blue` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'IndianRed',205,92,92),(2,'LightCoral',240,128,128),(3,'Salmon',250,128,114),(4,'DarkSalmon',233,150,122),(5,'LightSalmon',255,160,122),(6,'Crimson',220,20,60),(7,'Red',255,0,0),(8,'FireBrick',178,34,34),(9,'DarkRed',139,0,0),(10,'Pink',255,192,203),(11,'LightPink',255,182,193),(12,'HotPink',255,105,180),(13,'DeepPink',255,20,147),(14,'MediumVioletRed',199,21,133),(15,'PaleVioletRed',219,112,147),(16,'LightSalmon',255,160,122),(17,'Coral',255,127,80),(18,'Tomato',255,99,71),(19,'OrangeRed',255,69,0),(20,'DarkOrange',255,140,0),(21,'Orange',255,165,0),(22,'Gold',255,215,0),(23,'Yellow',255,255,0),(24,'LightYellow',255,255,224),(25,'LemonChiffon',255,250,205),(26,'LightGoldenrodYellow',250,250,210),(27,'PapayaWhip',255,239,213),(28,'Moccasin',255,228,181),(29,'PeachPuff',255,218,185),(30,'PaleGoldenrod',238,232,170),(31,'Khaki',240,230,140),(32,'DarkKhaki',189,183,107),(33,'Lavender',230,230,250),(34,'Thistle',216,191,216),(35,'Plum',221,160,221),(36,'Violet',238,130,238),(37,'Orchid',218,112,214),(38,'Fuchsia',255,0,255),(39,'Magenta',255,0,255),(40,'MediumOrchid',186,85,211),(41,'MediumPurple',147,112,219),(42,'BlueViolet',138,43,226),(43,'DarkViolet',148,0,211),(44,'DarkOrchid',153,50,204),(45,'DarkMagenta',139,0,139),(46,'Purple',128,0,128),(47,'Indigo',75,0,130),(48,'SlateBlue',106,90,205),(49,'DarkSlateBlue',72,61,139),(50,'GreenYellow',173,255,47),(51,'Chartreuse',127,255,0),(52,'LawnGreen',124,252,0),(53,'Lime',0,255,0),(54,'LimeGreen',50,205,50),(55,'PaleGreen',152,251,152),(56,'LightGreen',144,238,144),(57,'MediumSpringGreen',0,250,154),(58,'SpringGreen',0,255,127),(59,'MediumSeaGreen',60,179,113),(60,'SeaGreen',46,139,87),(61,'ForestGreen',34,139,34),(62,'Green',0,128,0),(63,'DarkGreen',0,100,0),(64,'YellowGreen',154,205,50),(65,'OliveDrab',107,142,35),(66,'Olive',128,128,0),(67,'MediumAquamarine',102,205,170),(68,'DarkSeaGreen',143,188,143),(69,'LightSeaGreen',32,178,170),(70,'DarkCyan',0,139,139),(71,'Teal',0,128,128),(72,'Aqua',0,255,255),(73,'Cyan',0,255,255),(74,'LightCyan',224,255,255),(75,'PaleTurquoise',175,238,238),(76,'Aquamarine',127,255,212),(77,'Turquoise',64,224,208),(78,'MediumTurquoise',72,209,204),(79,'DarkTurquoise',0,206,209),(80,'CadetBlue',95,158,160),(81,'SteelBlue',70,130,180),(82,'LightSteelBlue',176,196,222),(83,'PowderBlue',176,224,230),(84,'LightBlue',173,216,230),(85,'SkyBlue',135,206,235),(86,'LightSkyBlue',135,206,250),(87,'DeepSkyBlue',0,191,255),(88,'DodgerBlue',30,144,255),(89,'CornflowerBlue',100,149,237),(90,'MediumSlateBlue',123,104,238),(91,'RoyalBlue',65,105,225),(92,'Blue',0,0,255),(93,'MediumBlue',0,0,205),(94,'DarkBlue',0,0,139),(95,'Navy',0,0,128),(96,'MidnightBlue',25,25,112),(97,'Cornsilk',255,248,220),(98,'BlanchedAlmond',255,235,205),(99,'Bisque',255,228,196),(100,'NavajoWhite',255,222,173),(101,'Wheat',245,222,179),(102,'BurlyWood',222,184,135),(103,'Tan',210,180,140),(104,'RosyBrown',188,143,143),(105,'SandyBrown',244,164,96),(106,'Goldenrod',218,165,32),(107,'DarkGoldenrod',184,134,11),(108,'Peru',205,133,63),(109,'Chocolate',210,105,30),(110,'SaddleBrown',139,69,19),(111,'Sienna',160,82,45),(112,'Brown',165,42,42),(113,'Maroon',128,0,0),(114,'White',255,255,255),(115,'Snow',255,250,250),(116,'Honeydew',240,255,240),(117,'MintCream',245,255,250),(118,'Azure',240,255,255),(119,'AliceBlue',240,248,255),(120,'GhostWhite',248,248,255),(121,'WhiteSmoke',245,245,245),(122,'Seashell',255,245,238),(123,'Beige',245,245,220),(124,'OldLace',253,245,230),(125,'FloralWhite',255,250,240),(126,'Ivory',255,255,240),(127,'AntiqueWhite',250,235,215),(128,'Linen',250,240,230),(129,'LavenderBlush',255,240,245),(130,'MistyRose',255,228,225),(131,'Gainsboro',220,220,220),(132,'LightGrey',211,211,211),(133,'Silver',192,192,192),(134,'DarkGray',169,169,169),(135,'Gray',128,128,128),(136,'DimGray',105,105,105),(137,'LightSlateGray',119,136,153),(138,'SlateGray',112,128,144),(139,'DarkSlateGray',47,79,79),(140,'Black',0,0,0);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `place` varchar(50) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `backgroundColor` bigint NOT NULL,
  `textColor` bigint NOT NULL,
  `visible` bit(1) NOT NULL,
  `registrationDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlijrdyb5brym67tlvg6pmc6n5` (`backgroundColor`),
  KEY `FKlq1fcp3ejjup9ebfklfoj7n13` (`textColor`),
  CONSTRAINT `FKlijrdyb5brym67tlvg6pmc6n5` FOREIGN KEY (`backgroundColor`) REFERENCES `colors` (`id`),
  CONSTRAINT `FKlq1fcp3ejjup9ebfklfoj7n13` FOREIGN KEY (`textColor`) REFERENCES `colors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Curso Java SE 15 - Sesión 1','2021-05-26','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 1 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(2,'Curso Java SE 15 - Sesión 2','2021-06-29','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 2 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(3,'Curso Java SE 15 - Sesión 3','2021-06-30','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 3 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(4,'Curso Java SE 15 - Sesión 4','2021-07-01','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 4 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(5,'Curso Java SE 15 - Sesión 5','2021-07-02','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 5 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(6,'Curso Java SE 15 - Sesión 6','2021-07-03','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 6 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(7,'Cena de Nochevieja 2020','2020-12-31','21:30:00','23:30:00','Restaurante El Torreón','Cena familiar anual de Nochevieja',5,80,_binary '','2015-05-01 09:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'hcalendar'
--

--
-- Dumping routines for database 'hcalendar'
--

--
-- Current Database: `calendar`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `calendar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `calendar`;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colors` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `red` int unsigned NOT NULL,
  `green` int unsigned NOT NULL,
  `blue` int unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'IndianRed',205,92,92),(2,'LightCoral',240,128,128),(3,'Salmon',250,128,114),(4,'DarkSalmon',233,150,122),(5,'LightSalmon',255,160,122),(6,'Crimson',220,20,60),(7,'Red',255,0,0),(8,'FireBrick',178,34,34),(9,'DarkRed',139,0,0),(10,'Pink',255,192,203),(11,'LightPink',255,182,193),(12,'HotPink',255,105,180),(13,'DeepPink',255,20,147),(14,'MediumVioletRed',199,21,133),(15,'PaleVioletRed',219,112,147),(16,'LightSalmon',255,160,122),(17,'Coral',255,127,80),(18,'Tomato',255,99,71),(19,'OrangeRed',255,69,0),(20,'DarkOrange',255,140,0),(21,'Orange',255,165,0),(22,'Gold',255,215,0),(23,'Yellow',255,255,0),(24,'LightYellow',255,255,224),(25,'LemonChiffon',255,250,205),(26,'LightGoldenrodYellow',250,250,210),(27,'PapayaWhip',255,239,213),(28,'Moccasin',255,228,181),(29,'PeachPuff',255,218,185),(30,'PaleGoldenrod',238,232,170),(31,'Khaki',240,230,140),(32,'DarkKhaki',189,183,107),(33,'Lavender',230,230,250),(34,'Thistle',216,191,216),(35,'Plum',221,160,221),(36,'Violet',238,130,238),(37,'Orchid',218,112,214),(38,'Fuchsia',255,0,255),(39,'Magenta',255,0,255),(40,'MediumOrchid',186,85,211),(41,'MediumPurple',147,112,219),(42,'BlueViolet',138,43,226),(43,'DarkViolet',148,0,211),(44,'DarkOrchid',153,50,204),(45,'DarkMagenta',139,0,139),(46,'Purple',128,0,128),(47,'Indigo',75,0,130),(48,'SlateBlue',106,90,205),(49,'DarkSlateBlue',72,61,139),(50,'GreenYellow',173,255,47),(51,'Chartreuse',127,255,0),(52,'LawnGreen',124,252,0),(53,'Lime',0,255,0),(54,'LimeGreen',50,205,50),(55,'PaleGreen',152,251,152),(56,'LightGreen',144,238,144),(57,'MediumSpringGreen',0,250,154),(58,'SpringGreen',0,255,127),(59,'MediumSeaGreen',60,179,113),(60,'SeaGreen',46,139,87),(61,'ForestGreen',34,139,34),(62,'Green',0,128,0),(63,'DarkGreen',0,100,0),(64,'YellowGreen',154,205,50),(65,'OliveDrab',107,142,35),(66,'Olive',128,128,0),(67,'MediumAquamarine',102,205,170),(68,'DarkSeaGreen',143,188,143),(69,'LightSeaGreen',32,178,170),(70,'DarkCyan',0,139,139),(71,'Teal',0,128,128),(72,'Aqua',0,255,255),(73,'Cyan',0,255,255),(74,'LightCyan',224,255,255),(75,'PaleTurquoise',175,238,238),(76,'Aquamarine',127,255,212),(77,'Turquoise',64,224,208),(78,'MediumTurquoise',72,209,204),(79,'DarkTurquoise',0,206,209),(80,'CadetBlue',95,158,160),(81,'SteelBlue',70,130,180),(82,'LightSteelBlue',176,196,222),(83,'PowderBlue',176,224,230),(84,'LightBlue',173,216,230),(85,'SkyBlue',135,206,235),(86,'LightSkyBlue',135,206,250),(87,'DeepSkyBlue',0,191,255),(88,'DodgerBlue',30,144,255),(89,'CornflowerBlue',100,149,237),(90,'MediumSlateBlue',123,104,238),(91,'RoyalBlue',65,105,225),(92,'Blue',0,0,255),(93,'MediumBlue',0,0,205),(94,'DarkBlue',0,0,139),(95,'Navy',0,0,128),(96,'MidnightBlue',25,25,112),(97,'Cornsilk',255,248,220),(98,'BlanchedAlmond',255,235,205),(99,'Bisque',255,228,196),(100,'NavajoWhite',255,222,173),(101,'Wheat',245,222,179),(102,'BurlyWood',222,184,135),(103,'Tan',210,180,140),(104,'RosyBrown',188,143,143),(105,'SandyBrown',244,164,96),(106,'Goldenrod',218,165,32),(107,'DarkGoldenrod',184,134,11),(108,'Peru',205,133,63),(109,'Chocolate',210,105,30),(110,'SaddleBrown',139,69,19),(111,'Sienna',160,82,45),(112,'Brown',165,42,42),(113,'Maroon',128,0,0),(114,'White',255,255,255),(115,'Snow',255,250,250),(116,'Honeydew',240,255,240),(117,'MintCream',245,255,250),(118,'Azure',240,255,255),(119,'AliceBlue',240,248,255),(120,'GhostWhite',248,248,255),(121,'WhiteSmoke',245,245,245),(122,'Seashell',255,245,238),(123,'Beige',245,245,220),(124,'OldLace',253,245,230),(125,'FloralWhite',255,250,240),(126,'Ivory',255,255,240),(127,'AntiqueWhite',250,235,215),(128,'Linen',250,240,230),(129,'LavenderBlush',255,240,245),(130,'MistyRose',255,228,225),(131,'Gainsboro',220,220,220),(132,'LightGrey',211,211,211),(133,'Silver',192,192,192),(134,'DarkGray',169,169,169),(135,'Gray',128,128,128),(136,'DimGray',105,105,105),(137,'LightSlateGray',119,136,153),(138,'SlateGray',112,128,144),(139,'DarkSlateGray',47,79,79),(140,'Black',0,0,0);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `place` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `backgroundColor` bigint NOT NULL,
  `textColor` bigint NOT NULL,
  `visible` bit(1) NOT NULL,
  `registrationDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `backgroundColor_idx` (`backgroundColor`),
  KEY `textColor_idx` (`textColor`),
  CONSTRAINT `backgroundColor` FOREIGN KEY (`backgroundColor`) REFERENCES `colors` (`id`),
  CONSTRAINT `textColor` FOREIGN KEY (`textColor`) REFERENCES `colors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Curso Java SE 15 - Sesión 1','2021-05-26','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 1 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(2,'Curso Java SE 15 - Sesión 2','2021-06-29','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 2 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(3,'Curso Java SE 15 - Sesión 3','2021-06-30','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 3 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(4,'Curso Java SE 15 - Sesión 4','2021-07-01','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 4 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(5,'Curso Java SE 15 - Sesión 5','2021-07-02','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 5 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(6,'Curso Java SE 15 - Sesión 6','2021-07-03','09:00:00','14:00:00','Aula 3 PUE','Sesión presencial 6 del curso d´Ensenyament',1,40,_binary '','2015-05-01 09:00:00'),(7,'Cena de Nochevieja 2020','2020-12-31','21:30:00','23:30:00','Restaurante El Torreón','Cena familiar anual de Nochevieja',5,80,_binary '','2015-05-01 09:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'calendar'
--

--
-- Dumping routines for database 'calendar'
--
/*!50003 DROP PROCEDURE IF EXISTS `getColorById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getColorById`(
	IN id BIGINT
)
BEGIN

SELECT id,name,red,green,blue FROM Colors c WHERE c.id = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getColors` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getColors`()
BEGIN

SELECT id, name, red, green, blue FROM colors;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getEventById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getEventById`(
   IN id BIGINT
)
BEGIN

   SELECT e.id, e.name, e.date, e.startTime, e.endTime, e.place, e.description, e.backgroundColor, e.textColor, e.visible, e.registrationDate,
          bc.id, bc.name, bc.red, bc.green, bc.blue, 
          tc.id, tc.name, tc.red, tc.green, tc.blue 
   FROM events e 
   INNER JOIN Colors bc ON e.backgroundColor = bc.id 
   INNER JOIN Colors tc ON e.textColor = tc.id 
   WHERE e.id = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getEvents` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getEvents`()
BEGIN

 SELECT e.id, e.name, e.date, e.startTime, e.endTime, e.place, e.description, e.backgroundColor, e.textColor, e.visible, e.registrationDate,
          bc.id, bc.name, bc.red, bc.green, bc.blue, 
          tc.id, tc.name, tc.red, tc.green, tc.blue 
   FROM events e 
   INNER JOIN Colors bc ON e.backgroundColor = bc.id 
   INNER JOIN Colors tc ON e.textColor = tc.id;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `myvueling`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `myvueling` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `myvueling`;

--
-- Table structure for table `airports`
--

DROP TABLE IF EXISTS `airports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `airports` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `codeIATA` varchar(3) NOT NULL,
  `codeICAO` varchar(4) NOT NULL,
  `city` bigint NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int NOT NULL,
  `runways` int NOT NULL,
  `height` double NOT NULL,
  `website` varchar(250) DEFAULT NULL,
  `location` bigint NOT NULL,
  `active` bit(1) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codeIATA_UNIQUE` (`codeIATA`),
  UNIQUE KEY `codeICAO_UNIQUE` (`codeICAO`),
  KEY `city_fk_idx` (`city`),
  KEY `location_fk_idx` (`location`),
  CONSTRAINT `airport_city_fk` FOREIGN KEY (`city`) REFERENCES `cities` (`id`),
  CONSTRAINT `airport_location_fk` FOREIGN KEY (`location`) REFERENCES `locations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airports`
--

LOCK TABLES `airports` WRITE;
/*!40000 ALTER TABLE `airports` DISABLE KEYS */;
INSERT INTO `airports` VALUES (1,'BCN','LEBL',1,'Aeropuerto de Barcelona - El Prat',4,3,3.8,'http://www.aena.es/csee/Satellite/Aeropuerto-Barcelona/es/',9,_binary '','2015-01-29 18:00:00',NULL),(2,'MAD','LEMD',2,'Aeropuerto Adolfo Suárez Madrid-Barajas',4,4,611,'http://www.aena.es/csee/Satellite/Aeropuerto-Madrid-Barajas/es/',10,_binary '','2015-01-29 18:00:00',NULL),(3,'PMI','LEPA',3,'Aeropuerto de Palma de Mallorca',4,2,8,'http://www.aena.es/csee/Satellite/Aeropuerto-Palma-Mallorca/es/',11,_binary '','2015-01-29 18:00:00',NULL),(4,'JFK','KJFK',4,'Aeropuerto Internacional John F. Kennedy',4,4,4,'http://www.KennedyAirport.com',12,_binary '','2015-01-29 18:00:00',NULL),(5,'LHR','EGLL',5,'Aeropuerto de Londres-Heathrow',4,2,25,'http://heathrowairport.com',13,_binary '','2015-01-29 18:00:00',NULL),(6,'ACE','GCRR',6,'Aeropuerto de Lanzarote',4,1,14,'http://www.aena.es/csee/Satellite/Aeropuerto-Lanzarote/es/Lanzarote.html',64,_binary '','2015-01-29 18:00:00',NULL),(7,'AGP','LEMG',7,'Aeropuerto de Málaga-Costa del Sol',4,2,16,'http://www.aena.es/csee/Satellite/Aeropuerto-Malaga/en/Malaga-Costa-del-Sol.html',65,_binary '','2015-01-29 18:00:00',NULL),(8,'ALC','LEAL',8,'Aeropuerto de Alicante-Elche',2,1,43,'http://www.aena.es/csee/Satellite/Aeropuerto-Alicante/es/Page/1049437849204/Presentacion.html',66,_binary '','2015-01-29 18:00:00',NULL),(9,'BIO','LEBB',9,'Aeropuerto de Bilbao',3,2,42,'http://www.aena.es/csee/Satellite/Aeropuerto-Bilbao/en/Bilbao.html',67,_binary '','2015-01-29 18:00:00',NULL),(10,'EAS','LESO',10,'Aeropuerto de San Sebastián',3,1,5,'http://www.aena.es/csee/Satellite/Aeropuerto-San-Sebastian/es/',68,_binary '','2015-01-29 18:00:00',NULL),(11,'FUE','GCFV',11,'Aeropuerto de Fuerteventura',4,2,23,'http://www.aena.es/csee/Satellite/Aeropuerto-Fuerteventura/es/Inicio.html',69,_binary '','2015-01-29 18:00:00',NULL),(12,'GRX','LEGR',12,'Aeropuerto Federico García Lorca Granada-Jaén',2,1,567,'http://www.aena.es/csee/Satellite/Aeropuerto-Federico-Garcia-Lorca-Granada-Jaen/es/',70,_binary '','2015-01-29 18:00:00',NULL),(13,'IBZ','LEIB',13,'Aeropuerto de Ibiza',4,1,6,'http://www.aena.es/csee/Satellite/Aeropuerto-Ibiza/en/Ibiza.html',71,_binary '','2015-01-29 18:00:00',NULL),(14,'SCQ','LEST',14,'Aeropuerto de Santiago de Compostela',4,1,370,'http://www.aena.es/csee/ContentServer/Aeropuerto-Santiago/en/',72,_binary '','2015-01-29 18:00:00',NULL),(15,'LEI','LEAM',15,'Aeropuerto de Almería',3,1,21,'http://www.aena.es/csee/Satellite/Aeropuerto-Almeria/es/Inicio.html',73,_binary '','2015-01-29 18:00:00',NULL),(16,'LPA','GCLP',16,'Aeropuerto de Gran Canaria',4,2,23,'http://www.aena.es/csee/Satellite/Aeropuerto-Gran-Canaria/es/',74,_binary '','2015-01-29 18:00:00',NULL),(17,'MAH','LEMH',17,'Aeropuerto de Menorca',3,1,91,'http://www.aena.es/csee/Satellite/Aeropuerto-Menorca/es/',75,_binary '','2015-01-29 18:00:00',NULL),(18,'OVD','LEAS',18,'Aeropuerto de Asturias',2,1,127,'http://www.aena.es/csee/Satellite/Aeropuerto-Asturias/es/Inicio.html',76,_binary '','2015-01-29 18:00:00',NULL),(19,'SDR','LEXJ',19,'Aeropuerto de Santander',2,1,5,'http://www.aena.es/csee/Satellite/Aeropuerto-Santander/es/',77,_binary '','2015-01-29 18:00:00',NULL),(20,'SPC','GCLA',20,'Aeropuerto de la Palma',3,1,33,'http://www.aena.es/csee/Satellite/Aeropuerto-La-Palma/en/Page/1049125192309/Introduction.html',78,_binary '','2015-01-29 18:00:00',NULL),(21,'SVQ','LEZL',21,'Aeropuerto de Sevilla-San Pablo',4,2,34,'http://www.sevilla-airport.com/es/index.php',79,_binary '','2015-01-29 18:00:00',NULL),(22,'TFN','GCXO',22,'Aeropuerto de Tenerife Norte',4,1,610,'http://www.aena.es/csee/Satellite/Aeropuerto-Tenerife-Norte/en/',80,_binary '','2015-01-29 18:00:00',NULL),(23,'VGO','LEVX',23,'Aeropuerto de Vigo',2,1,261,'http://www.aena.es/csee/Satellite/Aeropuerto-Vigo/en/',81,_binary '','2015-01-29 18:00:00',NULL),(24,'VLC','LEVC',24,'Aeropuerto de Valencia',3,1,69,'http://www.aena.es/csee/Satellite/Aeropuerto-Valencia/en/',82,_binary '','2015-01-29 18:00:00',NULL),(25,'VLL','LEVD',25,'Aeropuerto de Valladolid',2,1,846,'http://www.aena.es/csee/Satellite/Aeropuerto-Valladolid/es/',83,_binary '','2015-01-29 18:00:00',NULL),(26,'XRY','LEJR',26,'Aeropuerto de Cádiz-Jerez',2,1,28,'http://www.aena.es/csee/Satellite/Aeropuerto-Jerez/en/',84,_binary '','2015-01-29 18:00:00',NULL),(27,'LCG','LECO',27,'Aeropuerto de La Coruña',3,1,98,'http://www.aena.es/csee/Satellite/Aeropuerto-A-Coruna/es/Page/1048146841374/Presentacion.html',85,_binary '','2015-01-29 18:00:00',NULL),(28,'DRS','EDDC',28,'Aeropuerto de Dresde',3,1,230,'http://www.dresden-airport.de/',92,_binary '','2015-01-29 18:00:00',NULL),(29,'LEJ','EDDP',29,'Aeropuerto de Leipzig/Halle',3,2,143,'https://www.leipzig-halle-airport.de/',93,_binary '','2015-01-29 18:00:00',NULL),(30,'MUC','EDDM',30,'Aeropuerto Internacional de Múnich',4,2,453,'http://www.munich-airport.de/de/consumer/index.jsp',94,_binary '','2015-01-29 18:00:00',NULL),(31,'NUE','EDDN',31,'Aeropuerto de Núremberg',3,1,319,'http://www.airport-nuernberg.de/',95,_binary '','2015-01-29 18:00:00',NULL),(32,'STR','EDDS',32,'Aeropuerto de Stuttgart',3,1,389,'http://www.flughafen-stuttgart.de/',96,_binary '','2015-01-29 18:00:00',NULL),(33,'SXF','EDDB',33,'Aeropuerto de Berlín-Schönefeld',4,2,47,'http://www.berlin-airport.de/de/reisende-sxf/index.php',97,_binary '','2015-01-29 18:00:00',NULL),(34,'BIA','LFKB',34,'Aeropuerto de Bastia-Poretta',3,1,23,'http://www.bastia.aeroport.fr/',107,_binary '','2015-01-29 18:00:00',NULL),(35,'BOD','LFBD',35,'Aeropuerto de Burdeos',3,2,49,'http://www.bordeaux.aeroport.fr/',108,_binary '','2015-01-29 18:00:00',NULL),(36,'LBG','LFPB',36,'Aeropuerto de París-Le Bourget',3,3,66,'http://www.aeroportsdeparis.fr/',109,_binary '','2015-01-29 18:00:00',NULL),(37,'ORY','LFPO',36,'Aeropuerto de París-Orly',4,3,89,'http://www.aeroportsdeparis.fr/',110,_binary '','2015-01-29 18:00:00',NULL),(38,'CDG','LFPG',36,'Aeropuerto de París-Charles de Gaulle',4,4,119,'http://www.aeroportsdeparis.fr/',111,_binary '','2015-01-29 18:00:00',NULL),(39,'LYS','LFLL',37,'Aeropuerto de Lyon-Saint Exupéry',3,2,250,'http://www.lyonaeroports.com/',112,_binary '','2015-01-29 18:00:00',NULL),(40,'MRS','LFML',38,'Aeropuerto de Marsella-Provenza',3,2,21,'http://www.mp2.aeroport.fr/',113,_binary '','2015-01-29 18:00:00',NULL),(41,'NCE','LFMN',39,'Aeropuerto Internacional de Niza-Costa Azul',4,2,4,'http://www.nice.aeroport.fr/',114,_binary '','2015-01-29 18:00:00',NULL),(42,'NTE','LFRS',40,'Aeropuerto de Nantes Atlantique',3,1,27,'http://www.nantes.aeroport.fr/',115,_binary '','2015-01-29 18:00:00',NULL),(43,'RNS','LFRN',41,'Aeropuerto de Rennes Saint-Jacque',3,2,37,'http://www.rennes.aeroport.fr/',116,_binary '','2015-01-29 18:00:00',NULL),(44,'TLS','LFBO',42,'Aeropuerto de Toulouse-Blagnac',3,2,152,'http://www.toulouse.aeroport.fr/',117,_binary '','2015-01-29 18:00:00',NULL),(45,'AOI','LIPY',43,'Aeropuerto de Ancona-Falconara',3,1,15,'http://www.ancona-airport.com/',138,_binary '','2015-01-29 18:00:00',NULL),(46,'BDS','LIBR',44,'Aeropuerto de Brindisi',3,2,14,NULL,139,_binary '','2015-01-29 18:00:00',NULL),(47,'MXP','LIMC',45,'Aeropuerto de Milán-Malpensa',4,2,234,'http://www.milanomalpensa-airport.com/en',140,_binary '','2015-01-29 18:00:00',NULL),(48,'BLQ','LIPE',46,'Aeropuerto de Bolonia',3,1,37,NULL,141,_binary '','2015-01-29 18:00:00',NULL),(49,'BRI','LIBD',47,'Aeropuerto de Bari-Palese',3,2,54,NULL,142,_binary '','2015-01-29 18:00:00',NULL),(50,'CAG','LIEE',48,'Aeropuerto de Cagliari-Elmas',3,1,4,NULL,143,_binary '','2015-01-29 18:00:00',NULL),(51,'CTA','LICC',49,'Aeropuerto de Catania-Fontanarossa',3,1,13,NULL,144,_binary '','2015-01-29 18:00:00',NULL),(52,'FCO','LIRF',50,'Aeropuerto de Roma-Fiumicino',4,4,5,'https://www.adr.it/fiumicino',145,_binary '','2015-01-29 18:00:00',NULL),(53,'FLR','LIRQ',51,'Aeropuerto de Florencia',3,1,44,NULL,146,_binary '','2015-01-29 18:00:00',NULL),(54,'GOA','LIMJ',52,'Aeropuerto de Génova',3,1,4,NULL,147,_binary '','2015-01-29 18:00:00',NULL),(55,'LMP','LICD',53,'Aeropuerto de Lampedusa',3,1,21,NULL,148,_binary '','2015-01-29 18:00:00',NULL),(56,'NAP','LIRN',54,'Aeropuerto de Nápoles-Capodichino',3,1,90,NULL,149,_binary '','2015-01-29 18:00:00',NULL),(57,'OLB','LIEO',55,'Aeropuerto de Olbia-Costa Smeralda',3,1,37,NULL,150,_binary '','2015-01-29 18:00:00',NULL),(58,'PEG','LIRZ',56,'Aeropuerto de Perugia',3,1,493,NULL,151,_binary '','2015-01-29 18:00:00',NULL),(59,'PMO','LICJ',57,'Aeropuerto de Palermo-Punta Raisi',3,2,20,NULL,152,_binary '','2015-01-29 18:00:00',NULL),(60,'PSA','LIRP',58,'Aeropuerto Internacional Galileo Galilei',2,1,2,'http://www.pisa-airport.com/',153,_binary '','2015-01-29 18:00:00',NULL),(61,'TRN','LIMF',59,'Aeropuerto de Turín-Caselle',4,1,301,NULL,154,_binary '','2015-01-29 18:00:00',NULL),(62,'VCE','LIPZ',60,'Aeropuerto Internacional Marco Polo',4,1,2,NULL,155,_binary '','2015-01-29 18:00:00',NULL),(63,'TPS','LICT',61,'Aeropuerto de Trapani-Birgi',3,1,7,NULL,156,_binary '','2015-01-29 18:00:00',NULL),(64,'AHO','LIEA',62,'Aeropuerto de Alghero-Fertilia',3,1,27,NULL,157,_binary '','2015-01-29 18:00:00',NULL);
/*!40000 ALTER TABLE `airports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `airways`
--

DROP TABLE IF EXISTS `airways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `airways` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `fromAirport` bigint NOT NULL,
  `toAirport` bigint NOT NULL,
  `type` int NOT NULL,
  `priority` int NOT NULL,
  `estimatedFlightTime` int NOT NULL,
  `minPrice` double NOT NULL,
  `maxPrice` double NOT NULL,
  `active` bit(1) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_airports` (`fromAirport`,`toAirport`),
  KEY `airway_fromAirport_fk_idx` (`fromAirport`),
  KEY `airway_toAirport_fk_idx` (`toAirport`),
  CONSTRAINT `airway_fromAirport_fk` FOREIGN KEY (`fromAirport`) REFERENCES `airports` (`id`),
  CONSTRAINT `airway_toAirport_fk` FOREIGN KEY (`toAirport`) REFERENCES `airports` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airways`
--

LOCK TABLES `airways` WRITE;
/*!40000 ALTER TABLE `airways` DISABLE KEYS */;
INSERT INTO `airways` VALUES (1,1,2,0,0,120,60,200,_binary '','2015-01-29 18:00:00',NULL),(2,2,1,0,0,120,60,200,_binary '','2015-01-29 18:00:00',NULL),(3,1,4,1,1,800,450,2500,_binary '','2015-01-29 18:00:00',NULL),(4,4,1,1,1,800,450,2500,_binary '','2015-01-29 18:00:00',NULL),(5,1,5,1,2,300,120,500,_binary '','2015-01-29 18:00:00',NULL),(6,5,1,1,2,300,120,500,_binary '','2015-01-29 18:00:00',NULL);
/*!40000 ALTER TABLE `airways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `country` bigint NOT NULL,
  `name` varchar(50) NOT NULL,
  `spanishName` varchar(50) NOT NULL,
  `population` int unsigned NOT NULL,
  `area` double unsigned NOT NULL,
  `location` bigint NOT NULL,
  `active` bit(1) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_fk_idx` (`country`),
  KEY `location_fk_idx` (`location`),
  CONSTRAINT `country_fk` FOREIGN KEY (`country`) REFERENCES `countries` (`id`),
  CONSTRAINT `location_fk` FOREIGN KEY (`location`) REFERENCES `locations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,1,'Barcelona','Barcelona',1602386,98.21,4,_binary '','2015-01-29 18:00:00',NULL),(2,1,'Madrid','Madrid',3165235,605.77,5,_binary '','2015-01-29 18:00:00',NULL),(3,1,'Palma de Mallorca','Palma de Mallorca',399093,208.63,6,_binary '','2015-01-29 18:00:00',NULL),(4,2,'New York','Nueva York',8406106,1214.4,7,_binary '','2015-01-29 18:00:00',NULL),(5,3,'London','Londres',8615246,1572,8,_binary '','2015-01-29 18:00:00',NULL),(6,1,'Arrecife','Arrecife',56880,22.72,42,_binary '','2015-01-29 18:00:00',NULL),(7,1,'Málaga','Málaga',566913,398.25,43,_binary '','2015-01-29 18:00:00',NULL),(8,1,'Alicante','Alicante',332067,201.27,44,_binary '','2015-01-29 18:00:00',NULL),(9,1,'Bilbao','Bilbao',346574,40.65,45,_binary '','2015-01-29 18:00:00',NULL),(10,1,'San Sebastián','San Sebastián',186126,60.89,46,_binary '','2015-01-29 18:00:00',NULL),(11,1,'Puerto del Rosario','Puerto del Rosario',36790,289.95,47,_binary '','2015-01-29 18:00:00',NULL),(12,1,'Granada','Granada',237540,88.02,48,_binary '','2015-01-29 18:00:00',NULL),(13,1,'Ibiza','Ibiza',49693,11.14,49,_binary '','2015-01-29 18:00:00',NULL),(14,1,'Santiago de Compostela','Santiago de Compostela',95800,220,50,_binary '','2015-01-29 18:00:00',NULL),(15,1,'Almería','Almería',193351,296.21,51,_binary '','2015-01-29 18:00:00',NULL),(16,1,'Las Palmas de Gran Canaria','Las Palmas de Gran Canaria',382283,100.55,52,_binary '','2015-01-29 18:00:00',NULL),(17,1,'Mahón','Mahón',28460,117.2,53,_binary '','2015-01-29 18:00:00',NULL),(18,1,'Oviedo','Oviedo',223765,186.65,54,_binary '','2015-01-29 18:00:00',NULL),(19,1,'Santander','Santander',175736,34.76,55,_binary '','2015-01-29 18:00:00',NULL),(20,1,'Santa Cruz de La Palma','Santa Cruz de La Palma',16184,43.38,56,_binary '','2015-01-29 18:00:00',NULL),(21,1,'Sevilla','Sevilla',696676,140.8,57,_binary '','2015-01-29 18:00:00',NULL),(22,1,'Santa Cruz de Tenerife','Santa Cruz de Tenerife',205279,150.56,58,_binary '','2015-01-29 18:00:00',NULL),(23,1,'Vigo','Vigo',294997,109.06,59,_binary '','2015-01-29 18:00:00',NULL),(24,1,'Valencia','Valencia',786424,13463,60,_binary '','2015-01-29 18:00:00',NULL),(25,1,'Valladolid','Valladolid',306830,197.91,61,_binary '','2015-01-29 18:00:00',NULL),(26,1,'Jérez de la Frontera','Jérez de la Frontera',212226,1188.23,62,_binary '','2015-01-29 18:00:00',NULL),(27,1,'A Coruña','A Coruña',244810,37.83,63,_binary '','2015-01-29 18:00:00',NULL),(28,4,'Dresden','Dresde',525105,328.3,86,_binary '','2015-01-29 18:00:00',NULL),(29,4,'Leipzig','Leipzig',520838,297.4,87,_binary '','2015-01-29 18:00:00',NULL),(30,4,'München','Munich',1407836,310.43,88,_binary '','2015-01-29 18:00:00',NULL),(31,4,'Nürnberg','Nuremberg',510602,186.4,89,_binary '','2015-01-29 18:00:00',NULL),(32,4,'Stuttgart','Stuttgart',600038,207.36,90,_binary '','2015-01-29 18:00:00',NULL),(33,4,'Berlín','Berlín',3421829,891.8,91,_binary '','2015-01-29 18:00:00',NULL),(34,13,'Bastia','Bastia',43315,19.38,98,_binary '','2015-01-29 18:00:00',NULL),(35,13,'Bordeaux','Burdeos',235178,49.36,99,_binary '','2015-01-29 18:00:00',NULL),(36,13,'París','París',2249975,105.4,100,_binary '','2015-01-29 18:00:00',NULL),(37,13,'Lyon','Lyon',480660,47.87,101,_binary '','2015-01-29 18:00:00',NULL),(38,13,'Marseille','Marsella',859543,240.62,102,_binary '','2015-01-29 18:00:00',NULL),(39,13,'Nice','Niza',348721,71.92,103,_binary '','2015-01-29 18:00:00',NULL),(40,13,'Nantes','Nantes',282047,65.19,104,_binary '','2015-01-29 18:00:00',NULL),(41,13,'Rennes','Rennes',206604,50.39,105,_binary '','2015-01-29 18:00:00',NULL),(42,13,'Tolosa','Toulouse',449328,118.3,106,_binary '','2015-01-29 18:00:00',NULL),(43,19,'Ancona','Ancona',100261,123,118,_binary '','2015-01-29 18:00:00',NULL),(44,19,'Brindisi','Brindisi',89735,328,119,_binary '','2015-01-29 18:00:00',NULL),(45,19,'Milano','Milán',1345890,181.76,120,_binary '','2015-01-29 18:00:00',NULL),(46,19,'Bologna','Bolonia',383949,180,121,_binary '','2015-01-29 18:00:00',NULL),(47,19,'Bari','Bari',320146,116,122,_binary '','2015-01-29 18:00:00',NULL),(48,19,'Cagliari','Cagliari',156720,85.45,123,_binary '','2015-01-29 18:00:00',NULL),(49,19,'Catania','Catania',296453,180,124,_binary '','2015-01-29 18:00:00',NULL),(50,19,'Roma','Roma',2796102,1285.31,125,_binary '','2015-01-29 18:00:00',NULL),(51,19,'Firenze','Florencia',378236,102,126,_binary '','2015-01-29 18:00:00',NULL),(52,19,'Génova','Génova',594904,243.56,127,_binary '','2015-01-29 18:00:00',NULL),(53,19,'Lampedusa','Lampedusa',5000,20.2,128,_binary '','2015-01-29 18:00:00',NULL),(54,19,'Napoli','Nápoles',957838,117.27,129,_binary '','2015-01-29 18:00:00',NULL),(55,19,'Olbia','Olbia',55477,376.1,130,_binary '','2015-01-29 18:00:00',NULL),(56,19,'Perugia','Perugia',166667,449,131,_binary '','2015-01-29 18:00:00',NULL),(57,19,'Palermo','Palermo',658112,158,132,_binary '','2015-01-29 18:00:00',NULL),(58,19,'Pisa','Pisa',86052,185,133,_binary '','2015-01-29 18:00:00',NULL),(59,19,'Torino','Turin',911823,130,134,_binary '','2015-01-29 18:00:00',NULL),(60,19,'Venezia','Venecia',270884,414.6,135,_binary '','2015-01-29 18:00:00',NULL),(61,19,'Trapani','Trapani',70531,271,136,_binary '','2015-01-29 18:00:00',NULL),(62,19,'Alghero','Alguer',40803,224.43,137,_binary '','2015-01-29 18:00:00',NULL);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `codeISO2` varchar(2) NOT NULL,
  `codeISO3` varchar(3) NOT NULL,
  `codeISONum` varchar(3) NOT NULL,
  `nativeName` varchar(50) NOT NULL,
  `englishName` varchar(50) NOT NULL,
  `spanishName` varchar(50) NOT NULL,
  `location` bigint NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codeISO2_UNIQUE` (`codeISO2`),
  UNIQUE KEY `codeISO3_UNIQUE` (`codeISO3`),
  UNIQUE KEY `codeISONum_UNIQUE` (`codeISONum`),
  KEY `location_idx` (`location`),
  CONSTRAINT `location` FOREIGN KEY (`location`) REFERENCES `locations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'PP','PPP','724','España','Spain','España',1,'2015-01-29 18:00:00','2015-02-09 16:33:51'),(2,'US','USA','840','United States','United States','Estados Unidos',2,'2015-01-29 18:00:00',NULL),(3,'GB','GBR','826','United Kingdom','United Kingdom','Reino Unido',3,'2015-01-29 18:00:00',NULL),(4,'DE','DEU','276','Deutschland','Germany','Alemania',14,'2015-01-29 18:00:00',NULL),(5,'DZ','DZA','012','Al-Jaza´ir','Algeria','Argelia',15,'2015-01-29 18:00:00',NULL),(6,'AM','ARM','051','Hayastán','Armenia','Armenia',16,'2015-01-29 18:00:00',NULL),(7,'AT','AUT','040','Österreich','Austria','Austria',17,'2015-01-29 18:00:00',NULL),(8,'BY','BLR','112','Belarus','Belarus','Bielorussia',18,'2015-01-29 18:00:00',NULL),(9,'BG','BGR','100','Bulgariya','Bulgaria','Bulgaria',19,'2015-01-29 18:00:00',NULL),(10,'CY','CYP','196','Kypros','Cyprus','Chipre',20,'2015-01-29 18:00:00',NULL),(11,'HR','HRV','191','Hrvatska','Croatia','Croacia',21,'2015-01-29 18:00:00',NULL),(12,'DK','DNK','208','Danmark','Denmark','Dinamarca',22,'2015-01-29 18:00:00',NULL),(13,'FR','FRA','250','France','France','Francia',23,'2015-01-29 18:00:00',NULL),(14,'GR','GRC','300','Hellas','Greece','Grecia',24,'2015-01-29 18:00:00',NULL),(15,'HU','HUN','348','Magyarország','Hungary','Hungria',25,'2015-01-29 18:00:00',NULL),(16,'IE','IRL','372','Éire','Ireland','Irlanda',26,'2015-01-29 18:00:00',NULL),(17,'IS','ISL','352','Ísland','Iceland','Islandia',27,'2015-01-29 18:00:00',NULL),(18,'IL','ISR','376','Yisrael','Israel','Israel',28,'2015-01-29 18:00:00',NULL),(19,'IT','ITA','380','Italia','Italy','Italia',29,'2015-01-29 18:00:00',NULL),(20,'MT','MLT','470','Malta','Malta','Malta',30,'2015-01-29 18:00:00',NULL),(21,'MA','MAR','504','Amerruk','Morocco','Marruecos',31,'2015-01-29 18:00:00',NULL),(22,'NO','NOR','578','Norge','Norway','Noruega',32,'2015-01-29 18:00:00',NULL),(23,'PL','POL','616','Polska','Poland','Polonia',33,'2015-01-29 18:00:00',NULL),(24,'PT','PRT','620','Portugal','Portugal','Portugal',34,'2015-01-29 18:00:00',NULL),(25,'RO','ROU','642','România','Romania','Rumanía',35,'2015-01-29 18:00:00',NULL),(26,'RU','RUS','643','Rossiya','Russia','Rusia',36,'2015-01-29 18:00:00',NULL),(27,'RS','SRB','688','Srbija','Serbia','Serbia',37,'2015-01-29 18:00:00',NULL),(28,'SE','SWE','752','Sverige','Sweden','Suecia',38,'2015-01-29 18:00:00',NULL),(29,'CH','CHE','756','Schweiz','Switzerland','Suiza',39,'2015-01-29 18:00:00',NULL),(30,'TR','TUR','792','Türkiye','Turkey','Turquía',40,'2015-01-29 18:00:00',NULL),(31,'UA','UKR','804','Ukraina','Ukraine','Ucrania',41,'2015-01-29 18:00:00',NULL),(42,'XX','XXX','001','My Country','My Country','Mi País',169,'2015-02-09 16:36:20',NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,58.3,28.4),(2,37.09024,-95.712891),(3,55.378051,-3.435973),(4,41.3850639,2.1734034999999494),(5,40.4167754,-3.703790199999957),(6,39.57119,2.6466339),(7,40.7143528,-74.005973),(8,51.5112138,-0.11982439),(9,41.296944,2.078333),(10,40.484118,-3.567743),(11,39.551667,2.738889),(12,40.639722,-73.778889),(13,51.4775,-0.461389),(14,51.165691,10.451526),(15,28.033886,1.659626),(16,40.069099,45.038189),(17,47.516231,14.550072),(18,53.709807,27.953389),(19,42.733883,25.48583),(20,35.126413,33.429859),(21,45.1,15.2),(22,56.26392,9.501785),(23,46.227638,2.213749),(24,39.074208,21.824312),(25,47.162494,19.503304),(26,53.41291,-8.24389),(27,64.963051,-19.020835),(28,31.046051,34.851612),(29,41.87194,12.56738),(30,35.937496,14.375416),(31,31.791702,-7.09262),(32,60.472024,8.468946),(33,51.919438,19.145136),(34,39.399872,-8.224454),(35,45.943161,24.96676),(36,61.52401,105.318756),(37,44.016521,21.005859),(38,60.128161,18.643501),(39,46.818188,8.227512),(40,38.963745,35.243322),(41,48.379433,31.16558),(42,28.9651694,-13.555036299999983),(43,36.721261,4.421265500000004),(44,38.3459963,-0.4906855000000405),(45,43.2566901,-2.924061599999959),(46,43.318334,-1.9812312999999904),(47,28.5013884,-13.865105599999993),(48,37.1773363,-3.5985570999999936),(49,39.0200099,1.4821481999999833),(50,42.8782132,-8.544844499999954),(51,36.834047,-2.4637136000000055),(52,28.1235459,-15.436257399999931),(53,39.8873296,4.259619499999985),(54,43.3619145,-5.849388699999963),(55,43.46230569999999,-3.8099803000000065),(56,28.6839885,-17.764574700000026),(57,37.3880961,-5.982329899999968),(58,28.4636296,-16.251846699999987),(59,42.24059889999999,-8.720726799999966),(60,39.4699075,-0.3762881000000107),(61,41.652251,-4.724532100000033),(62,36.6936,-6.132429999999999),(63,43.3623436,-8.411540100000025),(64,28.9454994,-13.6051998),(65,36.6749001,-4.4991102),(66,38.2821999,-0.558156),(67,43.3011017,-2.91061),(68,43.3564987,-1.79061),(69,28.4526997,-13.8638),(70,37.1887016,-3.77736),(71,38.8728981,1.37312),(72,42.8963013,-8.4151402),(73,36.8438988,-2.3701),(74,27.9319,-15.3865995),(75,39.8625984,4.2186499),(76,43.5635986,-6.0346198),(77,43.4271011,-3.8200099),(78,28.6264992,-17.7556),(79,37.4179993,-5.8931098),(80,28.4827003,-16.3414993),(81,42.2318001,-8.62677),(82,39.4892998,-0.481625),(83,41.7061005,-4.8519402),(84,36.7445984,-6.0601101),(85,43.3021011,-8.3772602),(86,51.0504088,13.737262099999953),(87,51.3396955,12.373074699999961),(88,48.1351253,11.581980599999952),(89,49.45203,11.076749999999947),(90,48.7754181,9.181758800000011),(91,52.520006599999,13.404953999999975),(92,51.1328011,13.7672005),(93,51.4323997,12.2416),(94,48.3538017,11.7861004),(95,49.4986992,11.0669003),(96,48.6898994,9.219601),(97,52.3800011,13.5225),(98,42.697283,9.450880999999981),(99,44.837789,-0.5791799999999512),(100,48.856614,2.3522219000000177),(101,45.764043,4.835658999999964),(102,43.296482,5.369779999999992),(103,43.696036,7.26559199999997),(104,47.218371,-1.553621000000021),(105,48.113475,-1.675707999999986),(106,43.604652,1.4442090000000007),(107,42.5527,9.4837303),(108,44.8283005,-0.715556),(109,48.9693985,2.44139),(110,48.7252998,2.3594401),(111,49.0127983,2.55),(112,45.7263985,5.0908298),(113,43.4392719,5.2214241),(114,43.6584015,7.2158699),(115,47.1531982,-1.6107301),(116,48.0695,-1.73479),(117,43.6291008,1.36382),(118,43.6158299,13.518914999999993),(119,40.6327278,17.941761600000063),(120,45.4654542,9.186515999999983),(121,44.494887,11.342616300000032),(122,41.1171432,16.871871499999997),(123,39.2238411,9.1216613),(124,37.5080386,15.08285120000005),(125,41.8929163,12.482519899999943),(126,43.7710332,11.248000600000069),(127,44.4056499,8.946255999999948),(128,35.5005212,12.60583459999998),(129,40.8517746,14.268124400000033),(130,40.92357639999999,9.496442900000034),(131,43.1107168,12.390827899999977),(132,38.1156879,13.361267099999964),(133,43.7228386,10.401688799999988),(134,45.07098200000001,7.685676000000058),(135,45.4408474,12.31551509999997),(136,38.0176177,12.53720199999998),(137,40.5579517,8.319294899999932),(138,43.6162987,13.3622999),(139,40.6576004,17.9470005),(140,45.6306,8.7281103),(141,44.5354004,11.2887001),(142,41.1389008,16.7605991),(143,39.2514992,9.0542803),(144,37.4668007,15.0663996),(145,41.8044444,12.2508333),(146,43.8100014,11.2051001),(147,44.4132996,8.8374996),(148,35.4978981,12.6181002),(149,40.8860016,14.2908001),(150,40.8987007,9.5176296),(151,43.0959015,12.5131998),(152,38.1759987,13.0909996),(153,43.6838989,10.3927002),(154,45.2008018,7.6496301),(155,45.5052986,12.3519001),(156,37.9113998,12.4879999),(157,40.6320992,8.2907696),(169,58.3,28.4);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'myvueling'
--

--
-- Dumping routines for database 'myvueling'
--
/*!50003 DROP FUNCTION IF EXISTS `getNumOfCountries` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `getNumOfCountries`() RETURNS int
BEGIN
   DECLARE num INTEGER;

   SELECT COUNT(id) FROM countries INTO num;

	RETURN num;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteCountry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteCountry`(
	IN countryId BIGINT
)
BEGIN

DELETE FROM locations WHERE id = (SELECT location FROM countries WHERE id = countryId);
DELETE FROM countries WHERE id = countryId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAirportByCodeIATA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAirportByCodeIATA`(
	IN codeIATA VARCHAR(3)
)
BEGIN

	SELECT air.id, air.codeIATA, air.codeICAO, air.name, air.type, air.runways, air.height, air.website, air.active, air.createdAt, air.lastUpdate, airl.id, airl.latitude, airl.longitude, airci.id, airci.name, airci.spanishname, airci.population, airci.area, airci.active, airci.createdAt, airci.lastUpdate, aircil.id, aircil.latitude, aircil.longitude, aircico.id, aircico.codeiso2, aircico.codeiso3, aircico.codeisonum, aircico.nativename, aircico.englishname, aircico.spanishname, aircico.createdAt, aircico.lastUpdate, aircicol.id, aircicol.latitude, aircicol.longitude
	FROM airports air
    INNER JOIN locations airl ON air.location = airl.id
    INNER JOIN cities airci ON air.city = airci.id
    INNER JOIN locations aircil ON airci.location = aircil.id
	INNER JOIN countries aircico ON airci.country = aircico.id
	INNER JOIN locations aircicol ON aircico.location = aircicol.id
	WHERE air.codeIATA = codeIATA;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAirportByCodeICAO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAirportByCodeICAO`(
	IN codeICAO VARCHAR(4)
)
BEGIN

	SELECT air.id, air.codeIATA, air.codeICAO, air.name, air.type, air.runways, air.height, air.website, air.active, air.createdAt, air.lastUpdate, airl.id, airl.latitude, airl.longitude, airci.id, airci.name, airci.spanishname, airci.population, airci.area, airci.active, airci.createdAt, airci.lastUpdate, aircil.id, aircil.latitude, aircil.longitude, aircico.id, aircico.codeiso2, aircico.codeiso3, aircico.codeisonum, aircico.nativename, aircico.englishname, aircico.spanishname, aircico.createdAt, aircico.lastUpdate, aircicol.id, aircicol.latitude, aircicol.longitude
	FROM airports air
    INNER JOIN locations airl ON air.location = airl.id
    INNER JOIN cities airci ON air.city = airci.id
    INNER JOIN locations aircil ON airci.location = aircil.id
	INNER JOIN countries aircico ON airci.country = aircico.id
	INNER JOIN locations aircicol ON aircico.location = aircicol.id
	WHERE air.codeICAO = codeICAO;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAirportById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAirportById`(
	IN id BIGINT
)
BEGIN

	SELECT air.id, air.codeIATA, air.codeICAO, air.name, air.type, air.runways, air.height, air.website, air.active, air.createdAt, air.lastUpdate, airl.id, airl.latitude, airl.longitude, airci.id, airci.name, airci.spanishname, airci.population, airci.area, airci.active, airci.createdAt, airci.lastUpdate, aircil.id, aircil.latitude, aircil.longitude, aircico.id, aircico.codeiso2, aircico.codeiso3, aircico.codeisonum, aircico.nativename, aircico.englishname, aircico.spanishname, aircico.createdAt, aircico.lastUpdate, aircicol.id, aircicol.latitude, aircicol.longitude
	FROM airports air
    INNER JOIN locations airl ON air.location = airl.id
    INNER JOIN cities airci ON air.city = airci.id
    INNER JOIN locations aircil ON airci.location = aircil.id
	INNER JOIN countries aircico ON airci.country = aircico.id
	INNER JOIN locations aircicol ON aircico.location = aircicol.id
	WHERE air.id = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAirwayById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAirwayById`(
	IN id BIGINT
)
BEGIN

	SELECT airw.id, airw.type, airw.priority, airw.estimatedFlightTime, airw.minPrice, airw.maxPrice, airw.active, airw.createdAt, airw.lastUpdate,
		   fair.id, fair.codeIATA, fair.codeICAO, fair.name, fair.type, fair.runways, fair.height, fair.website, fair.active, fair.createdAt, fair.lastUpdate, fairl.id, fairl.latitude, fairl.longitude, fairci.id, fairci.name, fairci.spanishname, fairci.population, fairci.area, fairci.active, fairci.createdAt, fairci.lastUpdate, faircil.id, faircil.latitude, faircil.longitude, faircico.id, faircico.codeiso2, faircico.codeiso3, faircico.codeisonum, faircico.nativename, faircico.englishname, faircico.spanishname, faircico.createdAt, faircico.lastUpdate, faircicol.id, faircicol.latitude, faircicol.longitude,
           tair.id, tair.codeIATA, tair.codeICAO, tair.name, tair.type, tair.runways, tair.height, tair.website, tair.active, tair.createdAt, tair.lastUpdate, tairl.id, tairl.latitude, tairl.longitude, tairci.id, tairci.name, tairci.spanishname, tairci.population, tairci.area, tairci.active, tairci.createdAt, tairci.lastUpdate, taircil.id, taircil.latitude, taircil.longitude, taircico.id, taircico.codeiso2, taircico.codeiso3, taircico.codeisonum, taircico.nativename, taircico.englishname, taircico.spanishname, taircico.createdAt, taircico.lastUpdate, taircicol.id, taircicol.latitude, taircicol.longitude
	FROM airways airw
	INNER JOIN airports fair ON airw.fromAirport = fair.id
    INNER JOIN locations fairl ON fair.location = fairl.id
    INNER JOIN cities fairci ON fair.city = fairci.id
    INNER JOIN locations faircil ON fairci.location = faircil.id
	INNER JOIN countries faircico ON fairci.country = faircico.id
	INNER JOIN locations faircicol ON faircico.location = faircicol.id
	INNER JOIN airports tair ON airw.toAirport = tair.id
    INNER JOIN locations tairl ON fair.location = tairl.id
    INNER JOIN cities tairci ON tair.city = tairci.id
    INNER JOIN locations taircil ON tairci.location = taircil.id
	INNER JOIN countries taircico ON tairci.country = taircico.id
	INNER JOIN locations taircicol ON taircico.location = taircicol.id
	WHERE airw.id = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCityById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCityById`(
	IN id BIGINT
)
BEGIN

	SELECT ci.id, ci.name, ci.spanishname, ci.population, ci.area, ci.active, ci.createdAt, ci.lastUpdate, cil.id, cil.latitude, cil.longitude, cico.id, cico.codeiso2, cico.codeiso3, cico.codeisonum, cico.nativename, cico.englishname, cico.spanishname, cico.createdAt, cico.lastUpdate, cicol.id, cicol.latitude, cicol.longitude
	FROM cities ci
    INNER JOIN locations cil ON ci.location = cil.id
	INNER JOIN countries cico ON ci.country = cico.id
	INNER JOIN locations cicol ON cico.location = cicol.id
	WHERE ci.id = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountries` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountries`(
)
BEGIN
    
	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountriesPag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountriesPag`(
	IN offset INT,
	IN count INT
)
BEGIN
    
	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
	LIMIT offset, count;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountriesStats` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountriesStats`(
	OUT num INT
)
BEGIN

	SELECT COUNT(*) FROM countries INTO num;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountryByCodeISO2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountryByCodeISO2`(
	IN codeISO2 NVARCHAR(2)
)
BEGIN
	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
    WHERE co.codeISO2 = codeISO2;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountryByCodeISO3` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountryByCodeISO3`(
	IN codeISO3 NVARCHAR(3)
)
BEGIN
	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
    WHERE co.codeISO3 = codeISO3;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountryByCodeISONum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountryByCodeISONum`(
	IN codeISONum NVARCHAR(3)
)
BEGIN
	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
    WHERE co.codeISONum = codeISONum;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCountryById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCountryById`(
	IN id BIGINT
)
BEGIN
	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
    WHERE co.id = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertCountry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCountry`(
  IN codeISO2 VARCHAR(2),
  IN codeISO3 VARCHAR(3),
  IN codeISONum VARCHAR(3),
  IN nativeName VARCHAR(50),
  IN englishName VARCHAR(50),
  IN spanishName VARCHAR(50),
  IN latitude DOUBLE,
  IN longitude DOUBLE,
  IN createdAt DATETIME,
  IN lastUpdate DATETIME,
  OUT countryId BIGINT,
  OUT locationId BIGINT
)
BEGIN

   INSERT INTO locations(latitude, longitude) VALUES(latitude, longitude);
   SELECT LAST_INSERT_ID() INTO locationID;
  
   INSERT INTO Countries(codeISO2, codeISO3, codeISONum, nativeName, englishName, spanishName, location, createdAt, lastUpdate)
   VALUES(codeISO2, codeISO3, codeISONum, nativeName, englishName, spanishName, locationID, createdAt, lastUpdate);

   SELECT LAST_INSERT_ID() INTO countryId;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `searchCountries` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchCountries`(
	IN searchTerm NVARCHAR(50)
)
BEGIN
    SET searchTerm = CONCAT('%', searchTerm,'%');

	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
    WHERE co.codeISO2 LIKE searchTerm 
          OR co.codeISO3 LIKE searchTerm
		  OR co.codeISONum LIKE searchTerm
		  OR co.nativeName LIKE searchTerm
		  OR co.englishName LIKE searchTerm
		  OR co.spanishName LIKE searchTerm;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `searchCountriesPag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchCountriesPag`(
	IN searchTerm NVARCHAR(50),
	IN offset INTEGER,
	IN count INTEGER
)
BEGIN
    SET searchTerm = CONCAT('%', searchTerm,'%');

	SELECT co.id, co.codeISO2, co.codeISO3, co.codeISONum, co.nativeName, co.englishName, co.spanishName, co.createdAt, co.lastUpdate, col.id, col.latitude, col.longitude 
    FROM countries co
	INNER JOIN locations col ON co.location = col.id
    WHERE co.codeISO2 LIKE searchTerm 
          OR co.codeISO3 LIKE searchTerm
		  OR co.codeISONum LIKE searchTerm
		  OR co.nativeName LIKE searchTerm
		  OR co.englishName LIKE searchTerm
		  OR co.spanishName LIKE searchTerm
	LIMIT offset, count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateCountry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateCountry`(
  IN countryId BIGINT,
  IN codeISO2 VARCHAR(2),
  IN codeISO3 VARCHAR(3),
  IN codeISONum VARCHAR(3),
  IN nativeName VARCHAR(50),
  IN englishName VARCHAR(50),
  IN spanishName VARCHAR(50),
  IN latitude DOUBLE,
  IN longitude DOUBLE,
  IN lastUpdate DATETIME,
  OUT locationId BIGINT
)
BEGIN
   SELECT location INTO locationId FROM countries co WHERE co.id = countryId;
   UPDATE locations loc
   SET loc.latitude = latitude, loc.longitude = longitude
   WHERE loc.id = locationId;
   
   UPDATE Countries co
   SET co.codeISO2 = codeISO2, co.codeISO3 = codeISO3, co.codeISONum = codeISONum,
       co.nativeName = nativeName, co.englishName = englishName, co.spanishName = spanishName,
	   co.lastUpdate = lastUpdate
	WHERE co.id = countryId;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-16  9:23:25
