package exceptions0;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Exercici2 {
	public void funcio2_1() {
		try {
			FileOutputStream f = new FileOutputStream ("../docs/test.txt");
			try {
				f.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Error al tancar.");
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error al obrir.");
		}
	}
	/*
	 * 2.2: 
	 *///SecurityException - if a security manager exists and its checkWrite method denies write access to the file.
	 /* Es a dir entenc que en aquest cas no hi ha un error de permissos per que l'arxiu no existeix, per tant no
	 * pot haver-hi un error de permissos
	 * */
}
