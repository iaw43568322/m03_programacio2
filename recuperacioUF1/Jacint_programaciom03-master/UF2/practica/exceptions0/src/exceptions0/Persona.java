package exceptions0;
/*
 * EXERCICI 4:
	Modifica el codi següent per tal que setEdat() llenci una excepció de 
	tipus IllegalArgumentException si s'intenta assignar una edat negativa.
		public class Persona {
			private int edat;
	    
			public void setEdat(int edat) {
	  			this.edat = edat;
			 }
	    		...
		}
	Inclou un exemple de crida d'aquest mètode que controli correctament la 
	possible excepció.*/
public class Persona {
	private int edat;
    
	public void setEdat(int edat) {
		/*
		 */// 4
		/*
			try {
				if (edat < 0) {
					throw new IllegalArgumentException();
				} 
				this.edat = edat;
			} catch (IllegalArgumentException e) {
				System.out.println("ERROR: edat ha de ser igual o major que 0!");
			}
		*/
		// 5
		/*
		 * Modifica el codi següent per tal que setEdat() llenci una excepció de tipus 
		 * Exercici5ValidarEdatException si s'intenta assignar una edat negativa o superior 
		 * a 100 anys.
		 * 
		 * Exercici5ValidarEdatException serà una classe meva que s'extén d'Exception on 
		 * posarè els missatges que indicaran a l'usuari que ha introduit una edat 
		 * inferior a 0 o superior a 100.
			Inclou un exemple de crida d'aquest mètode que controli correctament la possible excepció.
		 * */
		try {
			if (edat < 0 || edat > 100) {
				throw new Exercici5ValidarEdatException();
			} 
			this.edat = edat;
		} catch (Exercici5ValidarEdatException e) {
			System.out.println("ERROR: edat ha de ser igual o major que 0!");
		}
	 }
}
