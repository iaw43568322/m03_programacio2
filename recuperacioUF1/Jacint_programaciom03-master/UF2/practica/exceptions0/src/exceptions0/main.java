package exceptions0;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		String b = null;
		try {
			if (b == null) {
				throw new NullPointerException();
			}
		System.out.println(b.toString());
		} catch (NullPointerException e) {
			System.out.println("tuto pene");
		}
		// EXERCICI 0.1
		System.out.println("Introdueix la id de l'usuar: ");
		Scanner sc = new Scanner(System.in);
		int user_id = sc.nextInt();
		// segon exercici:
		// segon parametre a dividir el primer pel segon, (que no sigui zero)
		
		try {
			if (user_id != 1234) {
				throw new invalidUserIdException();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		// EXERCICI 0.2
		int current_balance = 1000;
		System.out.println("----Entra l’ ingrès a dispositar ----");
		int deposit_amount=sc.nextInt();


		try{
		   if(deposit_amount<0){
		       throw new NegativeNotAllowedException();
		   }else{
		       current_balance += deposit_amount;
		       System.out.println("Updated balance is: " + current_balance);

		   }
		}catch (Exception e){
		   System.out.println(e);
		}
		
		// EXERCICI 1.1
		int[] myOutOfBounds = new int[2];
		System.out.println("1.1 - 1.3");
		System.out.println("--- Entra la posició de l'array a la qual insertar un element, l'array te 2 posicions.");
		int array_pos = sc.nextInt();
		// 1.1 genera un exception
		//// COMENTAT PER PODER IMPRIMIR EXCEPTION 
		// myOutOfBounds[array_pos] = 1;
		// 1.2 codi a executar despres de l'excepció
		// 1.3 fent que final del programa s'executi

		if (array_pos >= myOutOfBounds.length || array_pos < 0) {
			// 1.3 aquest codi s'executara avans de l'excepció si hi ha excepció, ja que esta controlant la 
			// condició d'excepció i executantse avans de que aquesta passi.
			System.out.println("Final del programa. (evitem la excepció amb l' if)");
		} else  {
			myOutOfBounds[array_pos] = 1;
			// aquest codi s'executara quan no hi ha excepció
			System.out.println("Final del programa.");
		}
		// el codi de desperes de l'error no s'executa
		System.out.println("1.4");
		// 1.4 engloba el programa en un try catch
		System.out.println("--- Entra la posició de l'array a la qual insertar un element, l'array te 2 posicions. (try/catch version)");
		int array_pos2 = sc.nextInt();
		try{
			   if(array_pos2 >= myOutOfBounds.length || array_pos2 < 0){
			       throw new ArrayIndexOutOfBoundsException();
			   }else{
				   myOutOfBounds[array_pos2] = 1;
			       System.out.println("Updated array, position " +  array_pos2 + " =  " + myOutOfBounds[array_pos]);

			   }
			   System.out.println("No mes s'executa si no hi ha exception.");
			}catch (Exception e){
			   System.out.println(e);
			}finally { System.out.println("Codi del finally.");}
		// 1.4 
		System.out.println("Final del programa 2 (try/catch)"); // aquest codi sempre s'executara ja que la excepció no interrompeix el programa
		System.out.println("1.6");
		/* 1.6. Enlloc de capturar un error  de tipus ArrayIndexOutOfBoundsException, 
		 * captura un StringIndexOutOfBoundsException, s'executarà el codi del catch? 
		 * Perquè?  I si intententes capturar Exception ? (Respon a les mateixes preguntes)
		*/	
		String a = "abc";
		// String index out of range: 3
		/*
		a.charAt(a.length());
		*/
		System.out.println("--- Entra la posició de el String 'abc' a capturar (0-2)");
		int array_pos3 = sc.nextInt();
		try {
			if (array_pos3 >= myOutOfBounds.length || array_pos3 < 0) {throw new StringIndexOutOfBoundsException();}
			else {
				a.charAt(array_pos3);
			}
		} catch (Exception e){
			   System.out.println(e);
		}finally { System.out.println("Codi del finally.");}
		// versió amb error custom
		System.out.println("--- Entra la posició de el String 'abc' a capturar (0-2) (VERSIÓ AMB ERROR CATCH CUSTOM)");
		int array_pos4 = sc.nextInt();
		try {
			if (array_pos4 >= myOutOfBounds.length || array_pos4 < 0) {throw new MyStringIndexOutOfBoundsException();}
			else {
				a.charAt(array_pos4);
			}
		} catch (Exception e){
			   System.out.println(e);
		}finally { System.out.println("Codi del finally.");}
		System.out.println("1.7");
		// 1.7 mostra les propietats de l'objecte Exception
		
		System.out.println("--- Get exception properties, type n < 0 or n > 2)");
		int array_pos5 = sc.nextInt();
		try {
			if (array_pos5 >= myOutOfBounds.length || array_pos5 < 0) {throw new StringIndexOutOfBoundsException();}
			else {
				a.charAt(array_pos5);
			}
		} catch (Exception e){
			   System.out.println("Exception: "
			   		+ "\n cause: " +  e.getCause() 
			   		+ ", \n message: " + e.getMessage() 
					+ ", \n localized message: " + e.getLocalizedMessage() 
			   		+ ", \n fillinstacktrace: " + e.fillInStackTrace() 
			   		+ ", \n stacktrace: " + e.getStackTrace() 
			   		+ ", \n hashcode: " + e.hashCode()
			   		+ ", \n getClass: " + e.getClass()
			   		);
		}
		System.out.println("1.7");
		// 1.7 same but for customized exception
		System.out.println("--- Get exception properties, type n < 0 or n > 2 (customized exception))");
		int array_pos6 = sc.nextInt();
		try {
			if (array_pos6 >= myOutOfBounds.length || array_pos6 < 0) {throw new MyStringIndexOutOfBoundsException();}
			else {
				a.charAt(array_pos6);
			}
		} catch (Exception e){
			   System.out.println("Exception: "
			   		+ "\n cause: " +  e.getCause() 
			   		+ ", \n message: " + e.getMessage() 
					+ ", \n localized message: " + e.getLocalizedMessage() 
			   		+ ", \n fillinstacktrace: " + e.fillInStackTrace() 
			   		+ ", \n stacktrace: " + e.getStackTrace() 
			   		+ ", \n hashcode: " + e.hashCode()
			   		+ ", \n getClass: " + e.getClass()
			   		);
		}
		/*
		 * 1.8. Agafa el codi que genera l'error i encapsula'l en una funció (anomenada funcio3). 
		 * Des del main crida a la funcio1, que crida a la funcio2 i que a la vegada crida la 
		 * funcio3, cada una d'aquestes funcions abans de fer la crida mostren per pantalla el 
		 * nom del mètode i després de la crida mostren un missatge de finalització del mètode. 
		 * Fixa't en el missatge d'error i la cascada de crides, en ordre invers, des del programa 
		 * principal fins a la instrucció en què s’ha produït l’excepció.
		 * */
		System.out.println("1.8");
		FuncioError1_8 f = new FuncioError1_8();
		f.funcio1(3);
		/*
		 * funcio 1
       	   funcio 2
		   funcio 3
		   
		   Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range: 3
		   at java.base/java.lang.StringLatin1.charAt(StringLatin1.java:47)
		   at java.base/java.lang.String.charAt(String.java:693)
		   at exceptions0.FuncioError1_8.funcio3(FuncioError1_8.java:6)
		   at exceptions0.FuncioError1_8.funcio2(FuncioError1_8.java:12)
		   at exceptions0.FuncioError1_8.funcio1(FuncioError1_8.java:17)
		   at exceptions0.main.main(main.java:156)
		 */
		// CONCLUSSIÓ
		// Una funció no acava fins que la cadena de cridas no s'ha producit 
		// (funció 1 espera a executar la cadena completa avans d'exectuar la seguent linea 'fi funcio 1')
		// la cadena d'errors exception es mostra de manera inversa, primer la funció que ha disparat l'error (3)
		// seguidament de les que han heretat l'error 2-1 en cadena
		/*
		 * at exceptions0.FuncioError1_8.funcio3(FuncioError1_8.java:6)
		   at exceptions0.FuncioError1_8.funcio2(FuncioError1_8.java:12)
		   at exceptions0.FuncioError1_8.funcio1(FuncioError1_8.java:17)*/
		System.out.println("1.9 - captura l'error (commentar funció 2 per a veure 1.8 error  )");
		System.out.println("--- 3 per a veure l'error, 2 per no provocar cap error");
		int n = sc.nextInt();
		f.funcio1(n);
		// 1.9 -- Observació: el codi de despres del catch no s'executa (fi crida f3) no s'executa si hi ha exception
		
		//////////////////////
		//    EXERCICI 2    //
		//////////////////////
		System.out.println("Exercic 2");
		Exercici2 e2 = new Exercici2();
		e2.funcio2_1();
		// 2.3
		// El codi no arriba a entrar en el bloc de codi de tancar l'arxiu ja que 
		// al obrir directament entra en el catch
		
		//////////////////////
		//    EXERCICI 3    //
		//////////////////////
		System.out.println("Exercic 3");
		Exercici3 e3 = new Exercici3();
		e3.divisio(0);
		
		//////////////////////
		//    EXERCICI 4 (Persona)    //
		//////////////////////
		/// ATENCIO DESCOMENTAR EL CODI A LA CLASS PERSONA I AQI
		/*
		System.out.println("Exercic 4");
		Persona p = new Persona();
		p.setEdat(-1);
		p.setEdat(10);
		*/
		
		//////////////////////
		//    EXERCICI 5 (Persona)    //
		//////////////////////
		System.out.println("Exercic 5");
		Persona p = new Persona();
		p.setEdat(-1);
		p.setEdat(10);
		p.setEdat(101);
	}
}
