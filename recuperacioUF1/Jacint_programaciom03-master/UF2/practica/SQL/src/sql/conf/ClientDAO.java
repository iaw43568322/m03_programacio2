package sql.conf;

import java.util.ArrayList;


import business.Client;


public interface ClientDAO {

	//TO-DO crea el contracte o llista de mètodes a oferir
	
		Client getClientById(int id) throws DAOException;
		ArrayList<Client> getClients() throws DAOException;
		boolean updateCliente(int id, String nombre, String direccion) throws DAOException;
//    List<Color> getColors(int offset, int count) throws DAOException;
//    List<Color> getColors(String searchTerm) throws DAOException;
//    List<Color> getColors(String searchTerm, int offset, int count) throws DAOException;
	
	//Client getClientById(int id) throws DAOException;
    
}
