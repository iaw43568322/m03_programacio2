package sql.conf;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import business.Client;
import business.Comanda;
import utilities.JDBCUtils;

public class ComandaJDBCDAO implements ComandaDAO {
	
	// Conexión a la base de datos
    private static Connection conn = null;
	
	  /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     * @throws DAOException 
     * @throws SQLException 
     * @throws IOException 
     */
    public static boolean connect() throws DAOException, SQLException, IOException {
    	
    	
        try {
        	
        	//conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tienda?serverTimezone=Europe/Paris", "root", "admin");
            //sentSQL = connection.prepareStatement("SELECT id, name, red, green, blue FROM colors WHERE id= ?");
        	conn = JDBCUtils.openConnection();
            
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        
        return conn.isValid(10);
    	
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     * @throws SQLException 
     */
    public static boolean isConnected() throws SQLException {
        // Comprobamos estado de la conexión
        return !conn.isClosed();
    }

    /**
     * Cierra la conexión con la base de datos
     * @throws SQLException 
     */
    public static void close() throws SQLException {
        conn.close();
    }
    
    public static void prtingComandes(ArrayList<Comanda> c) {
    	c.forEach(el -> System.out.println(el));
    }
    
    @Override
	public ArrayList<Comanda> getComandes() throws DAOException {
    	Comanda comanda = null;
		ArrayList<Comanda> coCol = new ArrayList<Comanda>();
        
        try (CallableStatement sentSQL = conn.prepareCall("CALL getComandes()")) {
           
            //sentSQL.setLong(1, id);
            try (ResultSet reader = sentSQL.executeQuery()) {
                while (reader.next()) {
                    // ORM: [--,--,--,--,--,--] -----> []Color
                	coCol.add(JDBCUtils.getComandes(reader));
                }            
            }
        }
        catch (SQLException ex) {
            //Logger
            throw new DAOException(ex);
        }
        return coCol;
	}
}
