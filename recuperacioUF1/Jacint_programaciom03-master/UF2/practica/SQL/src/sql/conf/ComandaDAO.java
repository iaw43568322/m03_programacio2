package sql.conf;

import java.util.ArrayList;

import business.Comanda;

public interface ComandaDAO {

	ArrayList<Comanda> getComandes() throws DAOException;
}

