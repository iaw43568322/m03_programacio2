package business;

public class Client {
	// attributes
	private long id;
	private String nombre;
	private String direccion;
	
	// constructor
	public Client(long id, String nombre, String direccion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	// getters/setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	// to string
	@Override
	public String toString() {
		return "Client [id=" + id + ", nombre=" + nombre + ", direccion=" + direccion + "]";
	}
		
}
