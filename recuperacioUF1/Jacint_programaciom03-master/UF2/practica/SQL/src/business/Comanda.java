package business;

public class Comanda {
	
	private String client;
	private String producte;
	private int quantitat;
	private double preu_unitari;
	private double total;
	public Comanda(String client, String producte, int quantitat, double preu_unitari, double total) {
		super();
		this.client = client;
		this.producte = producte;
		this.quantitat = quantitat;
		this.preu_unitari = preu_unitari;
		this.total = total;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getProducte() {
		return producte;
	}
	public void setProducte(String producte) {
		this.producte = producte;
	}
	public int getQuantitat() {
		return quantitat;
	}
	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
	public double getPreu_unitari() {
		return preu_unitari;
	}
	public void setPreu_unitari(double preu_unitari) {
		this.preu_unitari = preu_unitari;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "Comanda [client=" + client + ", producte=" + producte + ", quantitat=" + quantitat + ", preu_unitari="
				+ preu_unitari + ", total=" + total + "]";
	}

}
