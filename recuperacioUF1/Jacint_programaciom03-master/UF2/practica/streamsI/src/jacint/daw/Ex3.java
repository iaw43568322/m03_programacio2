package jacint.daw;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Ex3 {
	
	/*
	 * EXERCICI 3
		Escriu un programa que compari dos arxius de text línia per línia. 
		S'ha de llegir una línia de cada arxiu i comparar-les. Si aquestes no coincideixen, 
		s'ha de mostrar  el número de línia que falla i les dues línies que són diferents. 
		El programa processarà totes les línies de l'arxiu. 
		Fes servir els Readers / Writers (apartat 1.3. dels apunts) per a llegir i escriure en fitxer.
	 * */
	
	
	
	public static void main(String[] args) throws IOException {    
		File fitxerA = new File("ex3A.txt");     	// Adreçament relatiu     
		File fitxerB = new File("ex3B.txt");    	// Adreçament relatiu   
		FileReader frA = null;                     	// Entrada    
		FileReader frB = null;                    	// Entrada    
		BufferedReader brA;                        	// Buffer   
		BufferedReader brB;                        	// Buffer  
		String lineaA;  							// strings de linea de text
		String lineaB; 								// strings de linea de text
		// obrir i llegir arxius
		try {        
			frA = new FileReader(fitxerA);           // Inicialitza entrada del fitxer      
			frB = new FileReader(fitxerB);           // Inicialitza entrada del fitxer  
			brA = new BufferedReader(frA);           // Inicialitza buffer amb l’entrada        
			brB = new BufferedReader(frB);
			int contLineas = 1;
			//llegir lineas
			while((lineaA = brA.readLine())!= null && (lineaB = brB.readLine())!= null) {   
				// llogica de comparació de lineas i tracking de linea en concret
				if (!lineaA.equals(lineaB)) {
					System.out.println("Lineas diferents a la linea: " + contLineas);
					System.out.println("Linea A" + contLineas + ": "  + lineaA);
					System.out.println("Linea B" + contLineas + ": "  + lineaB);
				}
				contLineas++;
			}
			if ((lineaA = brA.readLine())== null && (lineaB = brB.readLine())!= null) {
				System.out.println("WARNING: Fitxer ex3A.txt acavat, fitxer ex3B.txt continua");
			} else if ((lineaA = brA.readLine())!= null && (lineaB = brB.readLine())== null) {
				System.out.println("WARNING: Fitxer ex3B.txt acavat, fitxer ex3A.txt continua");
			}
			////////////////////////////////////////////////////////////////////////////////////
			///////   WARNING: if file A only has one extra line program will not thro error
			///////			   if file B has one or more extra lines the program will throw error
			///////  This is due to the while loop reading a line at the begining
			////////////////////////////////////////////////////////////////////////////////////
		} catch (FileNotFoundException e) {        
			System.out.println("Fitxer no existeix");    
		} catch (IOException e) {        
			System.out.println(e.getMessage());    
		} finally {        
			if (frA != null) 
				frA.close();  
				frB.close();
		}
	}

}
