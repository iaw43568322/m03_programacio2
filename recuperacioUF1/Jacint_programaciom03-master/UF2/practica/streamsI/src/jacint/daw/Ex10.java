/*
 * EXERCICI 10
	Ara farem servir el plugin "Apache Commons IO".
	
	INSTALAR EL PLUGIN EN NetBeans:
	Tools/Plugins --> Downloaded --> btn Add Plugins --> filtro = *.jar i a llavors 
	trobar el fitxer "commons-io-2.5.jar" --> btn Install
	
	QUE EL NOSTRE PROJECTE VEGI EL PLUGIN:
	Quan creem el projecte, ens posem sobre "Source Packages", 
	fem click btn dret --> properties --> libraries --> btn 
	Add JAR/Folder --> sel·leccionem el fitxer "commons-io-2.5.jar" i d'aquesta manera 
	al fer un "import org.apache.commons.io.FileUtils;" ja va bé.
	
	VEURE QUE PODEM FER AMB EL PLUGIN:
	Per a veure lo que aquest plugin pot fer escriurem FileUtils. i ens sortirà una 
	llista dels atributs i mètodes que té.
 * */

package jacint.daw;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class Ex10 {
	// FUNCTION FOR EXERCISE 3
	// returns whole text content of a file
	public static String readFile(String path) throws IOException {
		return Files.readString(Paths.get(path));
	}
	// FUNCTION EXERCISE 5
	public static void directoryContainsMine(File container, File content) throws IOException {
		String fileName = content.getName();
		System.out.println( FileUtils.directoryContains(container, content) ? "Directory contains" +  fileName : "Directory doesn't contain " + fileName);
	}
	
	public static void main(String[] args) throws IOException {
		// ROOT DIRECTORY FOR WHOLE EXERCISE 10
		String ROOT_DIR = "ex10Dir";
		
		// 1. create a directory with forcemkdir
		File dir = new File(ROOT_DIR + "/directoriExercici10");
		FileUtils.forceMkdir(dir);
		
		// 2. createfitxer[1-5] with a loop (createNewFile)
		int counterFile = 1;
		while (counterFile <=5) {
			File f = new File(ROOT_DIR + "/fitxer" + counterFile);
			if (f.createNewFile()) {
				System.out.println("APARTAT 2: File: " + f.getName() + " created sucessfully.");
			} else {
				System.out.println("APARTAT 2: File: " + f.getName() + " couldn't be created. Already exists.");
			}
			counterFile++;
		}
		
		// 3.  Afegeix una llista de linies de text en els fitxers (les linies de text seràn el 
		// path + nom dels fitxers) (listFiles)(writeLines)
		// get list of files in directory
		
		System.out.println("EXERCISE 3:");
		System.out.println("Do you want to print the content of the text files? If so, press 'y' and enter.");
		Scanner read = new Scanner(System.in);
		String readFiles = read.nextLine();
		File rootD = new File(ROOT_DIR);
		File[] listOfFiles = rootD.listFiles();
		for (File myFile : listOfFiles) {
			if (myFile.isFile()) {
				List<String> line = new ArrayList<String>();
				line.add(myFile.getPath());
				FileUtils.writeLines(myFile, line);
				String content = null;
				if (readFiles.equals("y")) {
					try {
						// call to readFile function defined at the top
						content = readFile(myFile.getPath());
						System.out.println(myFile.getName());
						System.out.println(content);
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		/*
		 * Apartat 4: Mou els 5 fitxers a dins del directori "origen" (moveFileToDirectory)
		 * */
		System.out.println("EXERCISE 4: Files moved to /origen");
		// creating file directory (doesn't actually creates the file just defines it)
		File origen = new File(ROOT_DIR + "/origen");
		// iterat throguh al the files inside ex10Dir
		for (File myFile : listOfFiles) {
			// target only files, not dieractories
			if (myFile.isFile())
				// move the files AND create target directory in the proccess
				try {
					FileUtils.moveFileToDirectory(myFile, origen, true);
				} catch (IOException e) {
					System.out.println("ERROR: can't move file already exist in destination.");
				}
		}
		
		/*
		 * Apartat 5: Pregunta si el directori conté algún fitxer amb nom "fitxer3.txt" (getFile)(directoryContains)
		 * */
		System.out.println("EXERCISE 5");
		File fContains = FileUtils.getFile(ROOT_DIR + "/origen");
		File fContent = new File(ROOT_DIR + "/origen/fitxer3");
		File fContent2 = new File(ROOT_DIR + "/origen/fitxer3.txt");
		directoryContainsMine(fContains, fContent);
		directoryContainsMine(fContains, fContent2);
		
		/*
		 * Apartat 6: Esborra el fitxer "fitxer3.txt" i torna a executar l'apartat 5 (forceDelete)
		 */
		System.out.println("EXERCISE 6");
		File f3 = new File(ROOT_DIR + "/origen/fitxer3");
		f3.delete();
		directoryContainsMine(fContains, fContent);
		/*
		Apartat 7: Comproba si el fitxer "fitxer1.txt" és mes vell que "Fitxer5.txt" (isFileOlder)
		*/
		System.out.println("EXERCISE 7");
		System.out.println("isFileOlder file1 vs file5s");
		File f1 = new File(ROOT_DIR + "/origen/fitxer1");
		File f5 = new File(ROOT_DIR + "/origen/fitxer5");
		System.out.println(FileUtils.isFileOlder(f1, f5) ? "file1 is older than file 5": "file1 is newer than file5");
		/*
		Apartat 8: Copia, machacant el contingut anterior, la linia "linia sobreescrita" en el fitxer "fitxer1.txt" (writeStringToFile)
		*/
		System.out.println("EXERCISE 8");
		System.out.println("Writing to file 1");
		FileUtils.writeStringToFile(f1, "linia sobreescrita");
		System.out.println(readFile(f1.getPath()));
		/*
		Apartat 9: Comproba si el fitxer "fitxer1.txt" és mes nou que "fitxer5.txt"
		*/
		System.out.println("EXERCISE 9");
		System.out.println(FileUtils.isFileNewer(f1, f5) ? "file1 is newer than file 5": "file1 is older than file5");
		/*
		Apartat 10: Fes un touch sobre el fitxer "fitxer5.txt" (touch)
		*/
		System.out.println("EXERCISE 10");
		System.out.println("Performing touch on file 5");
		FileUtils.touch(f5);
		System.out.println(FileUtils.isFileNewer(f1, f5) ? "file1 is newer than file 5": "file1 is older than file5");
		/*
		Apartat 11: Mou el directori "origen" al directori "desti" (moveDirectoryToDirectory) i si no existeix previament "desti" 
		donçs que es crei automàticament
		*/
		System.out.println("EXERCISE 11");
		File dOrig = new File(ROOT_DIR + "/origen");
		File dDest = new File(ROOT_DIR + "/desti");
		boolean destinyExists =  !dDest.exists();
		try {
		FileUtils.moveDirectoryToDirectory(dOrig, dDest, destinyExists);
		} catch (IOException e) {
			System.out.println("Can't move desi to origen, desti already exists there.");
		}
		/*
		Apartat 12: Calcula el tamany del directori "desti" (sizeOfDirectoryAsBigInteger)
		*/
		System.out.println("EXERCISE 12");
		BigInteger sizeOfDir = FileUtils.sizeOfDirectoryAsBigInteger(dDest);
		System.out.println("The size of the directory desti is: " + sizeOfDir + " bytes");
		/*
		Apartat 13: Fer servir el iterador de FileUtils per a recorre el directori "directoriExercici10" i treure per 
		pantalla el seu contingut indicant si son fitxers o directoris (iterateFilesAndDirs).
		 * */
		System.out.println("EXERCISE 13");
		Iterator<File> files = FileUtils.iterateFilesAndDirs(dDest, 
																	TrueFileFilter.INSTANCE, 
																	TrueFileFilter.INSTANCE);
		while (files.hasNext()) {
			File myFile = files.next();
			System.out.printf(myFile.getName() + " is a");
			System.out.printf(myFile.isDirectory() ? " directory\n" : " file\n");
		}
		//////////////////////////////////////////////////////////
		///////  DELETE ALL FILES IN FILESYSTEM  /////////////////
		//////////////////////////////////////////////////////////
		////// COMMENT THIS LINE TO AVOID AUTO-DELTE ////////////
		System.out.println("Do you want to purge all files now (to avoid file collisions when executing the program twice)? (y/n)");
		String deleteFiles = read.nextLine();
		if (deleteFiles.equals("y")) {
			File[] deleteAllfiles = rootD.listFiles();
			for (File myFile : deleteAllfiles) {
				FileUtils.forceDelete(myFile);
			}
		}
	}
	
	/*
	private static void  mostrarFitxersDirectoris(String dir, int nivell, List <String> myFilesList) {
		File f = new File(dir); 
		File[] listOfFiles = f.listFiles();
		for (File myFile : listOfFiles) {
			String prefix = "";
			for (int i = 0; i < nivell; i++) {
				prefix += "-";
			}
			if (!myFile.isDirectory()) {
				myFilesList.add(prefix + "f: " +  myFile.getName());
			} else {
				myFilesList.add(prefix + "d: " +  myFile.getName());
				mostrarFitxersDirectoris(dir + "/" + myFile.getName() + "/", nivell + 1, myFilesList);
			}
		}
	}*/
}
