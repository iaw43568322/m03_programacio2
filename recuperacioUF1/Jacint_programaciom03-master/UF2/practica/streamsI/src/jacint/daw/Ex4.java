package jacint.daw;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.TreeSet;

public class Ex4 {
	
	/*
	 * EXERCICI 4
		Escriu un programa que realitza còpies de seguretat de tots els arxius 
		d'un directori mitjançant la creació d'un nou subdirectori (del directori actual) 
		anomenat backUp0 i copiant tots els arxius del directori actual al mateix. 
		Els noms d'arxiu de les còpies són les mateixes que en el directori actual. 
		Si backUp0 ja existeix, a continuació, ha de crear Backup1, i així successivament. 
		Per tal de garantir el funcionament correcte per qualsevol tipus d'arxiu 
		realitza la lectura i escriptura de fluxes orientada a byte. 
		Utilitzeu el mètode :
			- list() de la classe File per obtenir una llista d'arxius i directoris.
			- isFile() per determinar quins elements de la llista són  arxius.
			- mkdir() per crear el nou directori.
		 Si un arxiu no es pot llegir (canRead() ), cal mostrar un missatge d'advertència 
		 i continuar. Executa el programa des de la línia de comandes. 
		Fes servir la classe File (apartat 1.4.2. dels apunts)
	 * */
	
	/*
	 * 	* directori A
	 * 		* realitzar copies de tots els arxius en un subdirectori creat
	 * 			* mateix nom d'arxius
	 * 		* si backUp0 ja existeix
	 * 			* crear backUP1 (i aixis successivament)
	 * 	* lectura i escritura orientada bytes
	 * 	* !canRead()
	 * 		* mostrar missatge d'advertencia
	 * 
	 * 
	 * */
	
	
	 public static void main(String[] args) throws IOException {        
		 
		 // list of backup directories
		 Set<String> backupDirs = new TreeSet<String>();
		 // number of backup directory
		 int dirNum = '0';
		 // name of backup dir
		 String backupDirName = "backup0";
		 // NAME OF ROOT FOLDER
		 String ROOT_DIR = "ex4Dir";
		 
		 //---- TRACTAMENT DE FITXERS ---- 
		 // defining root directory
		 File f = new File(ROOT_DIR); 
		 File[] listOfFiles = f.listFiles();
		 
		 // GET NEW BACKUP DIR NAME
		 for (File myFile : listOfFiles) {
			 if (myFile.isDirectory() && myFile.getName().matches("backup\\d+")) {
				 backupDirs.add(myFile.getName());
			 }
		 }
		 if(backupDirs.size() > 0 ) {
			 String lastBackupNumber = ((TreeSet<String>) backupDirs).last().replaceAll("\\D+", "");
			 int currentBackupNumber = Integer.parseInt(lastBackupNumber) + 1;
			 backupDirName = "backup" + currentBackupNumber;
		 } 
		 System.out.println("Creating backup directory named: " + backupDirName);
		 
		 // create directory
		 File d = new File(ROOT_DIR + "/" +  backupDirName);
		 d.mkdir();

		 
		 
		 // WRITE BACKUP
		 for (File myFile : listOfFiles) {
			 // backing up files that aren't a directory
			 if (!myFile.isDirectory() && myFile.canRead()) {
				 String arxiuLectura = ROOT_DIR + "/" +  myFile.getName();
				 String arxiuEscriptura = ROOT_DIR + "/" + backupDirName + "/" +  myFile.getName();
				 File fileCreated = new File(arxiuEscriptura);
				 fileCreated.createNewFile();
				 InputStream fInputStream = null;
				 OutputStream fOutputStream = null;
				 fInputStream = new FileInputStream(arxiuLectura);
				 fOutputStream = new FileOutputStream(arxiuEscriptura);
				 try {
					byte data[] = new byte[fInputStream.available()];
					fInputStream.read(data, 0, fInputStream.available());
					fOutputStream.write(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
			 } else if (!myFile.isDirectory() && !myFile.canRead()) {
				 System.out.println("Can't read file " + myFile.getName() + ", Skipping...");
			 }
		}
		 
	 }
}

