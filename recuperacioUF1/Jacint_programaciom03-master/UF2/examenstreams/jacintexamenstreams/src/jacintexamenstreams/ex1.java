package jacintexamenstreams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class ex1 {
	/*
	 * Exercici 1: (6p) Hi ha el directori fitxers_examen que conté el directori
	 * dirACopiar i el fitxer de propietats fitxerProperties.txt . Comproba si
	 * existeix el directori patata que ha d'estar dins del directori exercici1 el
	 * qual ha d'estar dins del directori fitxers_examen. Si no existeix el
	 * directori patata a llavors: 1. Crear el directori patata (File o FileUtils).
	 * 2. Crear el fitxer index.txt dins de patata (File). 3. Comprova si existeix
	 * el fitxer index.txt (File). 4. Escriu 2 línies de text dins de index.txt
	 * (FileUtils). 5. Copia el contingut del directori dirACopiar dins del
	 * directori patata (FileUtils). 6. Copia el fitxer index.txt en el directori
	 * fitxers_examen (FileUtils). 7. Rastreja tots els directoris i fitxers que hi
	 * ha dins del directori patata (FileUtils). 8. Per cada directori i fitxer
	 * trobat, comproba els seus permisos de lectura, escriptura i execució, si és
	 * ocult i el seu tamany. Envia el seu nom i les dades anteriors a la funció
	 * copiarNomEnFitxer() que serà l'encarregada de escriure aquestes dades en el
	 * fitxer index.txt (per escriure fes servir els Writers). Aquest codi està
	 * donat a l’annexe de la prova. Només has de crear el mètode
	 * copiarNomEnFitxer(). Es dona a l’annex la signatura del mètode. 9. Neteja el
	 * directori patata (esborra tot el seu contingut sense destruir el directori)
	 * (FileUtils). 10. Copia el fitxer index.txt en el directori patata
	 * (FileUtils). 11. Comprova que el contingut dels 2 fitxers index.txt és el
	 * mateix (FileUtils).
	 */
	// existeix dir patata dins de exercici1?
	// NO =>
	// 1. crear directori patata
	// 2. index.txt dins de patata
	// 3. comprovar si existeix index.txt
	// 4. 2 lineas dintre dindex.txt (fileutils)
	// 5. copiar diracopiar => dir patata (fileutils)
	// 6. copiar indext => fitxers_examen
	// 7. (fileutils) printa tots els fitxers dins de patata
	// 8. per cada fitxer comprovar:
	// . permisos de lectura
	// . permisos d'escpiptura
	// . permisos d'execucio
	// . si es ocult
	// . tamany
	// Nom + totes dades => copiarNomEnFitxer()
	// => escriu dades en index.txt
	// 9. borra contingut dir patata (fileutils)
	// 10. indext => patata copiar (fileutils)
	// 11. comprovar 2 index.txt content es el mateix (fileutils)

	static String pathDirArrel = "fitxers_examen/";
	static String pathDirExercici1 = "exercici1/";
	static String nomDirPatata = "patata";
	static String pathDirACopiar = "dirACopiar";

	private static void copiarNomEnFitxer(String nomFileTrobat, int tipusFile, StringBuilder permisosFile,
			PrintWriter pw) {
		// todo
	}

	public static void main(String[] args) {
		File directoriArrel = new File(pathDirArrel);
		File directoriPatata = new File(pathDirArrel + pathDirExercici1 + nomDirPatata);
		File directoriACopiar = new File(pathDirArrel + pathDirACopiar);
		File fitxerIndex = new File(pathDirArrel + pathDirExercici1 + nomDirPatata + "/index.txt");
		boolean creat;
		FileWriter fw = null;
		PrintWriter pw = null;
		StringBuilder permisosFile = new StringBuilder("---, ");
		File fitxerIndex2;

		// 1 .
		System.out.println("1.crear directori patata si no existeix");
		// 1. create a directory with forcemkdir
		
		File dir = new File(pathDirArrel + "/" + pathDirExercici1 + "/" + nomDirPatata);
		if (dir.exists()) {
			System.out.println("Patata exists, skipping.");
		} else {
			try {
				FileUtils.forceMkdir(dir);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 2. index.txt dins de patata
		
		System.out.println("Creant index.txt dins de patata");
		File index = new File(pathDirArrel + "/" + pathDirExercici1 + "/" + nomDirPatata + "/index.txt" );
		// 3. comprovar si existeix index.txt
		if (index.exists()) {
			System.out.println("index.txt exists, skipping.");
		} else {
			try {
				index.createNewFile();
				// 4. 2 lineas dintre dindex.txt (fileutils)
				 List<String> lines = new ArrayList<String>();
				 lines.add("line 1");
				 lines.add("line 2");
				 System.out.println("Writting lines to index.txt");
				 FileUtils.writeLines(index,lines);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 5. copiar CONTINGUT diracopiar => dir patata (fileutils)
		System.out.println("5. diracopiar copiat a patata");
		try {
			FileUtils.copyDirectory(directoriACopiar, dir);
		} catch (IOException e) {
			System.out.println("ERROR: fitxer ja copiat");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 6. copiar indext => fitxers_examen
		System.out.println(" 6. copiar indext => fitxers_examen");
		try {
			File indexCopy = new File(pathDirArrel + "/index.txt");
			FileUtils.copyFile(index,indexCopy);
		} catch (IOException e) {
			System.out.println("ERROR: fitxer ja copiat");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 7. (fileutils) printa tots els fitxers dins de patata
		System.out.println("7. (fileutils) printa tots els fitxers dins de patata + 8. per cada fitxer comprovar:");
		List<File> filesPatata = new ArrayList<File>();
		filesPatata.addAll(FileUtils.listFilesAndDirs(dir, TrueFileFilter.TRUE, TrueFileFilter.TRUE));
		filesPatata.forEach(myFile -> {
			System.out.println(myFile.getName());
			// 8. per cada fitxer comprovar:
			// . permisos de lectura
			// . permisos d'escpiptura
			// . permisos d'execucio
			// . si es ocult
			// . tamany
			// Nom + totes dades => copiarNomEnFitxer()
			// => escriu dades en index.txt
			String myLine = "\n\nFitxer: " + myFile.getName()  + "\n";
			myLine += "Lectura: " + myFile.canRead();
			myLine += "\nEscritura: " + myFile.canWrite();
			myLine += "\nExecució: " + myFile.canExecute();
			myLine += "\nOcult: "  + myFile.isHidden();
			myLine += "\nTamany: "  + FileUtils.sizeOf(myFile) + " bytes";
			File file = new File(pathDirArrel + "index.txt");
			FileWriter fr;
			try {
				fr = new FileWriter(file, true);
				fr.write(myLine);
				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		});
		
		// 9. borra contingut dir patata (fileutils)
		System.out.println("9. borra contingut dir patata (fileutils)");
		File patatDel = new File("fitxers_examen/exercici1/patata");
		File[] patataList = patatDel.listFiles();
		for (File f : patataList) {
			try {
				FileUtils.forceDelete(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	 	
		// 10. indext => patata copiar (fileutils)
		System.out.println("10. indext => patata copiar (fileutils)");
		File indexSrc = new File("fitxers_examen/index.txt");
		File indexCopy = new File("fitxers_examen/exercici1/patata/index.txt");
		try {
			FileUtils.copyFile(indexSrc, indexCopy);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 11. comprovar 2 index.txt content es el mateix (fileutils)
		System.out.println("11. comprovar 2 index.txt content es el mateix (fileutils)");
		FileReader frA = null;                     	// Entrada    
		FileReader frB = null;                    	// Entrada    
		BufferedReader brA;                        	// Buffer   
		BufferedReader brB;                        	// Buffer  
		String lineaA;  							// strings de linea de text
		String lineaB; 								// strings de linea de text
		// obrir i llegir arxius
		try {        
			frA = new FileReader(indexSrc);           // Inicialitza entrada del fitxer      
			frB = new FileReader(indexCopy);           // Inicialitza entrada del fitxer  
			brA = new BufferedReader(frA);           // Inicialitza buffer amb l’entrada        
			brB = new BufferedReader(frB);
			int contLineas = 1;
			boolean isEq = true;
			//llegir lineas
			while((lineaA = brA.readLine())!= null && (lineaB = brB.readLine())!= null) {   
				// llogica de comparació de lineas i tracking de linea en concret
				if (!lineaA.equals(lineaB)) {
					System.out.println("Lineas diferents a la linea: " + contLineas);
					System.out.println("Linea A" + contLineas + ": "  + lineaA);
					System.out.println("Linea B" + contLineas + ": "  + lineaB);
					isEq = false;
				}
				contLineas++;
			}
			if ((lineaA = brA.readLine())== null && (lineaB = brB.readLine())!= null) {
				System.out.println("WARNING: Fitxer ex3A.txt acavat, fitxer ex3B.txt continua");
			} else if ((lineaA = brA.readLine())!= null && (lineaB = brB.readLine())== null) {
				System.out.println("WARNING: Fitxer ex3B.txt acavat, fitxer ex3A.txt continua");
			}
			System.out.println("Els dos arxius " + (isEq ? "" : "no") + " son iguals" );
			////////////////////////////////////////////////////////////////////////////////////
			///////   WARNING: if file A only has one extra line program will not thro error
			///////			   if file B has one or more extra lines the program will throw error
			///////  This is due to the while loop reading a line at the begining
			////////////////////////////////////////////////////////////////////////////////////
		} catch (FileNotFoundException e) {        
			System.out.println("Fitxer no existeix");    
		} catch (IOException e) {        
			System.out.println(e.getMessage());    
		} finally {        
			if (frA != null)
				try {
					frA.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
				try {
					frB.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		
		
		
	}
}
