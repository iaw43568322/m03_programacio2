# Java Built-in exceptions  
definition of an exception: abnormal condition that arises **during runtime**.  
  
## Com es poden controlar les excepcions?  
```
try {
	// codi succeptible d'error
  if(condicio) {
    throw EvaException
  }
}

catch {
	// que fer quan hi ha l'error
		// per exemple un sysou
}

finally {
	// codi a executar en qualsevol cas
}
```
  
### Example: invalid user exception
```
System.out.println("Introdueix la id de l'usuar: ");
Scanner sc = new Scanner(System.in);
int user_id = sc.nextInt();
	
try {
	if (user_id != 1234) {
		throw new invalidUserIdException();
	}
} catch (Exception e) {
	System.out.println(e);
}
```
