
# Canviar password root a phpmyadmin  
per Eva Barbeito Andrade - dimecres, 10 febrer 2021, 08:53  
Nombre de respostes: 1  
  
Encenem al LAMPP la base de dades MySQL i l'Apache Web Server (l'apache "serveix" aquesta interfície gràfica de gestió de BBDD mysql, ha d'estar engegat).  
```
$ cd /opt/lampp

$ sudo ./manager-linux-x64.run
```
Anem al browser a localhost/phpmyadmin  -> Pestanya Comptes d' usuari -> root a localhost - > Edita els privilegis -> Canvia la contrasenya de root a localhost.  
  
La canviem per "admin" i salvem al botó "executa".  
  
És preferible crear comptes d' usuaris per a les aplicacions, i no treballar mai amb la de root, això és només un exemple.  
  
Ara en logar-nos, no agafa el nou password.  
  
Anem al fitxer de configuració de phpmyadmin, a canviar-ho també:  
  
Editem config.inc.php , que es troba a : /opt/lampp/phpmyadmin  
i posem la nova contrasenya "admin", on per defecte no hi ha res.  
  
Fem restart all al XAMPP i refresquem al navegador el phymyadmin, hauria d'agafar el login de root amb la nova contrasenya.  
  
Recordeu per crear stored procedures: clicar sobre la Base de Dades, per exemple, calendar, i apareix la pestanya "Rutines".  
  
Per tancar el XAMPP, cal tancar tots els serveis prèviament.  
  
  
# Actualitzar el mysql de versió pel phpmyadmin (XAMPP)  
per Eva Barbeito Andrade - dimarts, 16 febrer 2021, 19:03  
Nombre de respostes: 0  
  
Per poder importar/exportar taules, generar stored procedures, entre altres coses, necessitem tenir una versió actualitzada.  
  
Al directori /opt/lampp/bin tenim aquest script que ens permetrà fer-ho.  
``` 
eva@eva-katei /opt/lampp/bin $ sudo ./mysql_upgrade -u root -p  
  
sudo ./mysql_upgrade -u root -p  
```  
El password que et demana per defecte serà "res" ,  enter, retorn de carry, si no ho has canviat prèviament.  
  
Podriem treballar alternativament a phpmyadmin, per línia de comandes, igual que Postgress i altres, llençar scripts, etc.  
```
eva@eva-katei /opt/lampp/bin $ /opt/lampp/bin/mysql -u root -p  
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 93
Server version: 10.4.17-MariaDB Source distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
use calendar;
show tables;
etc etc
```
 
