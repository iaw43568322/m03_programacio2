package inheritance;

public class Design extends Employee {
	
	//attributes
	public double weeklyWage;
	
	public Design(String employeeId, String name, int age, double weeklyWage) {
		super(employeeId, name, age);
		this.weeklyWage = weeklyWage;
	}

	@Override
	public double getSalary(double weeksWorked) {
		return this.weeklyWage * weeksWorked;
	}

}
