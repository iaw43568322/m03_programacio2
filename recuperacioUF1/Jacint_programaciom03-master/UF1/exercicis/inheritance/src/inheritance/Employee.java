/**
 * 
 */
package inheritance;

/**
 * @author yo
 *
 */
public abstract class Employee {
	// attributes
	public String employeeId;
	public String name;
	public int age;
	// constructor
	public Employee(String employeeId, String name, int age) {
		this.employeeId = employeeId;
		this.name = name;
		this.age = age;
	}
	public void setData(String employeeId, String name, int age) {
		this.employeeId = employeeId;
		this.name = name;
		this.age = age;
	}
	public void getData() {
		System.out.println("EmployeeID: " + this.employeeId + ", name: " + this.name + ", age:" + this.age);
	}
	public abstract double getSalary(double timeWorked);
}
