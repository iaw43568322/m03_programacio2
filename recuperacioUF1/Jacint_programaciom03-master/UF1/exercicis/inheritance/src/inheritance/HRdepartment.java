package inheritance;

public class HRdepartment extends Employee {
	//attributes
	double monthlyWage;
	
	public HRdepartment(String employeeId, String name, int age, double monthlyWage) {
		super(employeeId, name, age);
		this.monthlyWage = monthlyWage;
	}

	@Override
	public double getSalary(double timeWorked) {
		return timeWorked * this.monthlyWage;
	}

}
