package circle;

public class Circle {
	// attributes
	public double myRadius;
	// constructor
	public Circle (double myRadius) {
		this.myRadius = myRadius;
	}
	public double area () {
		return Math.PI * Math.pow(myRadius, 2);
	}
	public double perimeter () {
		return 2 * Math.PI * myRadius;
	}
	@Override
	public String toString() {
		return "Circle [radius=" + myRadius + "]";
	}
	
}
