package com.daw.jacintiglesias.krona;

import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import com.sun.source.tree.DoWhileLoopTree;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

// import cadena
import Llibreries.Cadena;
import Varies.Data;

public class Waypoint {

	// methods
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimenTmp = new ComprovacioRendiment();
		return comprovacioRendimenTmp;
	}
	///// COMENTAT PER EXAMEN///
	/*
	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimenTmp) {
		// contador temps en milisegons
		long tempsEnMs;
		// contador de temps
		long tempsInicialArr = System.nanoTime();
		// lista arrayList
		for (int i = 0; i < numObjACrear; i++) {
			int[] arr = { 0, 0, 0 };
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", arr, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
			comprovacioRendimenTmp.addListArr(wd1);
		}
		// temps final linked list
		long tempsFinalArr = System.nanoTime();
		tempsEnMs = TimeUnit.MILLISECONDS.convert((tempsFinalArr - tempsInicialArr), TimeUnit.NANOSECONDS);
		System.out.println("Temps total de insertar " + numObjACrear + " elements en ArrayList: " + tempsEnMs);
		// contador de temps
		long tempsInicialLinked = System.nanoTime();
		// linked list
		for (int i = 0; i < numObjACrear; i++) {
			int[] arr = { 0, 0, 0 };
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", arr, true,
					LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
			comprovacioRendimenTmp.addListLinked(wd1);
		}
		long tempsFinalLinked = System.nanoTime();
		tempsEnMs = TimeUnit.MILLISECONDS.convert((tempsFinalLinked - tempsInicialLinked), TimeUnit.NANOSECONDS);
		System.out.println("Temps total de insertar " + numObjACrear + " elements en LinkedList: " + tempsEnMs);
		return comprovacioRendimenTmp;
	}
	*/
	/*
	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
		// object to insert
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		int[] arr = { 0, 0, 0 };
		/*
		Waypoint_Dades wd1 = new Waypoint_Dades(0, "Òrbita de la Terra", arr, true,
				LocalDateTime.parse("15-08-1954 00:01", formatter), null, null);
		int meitatllista = comprovacioRendimentTmp.llistaArrayList.size() / 2;
		// Print list size
		System.out.println("ArrayList.size(): " + comprovacioRendimentTmp.llistaArrayList.size() + " meitatllista: "
				+ meitatllista);
		long tempsInicialArr = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(0, wd1);
		long tempsFinalArr = System.nanoTime();
		long tempsInicialLink = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(0, wd1);
		long tempsFinalLink = System.nanoTime();
		System.out.println("Temps insertar 1 element a la posició 0 ArrayList: " + (tempsFinalArr - tempsInicialArr));
		System.out
				.println("Temps insertar 1 element a la posició 0 LinkedList: " + (tempsFinalLink - tempsInicialLink));
		System.out.println("-----------------------------------------------------------------------");
		tempsInicialArr = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(meitatllista, wd1);
		tempsFinalArr = System.nanoTime();
		tempsInicialLink = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(meitatllista, wd1);
		tempsFinalLink = System.nanoTime();
		System.out.println("Temps insertar 1 element a la posició " + meitatllista + " ArrayList: "
				+ (tempsFinalArr - tempsInicialArr));
		System.out.println("Temps insertar 1 element a la posició " + meitatllista + " LinkedList: "
				+ (tempsFinalLink - tempsInicialLink));
		System.out.println("-----------------------------------------------------------------------");
		tempsInicialArr = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(wd1);
		tempsFinalArr = System.nanoTime();
		tempsInicialLink = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(wd1);
		tempsFinalLink = System.nanoTime();
		System.out.println("Temps insertar 1 element a la posició final: " + (tempsFinalArr - tempsInicialArr));
		System.out.println(
				"Temps insertar 1 element a la posició final:  LinkedList: " + (tempsFinalLink - tempsInicialLink));
		System.out.println("-----------------------------------------------------------------------");

		return comprovacioRendimentTmp;
	}
	*/

	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("------------APARTAT1---------------");
		int sizeOfList = comprovacioRendimentTmp.llistaArrayList.size();
		for (int i = 0; i < sizeOfList; i++) {
			comprovacioRendimentTmp.idsPerArrayList.add(i);
		}
		System.out.println("El primer element te valor: " + comprovacioRendimentTmp.idsPerArrayList.get(0));
		System.out.println("El ultim element valor: " + comprovacioRendimentTmp.idsPerArrayList.get(sizeOfList - 1));

		System.out.println("------------APARTAT2---------------");
		for (Integer element : comprovacioRendimentTmp.idsPerArrayList) {
			System.out.println("Abans del canvi: comprovacioRendimentTmp.llistaArrayList.get(" + element + ").getId(): "
					+ comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			comprovacioRendimentTmp.llistaArrayList.get(element).setId(element);
			System.out.println("Despres del canvi: comprovacioRendimentTmp.llistaArrayList.get(" + element
					+ ").getId(): " + comprovacioRendimentTmp.llistaArrayList.get(element).getId());
			System.out.println();
		}
		System.out.println("------------APARTAT3.1 (bucle for)---------------");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			System.out.println("ID = " + comprovacioRendimentTmp.llistaArrayList.get(i).getId() + ", nom = "
					+ comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
		}
		System.out.println("------------APARTAT3.1 (Iterator)---------------");
		Iterator<Waypoint_Dades> iter = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades wp = iter.next();
			System.out.println("ID = " + wp.getId() + ", nom = " + wp.getNom());
		}
		System.out.println("------------APARTAT 4 (Iterator)---------------");
		System.out.println("Preparat per esborrar el contingut de llistaLinkedList que té "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		comprovacioRendimentTmp.llistaLinkedList.clear();
		System.out.println(
				"Esborrada. Ara llistaLinkedList té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
		Iterator<Waypoint_Dades> iter2 = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter2.hasNext()) {
			comprovacioRendimentTmp.llistaLinkedList.add(iter2.next());
		}
		System.out.println("Copiats els elements de llistaArrayList en llistaLinkedList que ara te "
				+ comprovacioRendimentTmp.llistaLinkedList.size() + " elements");
		System.out.println("------------APARTAT 5.1 (bucle for)---------------");
		for (Integer element : comprovacioRendimentTmp.idsPerArrayList) {
			if (comprovacioRendimentTmp.llistaArrayList.get(element).getId() > 5) {
				comprovacioRendimentTmp.llistaArrayList.get(element).setNom("Òrbita de Mart");
			}
			System.out.println("Modificat el waypoint amb id = " + element);
		}
		System.out.println();
		System.out.println("------------APARTAT 5.1 (comprovació)---------------");
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			Waypoint_Dades wp = comprovacioRendimentTmp.llistaArrayList.get(i);
			System.out.println("El Waypoint amb id " + wp.getId() + " té el nom = " + wp.getNom());
		}
		System.out.println("------------APARTAT 5.2 (Iterator)---------------");
		Iterator<Waypoint_Dades> iter3 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter3.hasNext()) {
			Waypoint_Dades wp = iter3.next();
			if (wp.getId() < 5) {
				wp.setNom("Punt Lagrange entre la Terra i la Lluna");
				System.out.println("Modificat el waypoint amb id = " + wp.getId());
			}
		}
		System.out.println();
		System.out.println("------------APARTAT 5.2 (comprovació)---------------");
		Iterator<Waypoint_Dades> iter4 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter4.hasNext()) {
			Waypoint_Dades wp = iter4.next();
			System.out.println("El waypoint amb id = " + wp.getId() + " té el nom = " + wp.getNom());
		}
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println(
				"S'esborraran waypoints de l'ArrayList i del LinkedList. Hi ha 3 apartats per a fer:APARTAT 1:Recorre la llista comprovacioRendimentTmp.llistaArrayList i suprimeix els waypoints amb IDmenors de 6 (amb for(Integer element: numbersList)). ¿Es pot? ¿Perquè?");
		System.out.println("No es pot per que la llista no es de Integer sino de Wapoint_Dades");
		System.out.println();
		System.out.println("---- APARTAT 2 (Iterator)");
		Iterator<Waypoint_Dades> iter5 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter5.hasNext()) {
			Waypoint_Dades wp = iter5.next();
			if (wp.getId() > 4) {
				iter5.remove();
				System.out.println("Esborrat el waypoint amb id = " + wp.getId());
			}
		}
		System.out.println("---- APARTAT 2 (Comprovació)");
		Iterator<Waypoint_Dades> iter6 = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (iter6.hasNext()) {
			Waypoint_Dades wp = iter6.next();
			System.out.println("El waypoint amb id = " + wp.getId() + " té el nom = " + wp.getNom());
		}
		System.out.println("---- APARTAT 3 (listIterator)");
		ListIterator<Waypoint_Dades> iter7 = comprovacioRendimentTmp.llistaLinkedList.listIterator();
		while (iter7.hasNext()) {
			Waypoint_Dades wp = iter7.next();
			if (wp.getId() == 2) {
				System.out.println("Esborrat el waypoint amb id = " + wp.getId());
				iter7.remove();
			}
		}
		System.out.println("---- APARTAT 3 (comprovació)");
		while (iter7.hasPrevious()) {
			Waypoint_Dades wp = iter7.previous();
			System.out.println("El waypoint amb id = " + wp.getId() + " té el nom = " + wp.getNom());
		}
		return comprovacioRendimentTmp;
	}

	// lists pt 1 pt 2 ex4
	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		// recorre llistaArraylist, modificar parells
		Iterator<Waypoint_Dades> iter8 = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iter8.hasNext()) {
			Waypoint_Dades wp = iter8.next();
			if (wp.getId() % 2 == 0) {
				System.out.println("----- Modificar el waypoint amb id = " + wp.getId() + " -----");
				System.out.println("Nom actual: " + wp.getNom());
				System.out.print("Nom nou: ");
				Scanner sc = new Scanner(System.in);
				String i = sc.nextLine();
				// cambiant el nom del atribut nom
				wp.setNom(i);
				System.out.println("Coordenades actuals: " + wp.getCoordenades()[0] + " " + wp.getCoordenades()[1] + " "
						+ wp.getCoordenades()[2]);
				System.out.print("Coordenades noves (format: 1 13 7): ");
				String coordsStr = sc.nextLine();
				String[] coodsArr = coordsStr.split(" ");
				while (coodsArr.length != 3 || !Cadena.stringIsInt(coodsArr[0])
						|| !Cadena.stringIsInt(coodsArr[1])
						|| !Cadena.stringIsInt(coodsArr[2])) {
					if (coodsArr.length != 3) {
						System.out.println("ERROR: Introduir 3 paràmetres separats per 1 espai en blanc. Has introduit "
								+ coodsArr.length + " paràmetres");
					}
					if (!Cadena.stringIsInt(coordsStr) && coodsArr.length > 1) {
						System.out.println("Coordenada " + Cadena.nonnonValidCoord(coodsArr[0]) + " "
								+ Cadena.nonnonValidCoord(coodsArr[1]) + " "
								+ Cadena.nonnonValidCoord(coodsArr[2]) + " no valida.");
					}
					System.out.println("Coordenades actuals: " + wp.getCoordenades()[0] + " " + wp.getCoordenades()[1]
							+ " " + wp.getCoordenades()[2]);
					System.out.print("Coordenades noves (format: 1 13 7): ");
					coordsStr = sc.nextLine();
					coodsArr = coordsStr.split(" ");
				}
				int[] intArr = new int[3];
				for (int counter = 0; counter < wp.getCoordenades().length; counter++) {
					intArr[counter] = Integer.parseInt(coodsArr[counter]);
				}
				wp.setCoordenades(intArr);
				System.out.println(Arrays.toString(wp.getCoordenades()));
				System.out.println();
			}
			
		}
		return comprovacioRendimentTmp;
	}
	// metode exercici 5
	public static ComprovacioRendiment visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		Iterator<Waypoint_Dades> iterSort = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (iterSort.hasNext()) {
			System.out.println(iterSort.next());
		}
		return comprovacioRendimentTmp;
	}
	// metode que realitza la accio de metode 6
	public static int calculaCoordenades(int[] coordenada) {
		int total = 0;
		for (int i = 0; i < coordenada.length; i++) {
			total += Math.pow(coordenada[i],2);
		}
		return total;
	}
	// metode 6
	public static ComprovacioRendiment waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.print("Distància màxima de la terra: ");
		Scanner sc = new Scanner(System.in);
		int distancia = sc.nextInt();
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		Iterator<Waypoint_Dades> distIt = comprovacioRendimentTmp.llistaArrayList.iterator();
		while(distIt.hasNext()) {
			Waypoint_Dades wp = distIt.next();
			System.out.println(wp);
		}
		return comprovacioRendimentTmp;
	}
	
	//////////////////////////////
	///////// EXAMEN /////////////
	//////////////////////////////
	//// 40 initzialitzar waypoints
	public static ComprovacioRendiment inicialitzarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		// ADD WAYPOINTS 
		Waypoint_Dades wp1 = new Waypoint_Dades(0, "Òrbita de la Terra", new int[] {0,0,0}, true, Data.formatData("21-10-2020 01:10"), null, Data.formatData("22-10-2020 23:55"), 0);
		Waypoint_Dades wp2 = new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] {1,1,1}, true,Data.formatData("21-10-2020 01:00"), null, Data.formatData("22-10-2020 23:55"),6);
		Waypoint_Dades wp3 = new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] {2,2,2}, true, Data.formatData("22-10-2020 00:50"), null, Data.formatData("22-10-2020 23:55"),1);
		Waypoint_Dades wp4 = new Waypoint_Dades(3, "Òrbita de Mart", new int[] {3,3,3}, true, Data.formatData("21-10-2020 00:40"), null, Data.formatData("22-10-2020 23:55"),0);
		Waypoint_Dades wp5 = new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] {4,4,4}, true, Data.formatData("21-10-2020 00:30"), null,Data.formatData("22-10-2020 23:55") ,0);
		Waypoint_Dades wp6 = new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa", new int[] {5,5,5}, true,Data.formatData("21-10-2020 00:20"), null, Data.formatData("22-10-2020 23:55"),6);
		Waypoint_Dades wp7 = new Waypoint_Dades(6, "Òrbita de Europa", new int[] {6,6,6}, true,Data.formatData("21-10-2020 00:10") , null, Data.formatData("22-10-2020 23:55"),0);
		Waypoint_Dades wp8 = new Waypoint_Dades(7, "Òrbita de Venus", new int[] {7,7,7}, true, Data.formatData("21-10-2020 00:01"), null, Data.formatData("22-10-2020 23:55"),0);
		// comprovar que no hi ha waypoints a la llista
		if (comprovacioRendimentTmp.llistaWaypoints.size() > 0 ) {
			/// ESBORRAR WAYPOINTS
			System.out.println("S'han trovat waypoints a la llista, esborrant waypoints.");
			comprovacioRendimentTmp.llistaWaypoints.clear();
		}
		comprovacioRendimentTmp.llistaWaypoints.add(wp1);
		comprovacioRendimentTmp.llistaWaypoints.add(wp2);
		comprovacioRendimentTmp.llistaWaypoints.add(wp3);
		comprovacioRendimentTmp.llistaWaypoints.add(wp4);
		comprovacioRendimentTmp.llistaWaypoints.add(wp5);
		comprovacioRendimentTmp.llistaWaypoints.add(wp6);
		comprovacioRendimentTmp.llistaWaypoints.add(wp7);
		comprovacioRendimentTmp.llistaWaypoints.add(wp8);
		return comprovacioRendimentTmp;
	}
	// 41 alta waypoint
	public static  void altaWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		// obtenint ID
		// arr to dodge var lock
		int[] maxId = {-1};
		comprovacioRendimentTmp.llistaWaypoints.forEach(wp -> {
			if (wp.getId() > maxId[0]) {
				maxId[0] = wp.getId();
			}
		});
		// declarant nova id
		int nouId = maxId[0] + 1;
		// get coords
		Scanner sc = new Scanner(System.in);
		boolean coordsInvalid = false;
		String coords = "";
		do {
			System.out.println("Introdueix 3 coordenades, exemple => '1 3 4'");
			coords = sc.nextLine();
			String[] coordsCount = coords.split(" ");
			if (coordsCount.length != 3) {
				coordsInvalid = true;
				System.out.println("ERROR: les coordenades no compleixen el format => 'int{blank space}int{blank space}int'");
			}
			if (!coordsInvalid && !Cadena.stringIsInt(coordsCount[0]) || !Cadena.stringIsInt(coordsCount[1]) || !Cadena.stringIsInt(coordsCount[2])) {
				coordsInvalid = true;
				System.out.println("ERROR: les coordenades no compleixen el format => 'int{blank space}int{blank space}int' - Hint: one or many coords are not integer");
			}
		} while(coordsInvalid);
		// check true or false
		Collator comparador = Collator.getInstance();
		comparador.setStrength(Collator.SECONDARY);
		boolean actiuNovalid = false;
		String actiu;
		do {
			System.out.println("Introdueix actiu: true/false");
			actiu = sc.nextLine();
			if (comparador.compare("true",actiu) != 0 && comparador.compare("false",actiu) != 0) {
				actiuNovalid = true;
				System.out.println("ERROR: has entrat altre valor que true o false. Hint -> don't use accents or extra spaces");
			}
		} while(actiuNovalid);
		// data creació actual
		LocalDateTime dataCreacio = LocalDateTime.now();
		// data anulació
		String dataAnulacio = "";
		do {
			System.out.println("Introdueix una data valida, format: 'DD-MM-AAA'");
			dataAnulacio = sc.nextLine();
		} while(!Data.esData(dataAnulacio));
		// data modificacio
		LocalDateTime dataModificacio = LocalDateTime.now();
		// escollir tipus
		boolean invalidWp = false;
		String wpt = "";
		int wpInt = -1;
		do {
		System.out.println("Aquests son els tipus a escollri: ");
		String totalPlanets = "";
		for (int i = 0;i < Waypoint_Type.TIPUS_WAYPOINT.length;i++) {
			totalPlanets += "num "+ (i+1) + ": " +Waypoint_Type.TIPUS_WAYPOINT[i] + "\n";
		}
		System.out.println(totalPlanets);
		System.out.println("Indica el numero que vols");
		wpt = sc.nextLine();
		if (!Cadena.stringIsInt(wpt)) {
			System.out.println("ERROR: el waypoint no es de tipus integer");
			invalidWp = true;
		}
		if (!invalidWp) {
			wpInt = Integer.parseInt(wpt);
			wpInt--;
			if (wpInt < 0 || wpInt > Waypoint_Type.TIPUS_WAYPOINT.length) {
				System.out.println("ERROR: el waypoint no es trova al array");
				invalidWp = true;
			}
		}
		} while(invalidWp);
		// formatejant coordenades
		String[] coordsString = coords.split(" ");
		int[] coordsInt = {0,0,0};
		coordsInt[0] = Integer.parseInt(coordsString[0]);
		coordsInt[1] = Integer.parseInt(coordsString[1]);
		coordsInt[2] = Integer.parseInt(coordsString[2]);
		// setting actiu
		boolean actiuFinal = false;
		if (actiu.compareTo("true") == 0) {
			actiuFinal = true;
		}
		Waypoint_Dades wp1 = new Waypoint_Dades(maxId[0], "Òrbita de la Terra", new int[] {coordsInt[0],coordsInt[1],coordsInt[2]}, 
				actiuFinal, dataCreacio, Data.formatDataDMY(dataAnulacio),dataCreacio, Integer.parseInt(wpt));
		comprovacioRendimentTmp.llistaWaypoints.add(wp1);
		System.out.println("El waypoint s'ha insertat correctament.");
	}
	/// 42 veure wp tipus
	public static  void veureWpTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String wpt;
		int wpInt = -1;
		boolean invalidWp = false;
		do {
			System.out.println("Seleccionar tipus de waypoint (numero)");
			String totalPlanets = "";
			for (int i = 0;i < Waypoint_Type.TIPUS_WAYPOINT.length;i++) {
				totalPlanets += "num "+ i + ": " +Waypoint_Type.TIPUS_WAYPOINT[i] + "\n";
			}
			System.out.println(totalPlanets);
			wpt = sc.nextLine();
			if (!Cadena.stringIsInt(wpt)) {
				System.out.println("ERROR: el waypoint no es de tipus integer");
				invalidWp = true;
			}
			if (!invalidWp) {
				wpInt = Integer.parseInt(wpt);
				if (wpInt < 0 || wpInt > Waypoint_Type.TIPUS_WAYPOINT.length) {
					System.out.println("ERROR: el waypoint no es trova al array");
					invalidWp = true;
				}
			}
		} while(invalidWp);
		LinkedList<Waypoint_Dades> llistaA = new LinkedList<Waypoint_Dades>(comprovacioRendimentTmp.llistaWaypoints);
		LinkedList<Waypoint_Dades> llistaB = new LinkedList<Waypoint_Dades>();
		Iterator<Waypoint_Dades> iter = llistaA.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades wp = iter.next();
			if (wp.getTipus() == wpInt) {
				llistaB.add(wp);
				iter.remove();
				System.out.println("Esborrat el waypoint amb id = " + wp.getId() + " i tipus = " + wp.getTipus());
			}
		}
		System.out.println("Imprimint llistaB: " + llistaB);
	}
	// 43
	public static  void numWpPerTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Waypoint_Dades> wpds = new HashSet<Waypoint_Dades>(comprovacioRendimentTmp.llistaWaypoints);
		TreeMap<Integer,Waypoint_Dades> wpdTree = new TreeMap<Integer,Waypoint_Dades>();
		TreeMap<Integer,Integer> countTree = new TreeMap<Integer,Integer>();
		Iterator<Waypoint_Dades> iter = wpds.iterator();
		int counter = 0;
		while (iter.hasNext()) {
			Waypoint_Dades wp = iter.next();
			wpdTree.put(counter,wp);
			counter++;
			
			if (countTree.keySet().contains(wp.getTipus())) {
				countTree.put(wp.getTipus(), countTree.get(wp.getTipus()) + 1);
			} else {
				countTree.put(wp.getTipus(),1);
			}
		}
		countTree.forEach((key,value) -> System.out.println("Tipus: " + key + " (" + Waypoint_Type.TIPUS_WAYPOINT[key] + "): " + value + "naus" ));
	}
	//44 wpperNom
	public static  void wpperNom(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un nom de waypoint: ");
		String wptNom = sc.nextLine();
		comprovacioRendimentTmp.llistaWaypoints.forEach(wpt -> {
			if (wpt.getNom().toLowerCase().contains(wptNom.toLowerCase())) {
				System.out.println("id " + wpt.getId() + ": " + wpt.getNom());
			}
		});
	}
	// 45mySort
	public static  void mySort(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Waypoint_Dades> wpds = new HashSet<Waypoint_Dades>(comprovacioRendimentTmp.llistaWaypoints);
		TreeMap<Integer,Waypoint_Dades> wpdTree = new TreeMap<Integer,Waypoint_Dades>();
		Iterator<Waypoint_Dades> iter = wpds.iterator();
		int counter = 0;
		while (iter.hasNext()) {
			Waypoint_Dades wp = iter.next();
			wpdTree.put(counter,wp);
			counter++;
		}
		TreeMap mapOrdenarPerId = new TreeMap(new comparadorPerID2(wpdTree));   //"comparadorPerEdat" està definit a baix de tot.
		mapOrdenarPerId.putAll(wpdTree);
		mapOrdenarPerId.forEach((key,value) -> System.out.println(key + ": Dades de la ruta: \n" + value));
	}
}

class comparadorPerID2 implements Comparator {
    Map mapa;

    public comparadorPerID2(TreeMap mapa){
        this.mapa = mapa;
    }

    //Al fer un MapTree fent servir aquest Comparator, es farà servir la funció compare()
    //per a determinar la posició en l'arbre ordenat i evidentment no hi podran haver elements 
    //repetits dins de l'arbre (la funció compare() determina quan 2 elements són iguals
    //perquè ocupen la mateix posició d'ordenació en l'arbre). 
    //Com està agafant objectes de tipus Persona, quan és fa "return valorA.compareTo(valorB)"
    //en realitat està fent servir la funció compareTo() que hi ha dins de Persona i que està 
    //comparant dues persones en funció de la edat.
    public int compare(Object clauOb1, Object clauOb2){
        Comparable valorA = (Comparable) mapa.get(clauOb1);
        Comparable valorB = (Comparable) mapa.get(clauOb2);
        return valorA.compareTo(valorB);
        
       /* date1.isBefore(date)*/
    }
}










