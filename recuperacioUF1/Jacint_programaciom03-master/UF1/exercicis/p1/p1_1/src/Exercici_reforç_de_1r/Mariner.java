/**
 * 
 */
package Exercici_reforç_de_1r;

import java.time.LocalDateTime;

/**
 * @author yo
 *
 */
public class Mariner extends Tripulant {

	/**
	 * @param ID
	 * @param nom
	 * @param actiu
	 * @param dataAlta
	 * @param departament
	 * @param llocDeServei
	 */
	// attributes
	private boolean serveiEnPont;
	private String descripcioFeina;

	public Mariner(String ID, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			boolean serveiEnPont, String descripcioFeina) {
		super(ID, nom, actiu, dataAlta, departament, llocDeServei);
		// TODO Auto-generated constructor stub
		// main attributes
		this.serveiEnPont = serveiEnPont;
		this.descripcioFeina = descripcioFeina;
	}

	@Override
	protected String ImprimirDadesTripulant() {
		String serveixEnPontString = "SI";
		if (!serveixEnElPont()) {
			serveixEnPontString = "NO";
		}
		return "Bandol: " + this.BANDOL + "\n" +
			   "ID: " + this.ID + "\n" +
			   "Nom: " + this.nom + "\n" +
			   "Actiu: " + this.actiu + "\n" +
			   "Departament (classe Tripulant): " + this.departament + "\n" +
			   "Departament (interficie IKSRotarranConstants): " + IKSRotarranConstants.LLOCS_DE_SERVEI[this.departament] + "\n"  +
			   "Lloc de servei (Tripulants): " + this.getLlocDeServei() + "\n" +
			   "Lloc de servei (Constants): " + IKSRotarranConstants.LLOCS_DE_SERVEI[this.getLlocDeServei()] + "\n" +
			   "Descripció: " + this.descripcioFeina + "\n" +
			   "Serveix en el pont?: " + serveixEnPontString + "\n" +
			   "Data d'alta: " + myDateFormatter(this.dataAlta);
	}

	private boolean serveixEnElPont() {
		return this.serveiEnPont;
	}
}
