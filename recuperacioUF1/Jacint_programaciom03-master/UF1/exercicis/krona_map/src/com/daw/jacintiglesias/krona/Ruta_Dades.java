package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import Varies.Data;

public class Ruta_Dades implements Comparable<Ruta_Dades>{
	// de set
	private int id;                     
	private String nom;
	private ArrayList<Integer> waypoints;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
	
	
	public int getId() {
		return id;
	}
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	@Override
	public String toString() {
		return "id = " + this.id + "\n "
				+ "" + 
				"nom = " + this.nom + "\n" +
				"actiu = " + this.actiu + "\n" +
				"dataCreacio = " + Data.formatDataLDT(this.dataCreacio) + "\n" +
				"dataAnulacio = " + Data.formatDataLDT(this.dataAnulacio) + "\n" +
				"dataModificacio = " + Data.formatDataLDT(this.dataModificacio) + "\n" +
				"waypoints: =  " + this.waypoints;
	}
	public List<Integer> getWaypoints() {
		return waypoints;
	}
	public String getNom() {
		return nom;
	}
	@Override
	public int compareTo(Ruta_Dades B) {
		if (this.getWaypoints().equals(B.getWaypoints())) {
			return 0;
		} else if (this.getId() < B.getId()){
			return 1;
		} else if (this.getId() > B.getId()){
			return -1;
		}
		return 0;
	}
	
	/*
	 * this is obsolete in stack exercise
	@Override
	public String toString() {
		return "Ruta_Dades [id=" + Ruta.getId()+ ", nom=" + Ruta.getNom() + ", waypoints=" + Ruta.getWaypoints() + ", actiu=" + Ruta.isActiu() 
				+ ", dataCreacio=" + Ruta.getDataCreacio() + ", dataAnulacio=" + Ruta.getDataAnulacio()  + ", dataModificacio="
				+ Ruta.getDataModificacio() + "]";
	}
	*/
}
