package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

import Llibreries.Cadena;
import Varies.Data;

public class Ruta {
	private static int id;
	private static String nom;
	private static ArrayList<Integer> waypoints;
	private static boolean actiu;
	private static LocalDateTime dataCreacio;
	private static LocalDateTime dataAnulacio;
	private static LocalDateTime dataModificacio;

	// constructor
	protected Ruta(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}

	///////// EXERCICI 10 (4) /////////
	// methods
	public static List<Waypoint_Dades> crearRutaInicial(ComprovacioRendiment cRtmp) {
		return cRtmp.llistaArrayList;
	}

	// executar el menu 1 previament
	public static ComprovacioRendiment inicialitarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		List rutaInicialTmp = crearRutaInicial(comprovacioRendimentTmp);
		Iterator<Waypoint_Dades> iter = rutaInicialTmp.iterator();
		while (iter.hasNext()) {
			Waypoint_Dades wpdEl = iter.next();
			comprovacioRendimentTmp.pilaWaypoints.push(wpdEl);
		}
		Waypoint_Dades wpDeque = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				Data.formatData("21-10-2020 00:30"), null, Data.formatData("22-10-2020 23:55"));
		comprovacioRendimentTmp.pilaWaypoints.addFirst(wpDeque);
		comprovacioRendimentTmp.wtmp = wpDeque;
		return comprovacioRendimentTmp;
	}

	///////// EXERCICI 11 (5) /////////
	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println(comprovacioRendimentTmp.pilaWaypoints);
	}

	///////// EXERCICI 12 (6) /////////
	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pw = comprovacioRendimentTmp.pilaWaypoints;
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
		while (!pw.isEmpty()) {
			pilaWaypointsInversa.push(pw.pop());
		}
		System.out.println(pilaWaypointsInversa);
	}

	///////// EXERCICI 13 (7) /////////
	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {

		System.out.println("Is wtmp found in pilaWaypoints? "
				+ comprovacioRendimentTmp.pilaWaypoints.contains(comprovacioRendimentTmp.wtmp));
		// creating new waypoint identic to wtmp
		Waypoint_Dades wpDeque = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				Data.formatData("21-10-2020 00:30"), null, Data.formatData("22-10-2020 23:55"));
		System.out.println("Is new Waypoint identic to wtmp found in pilaWaypoints? "
				+ comprovacioRendimentTmp.pilaWaypoints.contains(wpDeque));
	}

	// krona set 4 20
	public static ComprovacioRendiment inicialitzaLListaRutes(ComprovacioRendiment comprovacioRendimentTmp) {

		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta   0:   Terra   -->   Punt   Lagrange   Júpiter-Europa",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true, Data.formatData("28-10-2020 16:30"),
				null, Data.formatData("28-10-2020 16:30"));
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> Òrbita de Mart (directe)",
				new ArrayList<Integer>(Arrays.asList(0, 3)), true, Data.formatData("28-10-2020 16:31"), null,
				Data.formatData("28-10-2020 16:31"));
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> Òrbita de Venus",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				Data.formatData("28-10-2020 16:32"), null, Data.formatData("28-10-2020 16:32"));
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> Òrbita de Júpiter ",
				new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true, Data.formatData("28-10-2020 16:33"), null,
				Data.formatData("28-10-2020 16:33"));
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta   2.2:   Terra   -->   Òrbita   de   Venus   (REPETIDA)",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				Data.formatData("28-10-2020 16:32"), null, Data.formatData("30-10-2020 19:49"));
		// add routes to arraylist
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);
		Iterator<Ruta_Dades> rdi = comprovacioRendimentTmp.llistaRutes.iterator();
		while (rdi.hasNext()) {
			Ruta_Dades rd = rdi.next();
			System.out.println(rd.getNom() + " waypoints[" + rd.getWaypoints() + "]");
		}
		// comprovacioRendimentTmp.llistaRutes.forEach((route) ->
		// System.out.println(route));
		return comprovacioRendimentTmp;
	}

	// Krona set 5 21 Unio
	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Integer> waypoints = new <Integer>HashSet();

		comprovacioRendimentTmp.llistaRutes.forEach((ruta) -> waypoints.addAll(ruta.getWaypoints()));
		System.out.println("ID dels waypoints ficats en el set " + waypoints);
	}

	// Krona set 6 22 interseccio
	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		Set<Integer> waypointsIntersect = new <Integer>HashSet();
		Iterator<Ruta_Dades> ruDad = comprovacioRendimentTmp.llistaRutes.iterator();
		int counter = 0;
		for (int i = 0; i < comprovacioRendimentTmp.llistaRutes.size(); i++) {
			Ruta_Dades rdiCurr = comprovacioRendimentTmp.llistaRutes.get(i);
			if (i == 0) {
				waypointsIntersect.addAll(rdiCurr.getWaypoints());
			} else {
				Ruta_Dades rdiPrev = comprovacioRendimentTmp.llistaRutes.get(i - 1);
				waypointsIntersect.retainAll(rdiPrev.getWaypoints());
			}

		}
		System.out.println("ID dels waypoints en totes les rutes: " + waypointsIntersect);
	}

	// set 7 222 buscarruta
	public static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		List<Ruta_Dades> rd = comprovacioRendimentTmp.llistaRutes;
		Iterator<Ruta_Dades> rdIt = rd.iterator();
		while (rdIt.hasNext()) {
			Integer rdId = rdIt.next().getId();
			if (rdId == numRuta) {
				return rdId;
			}
		}
		return -1;
	}

	// set 8 23
	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		System.out.println("Rutes actuals:");
		comprovacioRendimentTmp.llistaRutes.forEach((ruta) -> System.out
				.println("ID: " + ruta.getId() + " ruta: " + ruta.getNom() + ": waypoints " + ruta.getWaypoints()));

		Scanner sc = new Scanner(System.in);
		boolean invalidRoute = false;
		String[] routesArr;
		do {
			invalidRoute = false;
			System.out.println("Selecciona ruta A i B (format: 3 17): ");
			String routes = sc.nextLine();
			routesArr = routes.split(" ");

			if (routesArr.length > 2) {
				System.out.println("ERROR: Introduir 2 parametres separats per un espai en blanc. Has introduit "
						+ routesArr.length + " arametres.");
				invalidRoute = true;
			} else {
				try {
					int A = Integer.parseInt(routesArr[0]);
				} catch (final NumberFormatException e) {
					System.out.println(
							"ERROR: Has introduit " + routesArr[0] + " com a ruta. Els ID de les rutes són integers.");
					invalidRoute = true;
				}
				try {
					int B = Integer.parseInt(routesArr[1]);
				} catch (final NumberFormatException e) {
					System.out.println(
							"ERROR: Has introduit " + routesArr[1] + " com a ruta. Els ID de les rutes són integers.");
					invalidRoute = true;
				}
				if (!invalidRoute) {
					if (buscarRuta(Integer.parseInt(routesArr[0]), comprovacioRendimentTmp) == -1) {
						System.out.println("ERROR: no existeix la ruta " + routesArr[0] + " en el sistema.");
						invalidRoute = true;
					}
					if (buscarRuta(Integer.parseInt(routesArr[1]), comprovacioRendimentTmp) == -1) {
						System.out.println("ERROR: no existeix la ruta " + routesArr[1] + " en el sistema.");
						invalidRoute = true;
					}
				}
			}

		} while (invalidRoute);
		Set<Integer> hsRouteA = null;
		Set<Integer> hsRouteB = null;
		List<Ruta_Dades> lRD = comprovacioRendimentTmp.llistaRutes;
		for (Ruta_Dades ruta : lRD) {
			if (ruta.getId() == Integer.parseInt(routesArr[0])) {
				hsRouteA = new HashSet<Integer>(ruta.getWaypoints());
			}
			if (ruta.getId() == Integer.parseInt(routesArr[1])) {
				hsRouteB = new HashSet<Integer>(ruta.getWaypoints());
			}
		}
		System.out.println("HashSet (havent-hi afegir els wayponts de la ruta A) = " + hsRouteA);
		System.out.println("HashSet (havent-hi afegir els wayponts de la ruta B) = " + hsRouteB);

	}
	/*
	 * class MyComparator implements Comparator<Ruta_Dades> {
	 * 
	 * @Override public int compare(Ruta_Dades A, Ruta_Dades B) {
	 * 
	 * /* Si 2 rutes tenen els mateixos waypoints, a llavors pel sistema seran la
	 * mateixa ruta.
	 * 
	 * Si 2 rutes no tenen els mateixos waypoints, a llavors s'ordenaran per l'ID
	 * (de ID més gran amés petit).
	 */
	/*
	 * if (A.getWaypoints().equals(B.getWaypoints())) {
	 * System.out.println("A i B son iguals " + A + "----" + B); return 0; } else if
	 * (A.getId() < B.getId()){ return -1; } else if (A.getId() > B.getId()){ return
	 * 1; } return 0; }
	 * 
	 * }
	 */

	// set 9 24
	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		TreeSet<Ruta_Dades> tree = new TreeSet<Ruta_Dades>();
		tree.addAll(comprovacioRendimentTmp.llistaRutes);
		// comprovacioRendimentTmp.llistaRutes.forEach((ruta) -> tree.add(ruta));
		System.out.println(tree);

	}

	////// MAP ///////
	// 30 menu Krona (executar 1 i 20 primer)
	public static void crearLinkedHashMapDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		///////////////////////////////////////
		// ATENCIÓ AQUEST METODE NO GUARDA
		// DADES EN ComprovacioRendiment =>  mapaLinkedDeRutes (<Integer,Ruta_Dades)
		// el seguen metode (31) si ho fa
		///////////////////////////////////////
		long tempsInicial;
		long tempsFinal;
		int temps1aForma;
		int temps2aForma;
		int temps3aForma;
		int temps4aForma;
		int temps5aForma;
		LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
		int clauDelMap;
		int whileController = 0;
		int median1 = 0;
		int median2 = 0;
		int median3 = 0;
		int median4 = 0;
		int median5 = 0;
		
		
		mapaLinkedDeRutes = new LinkedHashMap<Integer, Ruta_Dades>();
		for (Ruta_Dades rutaTmp : comprovacioRendimentTmp.llistaRutes) {
			mapaLinkedDeRutes.put(rutaTmp.getId(), rutaTmp);
		}
		
		while (whileController < 10) {
		// set iterator //
		// 1a forma de visualitzar el contingut del map.
// 		// Fem un set amb les entrades del map i el recorrem. Es crea un set on el seu contingut són les entrades del mapa.
		tempsInicial = System.nanoTime();
		Set setTmp = mapaLinkedDeRutes.entrySet();
		Iterator it1 = setTmp.iterator();
		System.out.println("1a forma de visualitzar el contingut del map (map --> set + iterador del set):");
		while (it1.hasNext()) {
			Map.Entry me = (Map.Entry) it1.next();
			System.out.println("Clau del map = " + me.getKey() + ": \n" + me.getValue().toString());
		}
		tempsFinal = System.nanoTime();
		temps1aForma = (int) ((tempsFinal - tempsInicial) / 1000);
		// key iterator //
		// 2a forma de visualitzar el contingut del map.
       // Fem un iterador que navegui per les claus del mapa. Es crea un set on el seu contingut són les claus del map.
		tempsInicial = System.nanoTime();
		System.out.println();
		System.out.println("2a forma de visualitzar el contingut del map (iterator de les claus del map):");
		Iterator<Integer> it2 = mapaLinkedDeRutes.keySet().iterator();
		while (it2.hasNext()) {
			clauDelMap = it2.next();
			System.out.println(clauDelMap + ": " + mapaLinkedDeRutes.get(clauDelMap));
		}
		tempsFinal = System.nanoTime();
		temps2aForma = (int) ((tempsFinal - tempsInicial) / 1000);
		// for-each iterator //
		// 3a forma de visualitzar el contingut del map.
        // Lo mateix que la forma 1 però fent servir un bucle for en comptes d'un iterator.
	    tempsInicial = System.nanoTime();
	    System.out.println();
	    System.out.println("3a forma de visualitzar el contingut del map (for-each del map --> set):");
	    for (Entry<Integer, Ruta_Dades> dada : mapaLinkedDeRutes.entrySet()) {
	        System.out.println(dada.getKey() + ": " + dada.getValue().toString());
	    }
	    tempsFinal = System.nanoTime();
	    temps3aForma = (int)((tempsFinal - tempsInicial)/1000);
		// for-each iterator 2 //
	    tempsInicial = System.nanoTime();
	    mapaLinkedDeRutes.forEach((key,value) -> System.out.println(key + ": " + value));
	    tempsFinal = System.nanoTime();
		temps4aForma = (int) ((tempsFinal - tempsInicial) / 1000);
		// foreach only key
		tempsInicial = System.nanoTime();
	    mapaLinkedDeRutes.entrySet().stream().forEach(key-> System.out.println(key + ": " + mapaLinkedDeRutes.get(key)));
	    tempsFinal = System.nanoTime();
		temps5aForma = (int) ((tempsFinal - tempsInicial) / 1000);
		/// PRINT TEMPS ////
		System.out.println();
	    System.out.println("TEMPS PER 1a FORMA (map --> set + iterador del set): " + temps1aForma);
	    System.out.println("TEMPS PER 2a FORMA (iterator de les claus del map): " + temps2aForma);
	    System.out.println("TEMPS PER 3a FORMA (for-each del map --> set): " + temps3aForma);
	    System.out.println("TEMPS PER 4a FORMA (for-each del map (key value) --> set): " + temps4aForma);
	    System.out.println("TEMPS PER 5a FORMA (for-each del map (.entrySet().stream()) -> key --> set): " + temps5aForma);
	    median1 += temps1aForma;
		median2 += temps2aForma;
		median3 += temps3aForma;
		median4 += temps4aForma;
		median5 += temps5aForma;
	    whileController++;
		} // end while
		/// PRINT MITJANA de 10 DE TEMPS ////
		System.out.println();
		System.out.println("TEMPS EN MITJANA DE 10:");
	    System.out.println("TEMPS MITJANA PER 1a FORMA (map --> set + iterador del set): " + (median1 / 10));
	    System.out.println("TEMPS MITJANA PER 2a FORMA (iterator de les claus del map): " + (median2 /10));
	    System.out.println("TEMPS MITJANA PER 3a FORMA (for-each del map --> set): " + (median3 /10));
	    System.out.println("TEMPS MITJANA PER 4a FORMA (for-each del map (key value) --> set): " + (median4 /10));
	    System.out.println("TEMPS MITJANA PER 5a FORMA (for-each del map (.entrySet().stream()) -> key --> set): " + (median5 /11));
	}
	
	// MAP 31 menu exercici 5 (executar 30 previament) (GUARDA DADES AL SEU LLOC
	public static void visualitzarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
		int  waypointInt = -1;
		Scanner sc = new Scanner(System.in);
		boolean invalidRoute = false;
		do {
			invalidRoute = false;
			System.out.println("Escriu el numero del waypoint que vols buscar: ");
			String waypoint = sc.nextLine();
			try {
				waypointInt = Integer.parseInt(waypoint);
			} catch (final NumberFormatException e) {
				System.out.println(
						"ERROR: Has introduit " + waypoint+ " com a waypoint. Els ID dels waypoints són integers.");
				invalidRoute = true;
			}
			if (!invalidRoute) {
				boolean waypointFound = false;
				//mapaLinkedDeRutes = new LinkedHashMap<Integer, Ruta_Dades>();
				///// AIXO D'AQUI TRANSFEREIX LES DADES AL mapaLinkedDeRutes
				for (Ruta_Dades rutaTmp : comprovacioRendimentTmp.llistaRutes) {
					comprovacioRendimentTmp.mapaLinkedDeRutes.put(rutaTmp.getId(), rutaTmp);
				}
				System.out.println("RUTES QUE CONTENEN EL WAYPOINT: " + waypoint);
				for (Entry<Integer, Ruta_Dades> dada : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
			        if (dada.getValue().getWaypoints().contains(waypointInt)) {
			        	System.out.println("Clau del map: " + dada.getKey());
			        	System.out.println("Dades de la ruta: ");
			        	System.out.println(dada.getValue());
			        	waypointFound = true;
			        }
			    }
				if (!waypointFound) {
					System.out.println("ERROR: cap ruta conte aquest waypoint.");
				}
			}
		}while(invalidRoute);
	}
	// HASHMAP 32krona key value lists
	public static void  esborrarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		/*///// DEBUGGING DISPLAYS LinkedHashMap
		System.out.println("Imprimint totes les rutes per debuggar");
		comprovacioRendimentTmp.mapaLinkedDeRutes.forEach((key,value) -> System.out.println(value));
		*/
		Scanner sc = new Scanner(System.in);
		boolean invalidWaypoint = false;
		String waypoint = "";
		// comprovacio de ID de ruta sent Integer
		do {
			invalidWaypoint = false;
			System.out.println("Escriu el numero del waypoint que vols ESBORRAR: ");
			waypoint = sc.nextLine();
			if (!Cadena.stringIsInt(waypoint)) {
				invalidWaypoint = true;
				System.out.println("ERROR: has introduit "+ waypoint + " com a ID de ruta, les IDs de la ruta son integer.");
			}
		} while(invalidWaypoint);
		int waypointInt = Integer.parseInt(waypoint);
		// comprovació de que la ruta existeix
		// fem una copia sobre la qual iterarem
		LinkedHashMap<Integer, Ruta_Dades> copiaRutes = new LinkedHashMap<Integer, Ruta_Dades>(comprovacioRendimentTmp.mapaLinkedDeRutes);
		System.out.println("Borrant rutes que contenen el waypoint " + waypointInt);
		int[] counter = {0};
		copiaRutes.forEach((key,value) -> {
			if (value.getWaypoints().contains(waypointInt)) {
				comprovacioRendimentTmp.mapaLinkedDeRutes.remove(key);
				counter[0]= 1;
				System.out.println(value);
			}
		});
		if (counter[0] == 1) {
			return;
		}
		System.out.println("Cap ruta conte el waypoint: " + waypointInt);
	}
	// 33 menu krona hashmaps
	public static void visualitzarUnaRutaDelMap(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner sc = new Scanner(System.in);
		String id = "";
		boolean invalidId = false;
		do {
			invalidId = false;
			System.out.println("Escriu la ID de la ruta que vols buscar: ");
			id = sc.nextLine();
			if (!Cadena.stringIsInt(id)) {
				invalidId = true;
				System.out.println("ERROR: has introduit "+ id + " com a ID de ruta, les IDs de la ruta son integer.");
			}
		} while(invalidId);
		int idInt = Integer.parseInt(id);
		comprovacioRendimentTmp.mapaLinkedDeRutes.forEach((key,value)->{
			if (value.getId() == idInt) {
				System.out.println("Rutes que tenen la id " + idInt + ":");
				System.out.println("Dades de la ruta: ");
				System.out.println(value);
			}
		});
	}
	// HASHMAP KEY VALUE 34 KRONA
	public static void ordenarRutesMapPerID(ComprovacioRendiment comprovacioRendimentTmp) {
		TreeMap tm = new TreeMap(comprovacioRendimentTmp.mapaLinkedDeRutes);
		tm.forEach((key, value) -> System.out.println(key + ": Dades de la ruta: \n" + value));
	}
	// HASHMAP KEY VALUE 35 KRONA
	public static void ordenarRutesMapPerWaypointsAndID(ComprovacioRendiment comprovacioRendimentTmp) {
		TreeMap tm = new TreeMap(comprovacioRendimentTmp.mapaLinkedDeRutes);
		TreeMap mapOrdenarPerId = new TreeMap(new comparadorPerID(tm));   //"comparadorPerEdat" està definit a baix de tot.
		mapOrdenarPerId.putAll(comprovacioRendimentTmp.mapaLinkedDeRutes);
		mapOrdenarPerId.forEach((key,value) -> System.out.println(key + ": Dades de la ruta: \n" + value));
	}
	// getters
	public static int getId() {
		return id;
	}

	public static String getNom() {
		return nom;
	}

	public static ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	public static boolean isActiu() {
		return actiu;
	}

	public static LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public static LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	public static LocalDateTime getDataModificacio() {
		return dataModificacio;
	}
	/*
	 * @Override public int compare(Ruta_Dades A, Ruta_Dades B) { if
	 * (A.getWaypoints().equals(B.getWaypoints())) {
	 * System.out.println("A i B son iguals " + A + "----" + B); return 0; } else if
	 * (A.getId() < B.getId()){ return -1; } else if (A.getId() > B.getId()){ return
	 * 1; } return 0; }
	 */
}
class comparadorPerID implements Comparator {
    Map mapa;

    public comparadorPerID(TreeMap mapa){
        this.mapa = mapa;
    }

    //Al fer un MapTree fent servir aquest Comparator, es farà servir la funció compare()
    //per a determinar la posició en l'arbre ordenat i evidentment no hi podran haver elements 
    //repetits dins de l'arbre (la funció compare() determina quan 2 elements són iguals
    //perquè ocupen la mateix posició d'ordenació en l'arbre). 
    //Com està agafant objectes de tipus Persona, quan és fa "return valorA.compareTo(valorB)"
    //en realitat està fent servir la funció compareTo() que hi ha dins de Persona i que està 
    //comparant dues persones en funció de la edat.
    public int compare(Object clauOb1, Object clauOb2){
        Comparable valorA = (Comparable) mapa.get(clauOb1);
        Comparable valorB = (Comparable) mapa.get(clauOb2);
        return valorA.compareTo(valorB);
    }
}