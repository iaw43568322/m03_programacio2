package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import Varies.Data;

public class ComprovacioRendiment {
	int[] coordenadesTmp = null;
	List<Waypoint_Dades> llistaArrayList;		
	
	List<Waypoint_Dades> llistaLinkedList;		// Com que el tipus és List tot i que fem un new ArrayList/LinkedList, només tindrem accés als mètodes definits per la interface List.
	List<Integer> idsPerArrayList;
	Waypoint_Dades wtmp;
	public Deque<Waypoint_Dades> pilaWaypoints;
	// Krona set
	public ArrayList<Ruta_Dades> llistaRutes;
	// Krona map
	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	//Waypoint_Dades waypoint;					// La idea és que en les llistes tots els elements siguin un clon d'aquest waypoint. El problema
												// és que al fer:
												//		comprovacioRendimentTmp.llistaArrayList.add(comprovacioRendimentTmp.waypoint);
												// estem ficant el mateix waypoint (el mateix objecte) en totes les posicions de la llista de manera
												// que al fer 1 canvi en un waypoint de la llista, es canvia en tots els altres (perquè en realitat
												// són el mateix objecte).
	// Qualsevol cosa que creem amb new() serà un objecte i per tant, quan el copiem d'un lloc a un altre, en realitat lo que estem fent es copiar el punter a l'objecte i no el propi objecte.
	// Això implica que si creo 1 objete i el copio en 30 posicions diferents d'un array, en realitat estarè copiant el punter a l'objecte de manera que quan modifiqui l'objete d'1 de les 
	// posicions en realitat estarè modificant l'objecte i per tant des de les altres 29 posicions veuran el canvi que hagi fet (perquè en les 30 posicions hi ha un punter a l'objecte i no
	// el propi objecte).
	
	
	public ComprovacioRendiment() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		this.coordenadesTmp = new int[] {0,0,0};
		this.llistaArrayList = new ArrayList<Waypoint_Dades>();		//
		this.llistaLinkedList = new LinkedList<Waypoint_Dades>();
		this.idsPerArrayList = new ArrayList<Integer>();
		// atributs afegits a krona 3.0 (Deque)
		int[] emptyCoords = {0,0,0}; 
		this.wtmp = new Waypoint_Dades(0, "default",emptyCoords , false, Data.formatData("11-08-2500 10:00"),Data.formatData("11-08-2500 10:00"), Data.formatData("11-08-2500 10:00")) ;
		this.pilaWaypoints = new ArrayDeque<Waypoint_Dades>();
		this.llistaRutes = new ArrayList<Ruta_Dades>();
		this.mapaLinkedDeRutes = new LinkedHashMap<Integer,Ruta_Dades>();
	}
	
	public void addListArr(Waypoint_Dades wpdObj) {
		this.llistaArrayList.add(wpdObj);
	}
	public void addListLinked(Waypoint_Dades wpdObj) {
		this.llistaLinkedList.add(wpdObj);
	}
	
}
