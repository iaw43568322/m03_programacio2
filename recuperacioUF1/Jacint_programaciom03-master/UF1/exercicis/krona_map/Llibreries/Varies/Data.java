package Varies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Data {
	public static  LocalDateTime formatData(String data) {
		if(data == null) {
			return null;
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return LocalDateTime.parse(data, formatter);
	}
	public static  String formatDataLDT(LocalDateTime data) {
		if(data == null) {
			return "NULL";
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		return data.format(formatter);
	}
}
