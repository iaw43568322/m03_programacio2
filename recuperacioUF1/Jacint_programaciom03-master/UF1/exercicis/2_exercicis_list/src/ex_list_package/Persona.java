package ex_list_package;

public class Persona implements Comparable<Persona> {
	// attributes
	public static int id = 1;
	public int dni;
	public String name;
	public int age;
	
	
	// constructor
	protected Persona(String name, int age) {
		this.dni= Persona.id;
		Persona.id++;
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Persona [name=" + name + ", age=" + age + ", dni=" + dni + "]";
	}
	// compareTo
	@Override
    public int compareTo(Persona persona) {        
        int compareAge = persona.age;
        return this.age - compareAge;
    }
}





















