package ex_list_package;

import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;

public class Ex_1 {
	// attributes
	LinkedList<Integer> numList;
	
	
	
	
	protected Ex_1() {
		this.numList = new LinkedList<Integer>();
	}

	// methods
	
	//add num
	public void myAdd(String n) {
		numList.add(Integer.parseInt(n));
	}
	
	// print content of list
	public void printList() {
		System.out.println("Recorregut llista cap en davant:");
		ListIterator<Integer> listIt = numList.listIterator();
		while(listIt.hasNext()) {
			System.out.println(listIt.next());
		}
	}
	
	// get size
	public int getSize() {
		return numList.size();
	}
	
	// transform sqr
	public void modifSqr() {
		int size = getSize();
		int i = 0;
		while (i < size) {
			numList.set(i,(int) Math.pow(numList.get(i),2) );
			i++;
		}
	}
	
	// remove items greater than 100 (with iterator)
	public void purgeGtT100() {
		ListIterator<Integer> listIt = numList.listIterator();
		while(listIt.hasNext()) {
			int checkVal = listIt.next(); 
			if (checkVal > 100) {
				numList.remove(listIt.previousIndex());
			}
		}
	}
	// sort method
	public void mySort() {
		Collections.sort(numList);
	}
	
	// returning list (for exercise 2)
	public LinkedList<Integer> retList() {
		return this.numList;
	}
}





















