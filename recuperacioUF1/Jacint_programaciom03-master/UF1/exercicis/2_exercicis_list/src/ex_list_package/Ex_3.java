package ex_list_package;

import java.util.ArrayList;
import java.util.ListIterator;

public class Ex_3 {
	// attributes
	ArrayList<Persona> persones;
	
	
	
	
	protected Ex_3() {
		this.persones = new ArrayList<Persona>();
	}

	// methods
	
	// add new elements to list 2
	public void myAdd(Persona p) {
		persones.add(p);
	}
	
	// build a list with only minors, return a list
	public ArrayList<Persona> menorsEdat() { 
		ArrayList<Persona> result = new ArrayList<Persona>();
		ListIterator<Persona> listIt = persones.listIterator();
		while(listIt.hasNext()) {
			Persona currPers = listIt.next();
			if (currPers.age < 18) {
				result.add(currPers);
			}
		}
		return result;
	}
	// return  stringbuilder element with all the elements of a list
	public StringBuilder printList(ArrayList<Persona> p) {
		StringBuilder result = new StringBuilder("");  
		ListIterator<Persona> listIt = p.listIterator();
		while(listIt.hasNext()) {
			result.append(listIt.next());  
			result.append("\n");
		}
		return result;
	}
	
	
}





















