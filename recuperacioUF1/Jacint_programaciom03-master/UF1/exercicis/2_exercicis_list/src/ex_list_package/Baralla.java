package ex_list_package;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Baralla {
	// attributes
	// pals
	public static final String[] PALS = {"cors", "piques", "diamants", "trèvols"};
	public static final String[] VALORS = {"AS", "2", "3", "4", "5", "6", "7", "8" ,"9", "10", "J", "Q", "K"};
	List<Carta> baralla;
	List<Player> players;

	// consturctor
	protected Baralla() {
		this.baralla = new ArrayList<Carta>();
		this.players = new ArrayList<Player>();
	}

	// methods
	
	// construir baralla
	public void crearBaralla() {
		int palsLength = Baralla.PALS.length;
		int valorsLength = Baralla.VALORS.length;
		for (int myPals = 0; myPals < palsLength; myPals++) {
			for (int myValors = 0; myValors < valorsLength; myValors++) {
				Carta c = new Carta (Baralla.PALS[myPals], Baralla.VALORS[myValors]);
				baralla.add(c);
			}
		}
	}
	
	// print cards method:
	public StringBuilder printList() {
		StringBuilder result = new StringBuilder("");  
		ListIterator<Carta> listIt = baralla.listIterator();
		int counter = 0;
		while(listIt.hasNext()) {
			result.append(listIt.next());
			result.append(", numOfCard" + counter);
			result.append("\n");
			counter++;
		}
		return result;
	}
	
	// shuffle method
	public void myShuffle() {
		Collections.shuffle(baralla);
	}
	
	// build players
	public void initializePlayers(int n) {
		int numOfCardsPerPlayer = baralla.size() / n;
		int currNum = numOfCardsPerPlayer * n;
		for (int myPlayers = 0; myPlayers < n; myPlayers++) {
			List<Carta> myCardsPlayer = baralla.subList(currNum - numOfCardsPerPlayer,currNum);
			List<Carta> playerFinalList = new ArrayList<Carta>(myCardsPlayer);
			Player p = new Player (myPlayers, playerFinalList);
			System.out.println(printThisList(myCardsPlayer));
			//baralla.subList(currNum - numOfCardsPerPlayer,currNum).clear();
			System.out.println("--------Printing List-----------");
			System.out.println(baralla);
			players.add(p);
			
			baralla.subList(currNum - numOfCardsPerPlayer,currNum).clear();
			//myCards.clear();
			//System.out.println("a");
			
			//System.out.println();
			//baralla.removeAll(myCards);
			//System.out.println("-------------Printing List---------\n" + printList() );
			//baralla.removeAll(myCards);
			//System.out.println("b");

			currNum -=  numOfCardsPerPlayer;
			//currNum
		}
	}
	
	// for debuggin purposes
	// print sublist
	// print cards method:
		public StringBuilder printThisList(List<Carta> myList) {
			StringBuilder result = new StringBuilder("");  
			ListIterator<Carta> listIt = myList.listIterator();
			int counter = 0;
			while(listIt.hasNext()) {
				result.append(listIt.next());
				result.append(", numOfCard " + counter);
				result.append("\n");
				counter++;
			}
			return result;
		}
	
	
	// print players and their cards method:
		public StringBuilder printPlayers() {
			StringBuilder result = new StringBuilder("");  
			ListIterator<Player> listIt = players.listIterator();
			while(listIt.hasNext()) {
				result.append(listIt.next());  
				result.append("\n");
			}
			return result;
		}
}





















