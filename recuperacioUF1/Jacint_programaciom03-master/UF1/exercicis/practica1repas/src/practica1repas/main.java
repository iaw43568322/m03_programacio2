package practica1repas;

public class main {

	public static void main(String[] args) {
		Programmer p = new Programmer("John", 1, 1000);
		
		System.out.println(p);
		
		// changing attributes
		p.setName("Manolo");
		p.setId(2);
		p.setSalary(1500);
		System.out.println(p);
		
	}

}
