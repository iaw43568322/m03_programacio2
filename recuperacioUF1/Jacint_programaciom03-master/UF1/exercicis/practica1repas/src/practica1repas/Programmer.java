package practica1repas;

public class Programmer {
	
	// attributes
	public String name;
	public int id;
	public double salary;
	
	public Programmer(String name, int id, double salary) {
		this.name = name;
		this.id = id;
		this.salary = salary;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Programmer [name=" + name + ", id=" + id + ", salary=" + salary + "]";
	}
}
