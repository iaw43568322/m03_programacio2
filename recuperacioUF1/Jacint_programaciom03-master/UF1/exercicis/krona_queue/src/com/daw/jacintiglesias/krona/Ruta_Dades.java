package com.daw.jacintiglesias.krona;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Ruta_Dades {
	@Override
	public String toString() {
		return "Ruta_Dades [id=" + Ruta.getId()+ ", nom=" + Ruta.getNom() + ", waypoints=" + Ruta.getWaypoints() + ", actiu=" + Ruta.isActiu() 
				+ ", dataCreacio=" + Ruta.getDataCreacio() + ", dataAnulacio=" + Ruta.getDataAnulacio()  + ", dataModificacio="
				+ Ruta.getDataModificacio() + "]";
	}
}
