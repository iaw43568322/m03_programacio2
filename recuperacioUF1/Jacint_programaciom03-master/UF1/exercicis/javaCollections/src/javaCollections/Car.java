/**
 * 
 */
package javaCollections;

/**
 * @author iaw14270791
 *
 */
public class Car implements Vehicle {
	// attributs
	public String name;
	public float price;
	public String brand;
	
	// constructor
	public Car(String name, float price, String brand) {
		this.name = name;
		this.price = price;
		this.brand = brand;
	}
	
	// setters and getters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Override
	public void Drive() {
		System.out.println("Conduint el cotxe");
		
	}

	@Override
	public void Stop() {
		System.out.println("Parant el cotxe");
		
	}

	@Override
	public void StartEngine() {
		// TODO Auto-generated method stub
		System.out.println("Enjegant el motor");
	}

	@Override
	public void TurnLeft() {
		// TODO Auto-generated method stub
		System.out.println("Girar a la esquerra");
	}

	@Override
	public void TurnRight() {
		// TODO Auto-generated method stub
		System.out.println("Girar a la dreta");
	}

	@Override
	public void GoToITV() {
		// TODO Auto-generated method stub
		System.out.println("Anara la ITV");
	}
}
