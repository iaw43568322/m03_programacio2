#M03 Programació.  
## Types of collections:  
* Deque
* Queue
* Stack
----
* HashSet
* Set
* TreeSet
* SortedSet (comparable)
* LinkedHashSet
----
* Map
* HashMap
  
### Queue
* **.offer** => **.add**  
* **.poll** => recupera el primer element i l'elimina
  
#### HAS METHOD DescendingIterator  
  
  
### Deque => pila  
  
```
|   4	|=> first to recover
|-------|
|   3	|
|-------|
|   2	|
|-------|
|   1	|
_________
```
  
* **.push** => last is first
* **.offer** last position 
* **.pop** => get last element and delete it  
  
### Set  
* metodes especials
	* removeAll
	* retainAll  
  
### HashSet  
Per defecte no accepta objectes repetits. Si vols repetits has de definir el hashcode i equals per redefinirles.  
Es molt eficient per accedir, pero **NO ORDENA**.  
Sense ordre.  
Inserta en base a la funció hash.  
* te iterator  
* no preserva ordre d'inserció
  
#### Hash function
Example 15? => 15 % 8 = 7 ||| 8 being the positions available.  
Pot emmagatzemar varis valors a la mateixa casella.  
  
    
### LinkedHashSet  
* Te ordre de insercio 
* podem iterar segons l'ordre d'inserció  
* no agafa duplicats
 
  
### TreeSet  
* extructura d'arbre
* no duplicats
* access molt eficient  
* no ordre d'inserció sino **ordre ascendent**
* descending iterator
* ordre ascendent

  
## Examen.  
No entra teoria, es practic, 2 hores.  
Todas las estructuras que hemos visto.  
* metodo comparator
* interficie comparable => compareTo  

